TARGET = QDominoes
TEMPLATE = app

MOC_DIR = moc
OBJECTS_DIR = obj
RCC_DIR = rcc

include(src/src.pri)

CONFIG += mobility
MOBILITY += gallery systeminfo

# Symbian-specific
symbian {
    TARGET.UID3 = 0xE875C44E
    TARGET.CAPABILITY = NetworkServices
    TARGET.EPOCHEAPSIZE = 0x100000 \
       0x2500000
    TARGET.EPOCSTACKSIZE = 0x14000

    # Libraries needed for locking Symbian device's orientation to landscape
    LIBS += -lcone \
            -leikcore \
            -lavkon \
            -lesock \
            -lcommdb \
            -linsock

    DEFINES += TARGET_SYMBIAN
}
else: contains(MEEGO_EDITION,harmattan) {
    isEmpty(PREFIX)
    PREFIX = /usr
    BINDIR = $$PREFIX/bin
    DATADIR = $$PREFIX/share

    DEFINES += DATADIR=\\\"$$DATADIR\\\" PKGDATADIR=\\\"$$PKGDATADIR\\\"

    desktopfile.files = qdominoes_harmattan.desktop
    desktopfile.path = $$DATADIR/applications

    INSTALLS += desktopfile

    icon.files = qdominoes80.png
    icon.path = $$DATADIR/icons/hicolor/80x80/apps
    INSTALLS += icon

    target.path = $$BINDIR
    INSTALLS += target

    # Speed up launching on MeeGo/Harmattan when using applauncherd daemon
    # CONFIG += qdeclarative-boostable

    DEFINES += TARGET_MEEGO
} else: android-g++ {
    DEFINES += TARGET_ANDROID
} else {
    DEFINES += TARGET_DESKTOP
}


OTHER_FILES += \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/manifest.aegis \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog

RESOURCES += \
    qml_files.qrc \
    src/resources/profile_pics.qrc \
    src/resources/tiles/tiles.qrc \
    src/resources/Meego/meego_assets.qrc











