QDominoes S60 5th v0.1 alpha - A dominoes game for mobile devices.
Copyright (C) 2010, Nokia Corporation.

This file is part of QDominoes S60 5th v0.1 alpha.

QDominoes is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QDominoes is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with QDominoes.  If not, see <http://www.gnu.org/licenses/>.

You can also see the COPYING and README files for more info on
copying and contacting the original author.

Contacting the authors: projeto_ceteli_nokia@googlegroups.com

Feel free to use the application and to upgrade it's features but remember: 
this is a GPL software, so always give credit to the original authors.

