// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../js/Director.js" as ViewsDirector


Item {
    id: menu

    visible: false

    signal show;
    signal hide;

    MouseArea {
        anchors.fill: parent
        onClicked: {}
    }

    SequentialAnimation {
        id: entering
        NumberAnimation {
            target: menu
            from: -menu.height
            to: 50
            properties: "y"
            duration: 300
        }
        onStarted: {
            menu.visible = true
        }
    }

    SequentialAnimation {
        id: leaving
        NumberAnimation {
            target: menu
            from: 50
            to: -menu.height
            properties: "y"
            duration: 300
        }
        onCompleted: {
            menu.visible = false
        }
    }

    onShow: {
        leaving.stop();
        entering.start();
    }

    onHide: {
        entering.stop();
        leaving.start();
    }

}
