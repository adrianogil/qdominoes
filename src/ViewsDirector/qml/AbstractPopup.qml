// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../js/Director.js" as ViewsDirector


Item {
    id: popup
    signal hide();
    signal show();

    anchors.centerIn: parent
    state: "invisible"

    MouseArea {
        anchors.fill: parent
        onClicked: {}
    }

    onShow: {
        popup.state = "visible"
    }

    onHide: {
        popup.state = "invisible";
        popup.destroy(1000);
    }

    states: [
        State {
            name: "visible"
            PropertyChanges {target: popup; opacity: 1}
        },
        State {
            name: "invisible"
            PropertyChanges {target: popup; opacity: 0 }
        }
    ]

    transitions: [
        Transition {
            from: "invisible"
            to: "visible"
            NumberAnimation { target: popup; property: "opacity"; duration: 400; easing.type: Easing.InExpo }
        },
        Transition {
            from: "visible"
            to: "invisible"
            NumberAnimation { target: popup; property: "opacity"; duration: 400; easing.type: Easing.InExpo }
        }
    ]



}
