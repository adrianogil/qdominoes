import QtQuick 1.1
import "../js/Director.js" as ViewsDirector

Rectangle {
    id: fade

    signal clicked;
    signal hide;
    signal show;

    anchors.fill: parent
    color: "#000000"
    state: "invisible"

    NumberAnimation on opacity {duration:300}

    MouseArea {
        anchors.fill: parent
        onClicked: {
            ViewsDirector.closePopup();
            fade.clicked();
        }
    }

    states: [
        State {
            name: "visible"
            PropertyChanges {target: fade; opacity: 0.85}
        },
        State {
            name: "invisible"
            PropertyChanges {target: fade; opacity: 0 }
        }
    ]

    transitions: [
        Transition {
            from: "invisible"
            to: "visible"
            NumberAnimation { target: fade; property: "opacity"; duration: 400; easing.type: Easing.InQuad }
        },
        Transition {
            from: "visible"
            to: "invisible"
            NumberAnimation { target: fade; property: "opacity"; duration: 400; easing.type: Easing.InQuad }
        }
    ]

    onShow: {
        fade.state = "visible"
    }

    onHide: {
        fade.state = "invisible"
        fade.destroy(1000);
    }

}
