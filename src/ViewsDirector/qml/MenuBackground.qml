// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../js/Director.js" as ViewsDirector

Item {
    id: background

    anchors.fill: parent;

    signal show
    signal hide

    state: "invisible"

    Rectangle {
        id: fade
        anchors.fill: parent
        opacity: 0
        color: "black"

        MouseArea {
            anchors.fill: parent
            onClicked: ViewsDirector.closeMenu();
        }
    }

    transitions: [
        Transition {
            from: "visible"
            to: "invisible"
            NumberAnimation { target: fade; property: "opacity"; duration: 300; easing.type: Easing.OutQuad }
        },
        Transition {
            from: "invisible"
            to: "visible"
            NumberAnimation { target: fade; property: "opacity"; duration: 300; easing.type: Easing.InQuad}
        }

    ]

    states:[
        State {
            name: "visible"
            PropertyChanges {target:fade; opacity: 0.8}
        },
        State {
            name: "invisible"
            PropertyChanges {target:fade; opacity: 0}
        }
    ]

    onShow: {
        background.state = "visible"
    }

    onHide: {
        background.state = "invisible"
    }

}
