.pragma library

function Menu() {

    var container;
    var menuComponent;
    var backComponent = Qt.createComponent("../qml/MenuBackground.qml");
    var background;
    var menuObject;

    this.create = function(){
             background = backComponent.createObject(container,{});
             menuObject = menuComponent.createObject(container,{});
         }

    this.setContainer = function(cont){
             container = cont;
         }

    this.setMenu = function(component){
             menuComponent = component;
         }

    this.open = function(z){
             background.z = z+1;
             menuObject.z = background.z+1;
             background.show();
             menuObject.show();
         }

    this.close = function(){
             menuObject.hide();
             background.hide();
         }
    
}
