function Dictionary() {

    var keys = new Array();
    var hash = new Object();

    this.add = function(key,value){
        if(typeof(hash[key])=='undefined'){
            keys.push(key);
        }
        hash[key] = value;
    }

    this.contains = function(key){
        if(keys.indexOf(key) == -1){
            return false;
        }
        return true;
    }

    this.get = function(key){
        return hash[key];
    }

    this.remove = function(key){
        var pos = keys.indexOf(key);
        if (pos != -1){
            keys.splice(pos,1);
            delete hash[key];
        }
    }

    this.getSortedKeys = function(){
        keys.sort();
        return keys;
    }

}
