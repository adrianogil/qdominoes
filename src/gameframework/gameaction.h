#ifndef GAMEACTION_H
#define GAMEACTION_H

#include <QObject>
#include "gamestate.h"
#include "qtsynthesize.h"

#include "appdebug.h"

#define CHECK_CAST(object, var, cast, callback) \
    cast var = qobject_cast<cast>(object); \
    if (var) \
        callback(var);

class GameAction : public QObject
{
    Q_OBJECT

    Q_SYNTHESIZE(int, actionNumber, ActionNumber, actionNumberChanged)
    Q_SYNTHESIZE(bool, hasEcho, HasEcho, hasEchoChanged)
public:
    GameAction(QObject *parent = 0) : QObject(parent), m_hasEcho(false) {}

    bool attemptToDo(GameState *gameState) {
        bool result = verifyConditions(gameState);
        if (result) {
            applyToState(gameState);
            onActionFinished(gameState);
        } else {
            onActionFailed(gameState);
        }

        debugMsg << result;

        return result;
    }

    virtual bool verifyConditions(GameState *gameState) { Q_UNUSED(gameState); return true; }
    virtual void applyToState(GameState *gameState) {
        if (gameState)
            gameState->setLastActionNumber(actionNumber());
    }
    virtual void onActionFinished(GameState *gameState) {
        Q_UNUSED(gameState);
    }
    virtual void onActionFailed(GameState *gameState) {
        Q_UNUSED(gameState);
    }

signals:
    void actionNumberChanged();
    void hasEchoChanged();
};

typedef QList<GameAction*> GameActionList;


#endif // GAMEACTION_H
