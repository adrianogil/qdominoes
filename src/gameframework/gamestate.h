#ifndef GAMESTATE_H
#define GAMESTATE_H

#include <QObject>
#include "qtsynthesize.h"

class Team;
class PlayerModel;
class TableModel;

class GameState : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QObject* currentPlayer READ currentPlayerObj NOTIFY currentPlayerChanged)
    Q_PROPERTY(QObject* teamA READ teamAObj NOTIFY teamAChanged)
    Q_PROPERTY(QObject* teamB READ teamBObj NOTIFY teamBChanged)

    //! TeamModel teamA
    Q_SYNTHESIZE(Team*, teamA, TeamA, teamAChanged)
    //! TeamModel teamB
    Q_SYNTHESIZE(Team*, teamB, TeamB, teamBChanged)
    //! Tile Model of Table
    Q_SYNTHESIZE(TableModel*, tableModel, TableModel, tableModelChanged)
    //! Current Player - PlayerModel
    Q_SYNTHESIZE(PlayerModel*, currentPlayer, CurrentPlayer, currentPlayerChanged)
    //! Last Action Number
    Q_SYNTHESIZE(int, lastActionNumber, LastActionNumber, lastActionNumberChanged)
    //! Match number
    Q_SYNTHESIZE(int, matchNumber, MatchNumber, matchNumberChanged)


public:
    explicit GameState(QObject *parent = 0)
        : QObject(parent)
        , m_tableModel(0)
  {
  }

    QObject* currentPlayerObj() { return (QObject*) m_currentPlayer; }
    QObject* teamAObj() { return (QObject*) m_teamA; }
    QObject* teamBObj() { return (QObject*) m_teamB; }

    void updateAll() {
        emit teamAChanged();
        emit teamBChanged();
        emit currentPlayerChanged();
    }

signals:
    void teamAChanged();
    void teamBChanged();
    void tableModelChanged();
    void currentPlayerChanged();
    void lastActionNumberChanged();
    void matchNumberChanged();

public slots:

};

#endif // GAMESTATE_H
