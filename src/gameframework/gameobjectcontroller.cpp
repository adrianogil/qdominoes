#include "gameobjectcontroller.h"

GameObjectController::GameObjectController(QObject *parent)
    : QObject(parent)
    , m_gameState(0)
{
}

void GameObjectController::updateState(GameState *gameState)
{
    m_gameState = gameState;
}

GameAction * GameObjectController::nextAction()
{
    GameAction *nextGameAction = 0;

    if (m_actionBuffer.length() > 0)
        nextGameAction = m_actionBuffer.first();

    return nextGameAction;
}

GameAction * GameObjectController::takeNextAction()
{
    GameAction *nextGameAction = 0;

    if (m_actionBuffer.length() > 0)
        nextGameAction = m_actionBuffer.takeFirst();

    return nextGameAction;
}
