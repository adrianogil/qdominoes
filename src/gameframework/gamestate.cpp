#include "gamestate.h"
#include "player/playermodel.h"
#include "team.h"
#include "table/tablemodel.h"

GameState::GameState(QObject *parent)
    : QObject(parent)
    , m_teamA(0)
    , m_teamB(0)
    , m_tableModel(0)
    , m_currentPlayer(0)
{
}

