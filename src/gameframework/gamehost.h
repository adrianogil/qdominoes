/*
* TukEngine v0.1 alpha - A game engine and editor for development of two-dimensional games.
* Copyright (C) 2011, Universidade Federal do Amazonas - UFAM.
*
* This file is part of TukEngine v0.1 alpha.
*
* TukEngine is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* TukEngine is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Star Battles. If not, see <http://www.gnu.org/licenses/>.
*
* You can also see the COPYING and README files for more info on
* copying and contacting the original author.
*
* Contacting the authors: projeto_ceteli_nokia@googlegroups.com
*
* Feel free to use the application and to upgrade it's features but remember: this is a GPL software, so always give credit to the original author.
*/

#ifndef GAMEHOST_H
#define GAMEHOST_H

#include <QtCore/QObject>

#include <QtCore/QBasicTimer>
#include <QtCore/QHash>
#include <QtCore/QList>
#include <QtCore/QTime>

#include "gameaction.h"

class GameObjectController;
class QTimerEvent;
class GameState;

class GameTime : public QTime
{
public:
    explicit GameTime();
    void pause();
    int totalElapsed() const;

private:
    int m_accumulatedTime;
};

class ActiveSourceAction : public QObject
{
    Q_NSYNTHESIZE(GameObjectController*, activeSource, ActiveSource)
    Q_NSYNTHESIZE(int, activeTime, ActiveTime)
public:
    ActiveSourceAction(GameObjectController *activeSource,
                       const int &activeTime,
                       QObject *parent = 0)
        : QObject(parent)
        , m_activeSource(activeSource)
        , m_activeTime(activeTime)
    {}
};

#define GET_SHARED_CONTROLLER(controllerName, controllerCast, objectController) \
    QObject *__temp = GameHost::sharedControllers(controllerName); \
    controllerCast objectController = qobject_cast<controllerCast>(__temp);

//! The GameHost class is the outermost class in the game framework hierarchy.
/*!
    It is a virtual class,
    built only for some convenience in having useful, common game features already implemented. That said, it
    is mandatory for the user to implement his or her own GameHost derived class and use it's built in functionality
    to his or her advantage.
    GameHost stores all the game objects and takes care of calling the GameObjectBase::updateState() and
    QGraphicsItem::paint() methods of each one, assuming startGameLoop() was called by the user.
    It also stores collections for quick access of multiple resources (e.g textures and sounds),
    as well as general game options.
    All other game objects should have this class as parent and should also be appended at gameObjects list.
    \sa renderQuality(), startGameLoop() and gameObjects.
*/

class GameHost : public QObject
{
    Q_OBJECT
    Q_ENUMS(RenderQuality)

    Q_PROPERTY(QObject *gameState READ gameState NOTIFY gameStateChanged)
public:
    //! RenderQuality is used by the user to specify the quality of rendered sprites.
    /*!
        The RenderQuality enum is used by the user to tell the system to use or not a group of
        optimization flags to render it's graphics. Each enumerator value activates a set of
        optimazation flags as specified below.

        \sa setRenderQuality() and renderQuality()
    */
    enum RenderQuality {
        LowQuality, /*!< At LowQuality level the engine will draw sprites the fastest way, without any aliasing. */
        NormalQuality, /*!< At NormalQuality the engine will use QPainter::SmoothPixmapTransform to draw sprites. */
        HighQuality /*!< At HighQuality the engine will use QPainter::SmoothPixmapTransform,
                        QPainter::Antialiasing and QPainter::TextAntialiasing to draw. */
    };

    //! GameHost constructor.
    /*!
        Explicitly constructs an empty GameHost object with no game objects registered. The user is responsible
        for registering game objects using the provided containers. Also note that GameHost, once constructed,
        will only start looping through the objects once the user calls startGameLoop().
        \sa startGameLoop() and stopGameLoop()
    */
    explicit GameHost(QObject *parent = 0);

    QObject *gameState();

    //! Set the engine's RenderQuality to be used by all sprites for rendering.
    /*!
        \param quality the RenderQuality enum value to be used by the paint engine.
        \sa renderQuality()
    */
    void setRenderQuality(RenderQuality quality) { m_renderQuality = quality; }

    //! Convenience function to query the currently quality option used by the engine.
    /*!
        \return the RenderQuality enum value currently used by the paint engine.
    */
    RenderQuality renderQuality() const { return m_renderQuality; }

    //! A public variable that stores all the game objects.
    /*!
        This variable should be used with caution. When the user creates a GameObjectBase object it is automatically
        appended in this list. If the object has to be destroyed by the user, however, it is the user's responsability to
        remove the object from this list prior to destroyment.
        This container is looped through by the engine wich then calls the update
        and paint methods of each object. The engine does this in a safe way so the user can modify this container's
        contents at will even during an updateAll() call.
    */
    QList<GameObjectController*> gameObjects;

    //! Starts looping through all gameObjects at a rate of \param framesPerSecond frames per second.
    /*!
        \param framesPerSecond the number of times per second that the engine will call the objects update and
        draw methods.
    */
    void startGameLoop(const int &framesPerSecond);

    //! Stops the engine loop.
    void stopGameLoop();

    //! Continue the engine loop
    void continueGameLoop();

    //! Access the GameTime object that stores the time since the game started.
    GameTime gameTime() const { return m_gameTime; }

    static QObject* sharedControllers(const QString &controllerName);
    static void addSharedController(const QString &controllerName, QObject *controller);

    void setAsActionSource(GameObjectController* objectController, const int &activeTime = 1) {
        m_actionSourceBuffer.append(new ActiveSourceAction(objectController, activeTime, this));
    }

    void removeActionSource(GameObjectController* objectController) {
        for (int i = 0; i < m_actionSourceBuffer.length(); i++) {
            ActiveSourceAction *elementSource = m_actionSourceBuffer[i];
            if (elementSource->activeSource() == objectController) {
                m_actionSourceBuffer.removeAt(i);
                break;
            }
        }
    }

signals:
    void gameStateChanged();

protected:
    //! Loop through all gameObjects calling their respective GameObjectBase::updateState() method.
    /*!
        \param gameTime a timer that stores the number of milliseconds that have elapsed since the last time
        startGameLoop() was called.
        \sa startGameLoop() and timerEvent()
    */
    virtual void updateAll(const GameTime &gameTime);

    //! Reimplemented function. Calls updateAll() each time the internal timer triggers this event.
    /*!
        \param event the event propagated by the system.
        \sa updateAll() and startGameLoop()
    */
    void timerEvent(QTimerEvent *event);

    GameState *m_gameState;

private:
    QBasicTimer m_gameLoopTimer;
    int m_fps;
    GameTime m_gameTime;
    RenderQuality m_renderQuality;

    GameActionList m_actionHistory;
    QList<ActiveSourceAction*> m_actionSourceBuffer;
};

#endif // GAMEHOST_H
