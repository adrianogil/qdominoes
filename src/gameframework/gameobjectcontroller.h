#ifndef GAMEOBJECTCONTROLLER_H
#define GAMEOBJECTCONTROLLER_H

#include <QObject>
#include "gameaction.h"

class GameObjectController : public QObject
{
    Q_OBJECT

public:
    explicit GameObjectController(QObject *parent = 0);

    //! Only return the first GameAction on qeue
    GameAction* nextAction();
    //! Remove and return the first GameAction on qeue
    GameAction* takeNextAction();

    virtual void updateState(GameState* gameState);

    virtual void receiveAction(GameAction* action) { Q_UNUSED(action)}

protected:
    GameState *m_gameState;
    GameActionList m_actionBuffer;

signals:
    void gameStateChanged();

public slots:

};

#endif // GAMEOBJECTCONTROLLER_H
