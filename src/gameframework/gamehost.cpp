/*
* TukEngine v0.1 alpha - A game engine and editor for development of two-dimensional games.
* Copyright (C) 2011, Universidade Federal do Amazonas - UFAM.
*
* This file is part of TukEngine v0.1 alpha.
*
* TukEngine is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* TukEngine is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Star Battles. If not, see <http://www.gnu.org/licenses/>.
*
* You can also see the COPYING and README files for more info on
* copying and contacting the original author.
*
* Contacting the authors: projeto_ceteli_nokia@googlegroups.com
*
* Feel free to use the application and to upgrade it's features but remember: this is a GPL software, so always give credit to the original author.
*/

#include "gamehost.h"

#include "gameobjectcontroller.h"
#include "gamestate.h"

#include <QtCore/QMap>
#include <QtCore/QTimerEvent>

#include "appdebug.h"

static QMap<QString, QObject*> sharedGameObjectControllers;

GameTime::GameTime()
    : QTime()
    , m_accumulatedTime(0)
{
    setHMS(0, 0, 0, 0);
}

void GameTime::pause()
{
    m_accumulatedTime += elapsed();
}

int GameTime::totalElapsed() const
{
    return (elapsed() + m_accumulatedTime);
}

GameHost::GameHost(QObject *parent)
    : QObject(parent)
    , m_fps(0)
    , m_renderQuality(NormalQuality)
    , m_gameState(0)
{
    sharedGameObjectControllers = QMap<QString, QObject*>();
}

void GameHost::updateAll(const GameTime &gameTime)
{
    Q_UNUSED(gameTime)

    QList<GameObjectController*> updatingGameObjects = gameObjects;

    foreach (GameObjectController *gameObject, updatingGameObjects) {
        if (gameObject)
            gameObject->updateState(m_gameState);
    }

    if (m_actionSourceBuffer.isEmpty()) {
        debugMsg << "Null action source!";
        return;
    }

    ActiveSourceAction *activeElement = m_actionSourceBuffer.first();
    if (activeElement->activeTime() > 1) {
        activeElement->setActiveTime(activeElement->activeTime() - 1);
    } else if (activeElement->activeTime() == 1) {
        m_actionSourceBuffer.takeFirst();
    }

    GameAction *currentAction = activeElement->activeSource()->takeNextAction();

    debugMsg << activeElement << currentAction;

    if (!currentAction) {
        debugMsg << "Null action!";
        return;
    }

    if (currentAction->attemptToDo(m_gameState)) {
        m_actionHistory.append(currentAction);
    } else debugMsg << "Action failed!";

    if (currentAction->hasEcho()) {
        foreach (GameObjectController *gameObject, updatingGameObjects) {
            if (gameObject)
                gameObject->receiveAction(currentAction);
        }
    }
}

void GameHost::startGameLoop(const int &framesPerSecond)
{
    Q_ASSERT_X(framesPerSecond != 0, "divide", "division by zero");

    m_fps = framesPerSecond;
    m_gameLoopTimer.start(1000 / framesPerSecond, this);
    m_gameTime.start();
}

void GameHost::stopGameLoop()
{
    m_gameLoopTimer.stop();
    m_gameTime.pause();
}

void GameHost::continueGameLoop()
{
    startGameLoop(1000/30);
}

QObject* GameHost::sharedControllers(const QString &controllerName)
{
    if (sharedGameObjectControllers.contains(controllerName))
        return sharedGameObjectControllers[controllerName];

    return 0;
}

void GameHost::addSharedController(const QString &controllerName, QObject *controller)
{
    sharedGameObjectControllers[controllerName] = controller;
}

void GameHost::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == m_gameLoopTimer.timerId()) {
        this->updateAll(m_gameTime);
    } else {
        QObject::timerEvent(event);
    }
}

QObject * GameHost::gameState()
{
    return (QObject*) m_gameState;
}
