#include <QtGui/QApplication>
#include <QtDeclarative/QDeclarativeContext>
#include <QtCore/QDebug>

#include "qmlapplicationviewer.h"
#include "maincontroller.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QScopedPointer<QApplication> app(createApplication(argc, argv));

    MainController mainController;
    mainController.setupUi();

    return app->exec();
}
