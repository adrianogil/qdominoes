INCLUDEPATH += $$PWD

include($$PWD/game/game.pri)
include($$PWD/gallery/gallery.pri)
include($$PWD/controller/controller.pri)
include($$PWD/gameframework/gameframework.pri)
include($$PWD/qmlapplicationviewer/qmlapplicationviewer.pri)

QT       += core gui network xml

SOURCES += \
    $$PWD/main.cpp

HEADERS  += \
    $$PWD/appdebug.h \
    $$PWD/qtsynthesize.h


OTHER_FILES += \
    src/qml/qdominoes/ScreenManager.js \
    src/qml/qdominoes/ProfileManager.js \
    src/qml/qdominoes/TitleScreen.qml \
    src/qml/qdominoes/ProfileScreen.qml \
    src/qml/qdominoes/Profile.qml \
    src/qml/qdominoes/NewProfileScreen.qml \
    src/qml/qdominoes/main.qml \
    src/qml/qdominoes/Button.qml \
    src/qml/qdominoes/HandControler.js \
    src/qml/qdominoes/Hand.qml \
    src/qml/qdominoes/Highlight.qml \
    src/qml/qdominoes/Hand.js \
    src/qml/qdominoes/Tile.qml \
    src/qml/qdominoes/GameScreen.qml \
    src/qml/qdominoes/gameutils.js \
    src/qml/qdominoes/Table.qml \
    src/qml/qdominoes/Table.js \
    src/qml/qdominoes/TableElement.qml \
    src/qml/qdominoes/TableElement.js \
    src/qml/qdominoes/Tile.js \
    src/qml/qdominoes/ScoreAnimation.qml \
    src/qml/qdominoes/Highlight.js \
    src/qml/qdominoes/MainView.qml \
    src/qml/qdominoes/Main.qml \
    src/qml/qdominoes/Splash.qml \
    src/qml/qdominoes/utils.js \
    src/qml/qdominoes/ScoreTable.qml \
    src/qml/qdominoes/TilesRow.qml \
    src/qml/qdominoes/Player.qml \
    src/qml/qdominoes/Results.qml \
    src/qml/qdominoes/LevelChoiceScreen.qml \
    src/qml/qdominoes/RulesScreen.qml \
    src/qml/qdominoes/AboutScreen.qml \
    src/qml/qdominoes/ContactScreen.qml \
    src/qml/qdominoes/Application.qml \
    src/qml/qdominoes/CloseAppPopUp.qml \
    src/qml/qdominoes/DeleteProfilePopUp.qml \
    src/ViewsDirector/qml/MenuBackground.qml \
    src/ViewsDirector/qml/FadeElement.qml \
    src/ViewsDirector/qml/AbstractView.qml \
    src/ViewsDirector/qml/AbstractPopup.qml \
    src/ViewsDirector/qml/AbstractMenu.qml \
    src/ViewsDirector/js/Menu.js \
    src/ViewsDirector/js/Fade.js \
    src/ViewsDirector/js/Director.js \
    src/ViewsDirector/js/Dictionary.js \
    src/qml/qdominoes/PassedTurnPopUp.qml \
    src/qml/qdominoes/MoveBackMenuPopUp.qml





