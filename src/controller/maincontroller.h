#ifndef MAINCONTROLLER_H
#define MAINCONTROLLER_H

#include <QObject>

class GalleryController;
class GalleryImageProvider;
class QmlApplicationViewer;
class ProfilesController;
class GameController;

#if defined (TARGET_MEEGO)
#include <QInputContext>
class EventFilter : public QObject
{
protected:
    bool eventFilter(QObject *obj, QEvent *event) {
        QInputContext *ic = qApp->inputContext();
        if (ic) {
            if (ic->focusWidget() == 0 && prevFocusWidget) {
                QEvent closeSIPEvent(QEvent::CloseSoftwareInputPanel);
                ic->filterEvent(&closeSIPEvent);
            } else if (prevFocusWidget == 0 && ic->focusWidget()) {
                QEvent openSIPEvent(QEvent::RequestSoftwareInputPanel);
                ic->filterEvent(&openSIPEvent);
            }
            prevFocusWidget = ic->focusWidget();
        }
        return QObject::eventFilter(obj,event);
    }

private:
    QWidget *prevFocusWidget;
};
#endif

class MainController : public QObject
{
    Q_OBJECT
public:
    explicit MainController(QObject *parent = 0);

    void setupUi();

public slots:
    void startGame();

signals:
    void gameStarted();

private slots:
    void onResize();

private:
    QmlApplicationViewer *m_viewer;
    GameController *m_gameController;
    ProfilesController *m_profileController;
    GalleryImageProvider *m_galleryImageProvider;
    GalleryController *m_galleryController;
};

#endif // MAINCONTROLLER_H
