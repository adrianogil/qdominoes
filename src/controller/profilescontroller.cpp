#include <QtCore/QFile>
#include <QtXml/QDomElement>
#include <QtCore/QTextStream>
#include <QtCore/QVariant>

#include "profilescontroller.h"
#include "player/playermodel.h"
#include "appdebug.h"

int ProfilesController::chosenProfile = 1;

ProfilesController::ProfilesController(QObject *parent) :
    QObject(parent)
{
    //m_xmlHandler = new XmlHandler(this);
    //updateListProfiles();
    debugMsg;
}

QList<QObject *> ProfilesController::convertToObj(QList<PlayerModel *> playerList)
{
    debugMsg;
    QList<QObject*> objList;
    foreach(PlayerModel* player, playerList) {
        QObject *obj = (QObject*) player;
        objList.append(obj);
    }
    return objList;
}
bool ProfilesController::createProfile(PlayerModel *player)
{
    debugMsg;
    QDomDocument profileXML("Profiles");
    QFile file("profiles.xml");
    if (!file.exists())
    {
        if (!file.open(QFile::WriteOnly))
            return false;

        QDomElement rootNode = profileXML.createElement("Profiles");
        QDomElement profileNode = profileXML.createElement("Player");
        profileNode.setAttribute("name", player->name());
        profileNode.setAttribute("avatar", player->avatarPath());

        rootNode.appendChild(profileNode);

        profileXML.appendChild(rootNode);

        QTextStream ts(&file);
        ts << profileXML.toString();
        file.close();
    }
    else if (file.open(QFile::ReadOnly))
    {
        if (!profileXML.setContent(&file))
        {
            file.close();
            return false;
        }
        QDomElement rootNode = profileXML.firstChildElement("Profiles");
        file.close();

        QDomElement profileNode = profileXML.createElement("Player");
        profileNode.setAttribute("name", player->name());
        profileNode.setAttribute("avatar", player->avatarPath());

        QDomElement aux = rootNode.firstChild().toElement();
        if (profileNode.attribute("name").toUpper() == aux.attribute("name").toUpper())
            return false;

        if (profileNode.attribute("name").toUpper() < aux.attribute("name").toUpper())
            rootNode.insertBefore(profileNode, aux);
        else
        {
            while (profileNode.attribute("name").toUpper() > aux.attribute("name").toUpper() && !aux.nextSibling().isNull())
            {
                aux = aux.nextSibling().toElement();
            }
            if (profileNode.attribute("name").toUpper() == aux.attribute("name").toUpper())
                return false;
            else if (profileNode.attribute("name").toUpper() > aux.attribute("name").toUpper())
                rootNode.insertAfter(profileNode, aux);
            else
                rootNode.insertBefore(profileNode, aux);
        }

        if (!file.open(QFile::WriteOnly))
            return false;
        QTextStream ts(&file);
        ts << profileXML.toString();
        file.close();
    }

    //    updateListProfiles();

    return true;
}

bool ProfilesController::createProfile(const QString &name, const QString &avatarPath) {
    debugMsg;
    PlayerModel *newPlayer = new PlayerModel();
    newPlayer->setName(name);
    newPlayer->setAvatarPath(avatarPath);

    if(createProfile(newPlayer))
        return true;
    return false;
}

bool ProfilesController::existAnyProfile()
{
    debugMsg;
    QDomDocument profileXML("Profiles");
    QFile file("profiles.xml");
    QList<PlayerModel*> profilesList;

    //verifies if the file exists
    if (file.exists())
    {
        if (file.open(QFile::ReadOnly))
        {
            //verifies if the content of the file is a xml
            if (!profileXML.setContent(&file))
            {
                file.close();
            }
            QDomElement rootNode = profileXML.firstChildElement("Profiles");
            file.close();
            QDomElement profileNode = rootNode.firstChild().toElement();
            if(!profileNode.isNull())
            {
                return true;
            } else {
                return false;
            }
        }
    }
    else
    {
        qDebug() << "File not exists";
        return false;
    }
}

void ProfilesController::updateListProfiles()
{
    debugMsg;
    QDomDocument profileXML("Profiles");
    QFile file("profiles.xml");
    QList<PlayerModel*> profilesList;

    //verifies if the file exists
    if (file.exists())
    {
        if (file.open(QFile::ReadOnly))
        {
            //verifies if the content of the file is a xml
            if (!profileXML.setContent(&file))
            {
                file.close();
            }
            QDomElement rootNode = profileXML.firstChildElement("Profiles");
            file.close();
            QDomElement profileNode = rootNode.firstChild().toElement();
            while (!profileNode.isNull())
            {
                PlayerModel *player = new PlayerModel();
                player->setName(profileNode.attribute("name"));
                player->setAvatarPath(profileNode.attribute("avatar"));
                profilesList.append(player);
                profileNode = profileNode.nextSibling().toElement();
            }
        }
    }
    else
    {
        qDebug() << "File not exists";
    }

    m_playerListModel.clear();
    m_playerListModel.append(convertToObj(profilesList));
    emit playerListModelChanged();
}

bool ProfilesController::editProfile(PlayerModel *oldPlayer, PlayerModel *newPlayer)
{
    debugMsg;
    QDomDocument profileXML("Profiles");
    QFile file("profiles.xml");

    //verifies if the file exists
    if (file.exists())
    {
        if (file.open(QFile::ReadOnly))
        {
            //verifies if the content of the file is a xml
            if (!profileXML.setContent(&file))
            {
                file.close();
                return false;
            }
            QDomElement rootNode = profileXML.firstChildElement("Profiles");
            file.close();
            QDomElement profileNode = rootNode.firstChild().toElement();
            while (!profileNode.isNull() && profileNode.attribute("name") != oldPlayer->name())
            {
                profileNode = profileNode.nextSibling().toElement();
            }
            if (!profileNode.isNull())
            {
                profileNode.setAttribute("name", newPlayer->name());
                profileNode.setAttribute("avatar", newPlayer->avatarPath());
                updateListProfiles();
            }
            else
            {
                return false;
            }
            if (!file.open(QFile::WriteOnly))
                return false;
            QTextStream ts(&file);
            ts << profileXML.toString();
            file.close();
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

bool ProfilesController::editProfile(const QString &oldName,const QString &oldAvatarPath, const QString &newName,
                                     const QString &newAvatarPath)
{
    debugMsg;
    PlayerModel *oldPlayer = new PlayerModel();
    oldPlayer->setName(oldName);
    oldPlayer->setAvatarPath(oldAvatarPath);

    PlayerModel *newPlayer = new PlayerModel();
    newPlayer->setName(newName);
    newPlayer->setAvatarPath(newAvatarPath);

    if (editProfile(oldPlayer,newPlayer))
        return true;
    return false;
}

bool ProfilesController::deleteProfile(QObject *player)
{
    debugMsg;
    QDomDocument profileXML("Profiles");
    QFile file("profiles.xml");

    //verifies if the file exists
    if (file.exists())
    {
        if (file.open(QFile::ReadOnly))
        {
            //verifies if the content of the file is a xml
            if (!profileXML.setContent(&file))
            {
                file.close();
                return false;
            }
            QDomElement rootNode = profileXML.firstChildElement("Profiles");
            file.close();
            QDomElement profileNode = rootNode.firstChild().toElement();
            while (!profileNode.isNull() && profileNode.attribute("name") != player->property("name").toString())
            {
                profileNode = profileNode.nextSibling().toElement();
            }
            if (!profileNode.isNull())
            {
                rootNode.removeChild(profileNode);
                //                updateListProfiles();
            }
            else
            {
                return false;
            }
            if (!file.open(QFile::WriteOnly))
                return false;
            QTextStream ts(&file);
            ts << profileXML.toString();
            file.close();
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

QList<QObject *> ProfilesController::playerListModel()
{
    QObjectList playerModel;
    int i = 0;

    for (; i < 4 && i < m_playerListModel.length(); i++)
        playerModel.append(m_playerListModel[i]);
     //playerModel.append(m_playerListModel);
    for (; i < 4; i++)
     playerModel.append(new QObject());

//    debugMsg << playerModel.length() << playerModel;

    return playerModel;
}

//bool ProfilesController::deleteProfile(const QString &name) {
//    Player *playerToDelete = new Player();
//    playerToDelete->setName(name);

//    if(deleteProfile(playerToDelete))
//        return true;
//    return false;
//}
