#include <QtCore/QDebug>
#include <QtCore/QEvent>
#include <QtCore/QHash>
#include <QtCore/QProcess>
#include <QtCore/QUrl>

#include <QtGui/QApplication>
#include <QtGui/QDesktopServices>
#include <QtGui/QInputContext>

#include "aboutcontroller.h"

#if defined (TARGET_SYMBIAN)
#include "symbianutils.h"
#elif defined (TARGET_MEEGO)
#include <QSystemInfo>
#include <QSystemDeviceInfo>
QTM_USE_NAMESPACE
#endif


AboutController::AboutController(QObject *parent) :
    QObject(parent),
    m_showUpdateButton(false)
{
}

void AboutController::setUpAboutMessage(){
    QHash<QString,QString> modelList;
    modelList["5230"] = QString("10.4");
    modelList["5233"] = QString("12.1");
    modelList["5235-1d"] = QString("12.8");
    modelList["5530"] = QString("20.0");
    modelList["5800"] = QString("20.0");
    modelList["5800 XpressMusic"] = QString("20.0");
    modelList["X6-00"] = QString("12.2");
    modelList["C5-03"] = QString("11.1.024");
    modelList["N97"] = QString("21.2");
    modelList["N97 mini"] = QString("11.2");
    modelList["C6-00"] = QString("10.2");
    modelList["N8-00"] = QString("011.012");
    modelList["C7-00"] = QString("013.016");
    modelList["E7-00"] = QString("013.016");
    modelList["N9"] = QString("10.2011");
    modelList["N950"] = QString("10.2011");

    QHash<QString,QString> rmList;
    rmList["5230"] = QString("RM-629");
    rmList["5233"] = QString("RM-625");
    rmList["5235-1d"] = QString("RM-629");
    rmList["5530"] = QString("RM-504");
    rmList["5800"] = QString("RM-356");
    rmList["5800 XpressMusic"] = QString("RM-356");
    rmList["X6-00"] = QString("RM-551");
    rmList["C5-03"] = QString("RM-697");
    rmList["N97"] = QString("RM-507");
    rmList["N97 mini"] = QString("RM-553");
    rmList["C6-00"] = QString("RM-624");
    rmList["N8-00"] = QString("RM-596");
    rmList["C7-00"] = QString("RM-675");
    rmList["E7-00"] = QString("RM-626");
    rmList["N9"] = QString("RM-696");
    rmList["N950"] = QString("RM-680");

#if defined (TARGET_SYMBIAN)
    m_model = SymbianUtils::model() + ", " + SymbianUtils::rm();
    m_firmware = SymbianUtils::firmware();
#elif defined(TARGET_MEEGO)
    QtMobility::QSystemInfo s;
    m_firmware = s.version(QtMobility::QSystemInfo::Firmware);

    QtMobility::QSystemDeviceInfo sd;
    m_model = sd.model() + ", " + sd.productName();
#endif
    if (modelList.contains(m_model)) {
        m_homofirmware = modelList[m_model];
        if (QString::compare(m_firmware, m_homofirmware) < 0) {
            m_message =
                    "<p align='justify'>"
                    "A vers&#227;o do software do seu Nokia <strong>" + m_model + "</strong> &#233; a "
                    "<strong>" + m_firmware + "</strong> e esta aplica&#231;&#227;o foi projetada "
                    "para a vers&#227;o <strong>" + modelList[m_model] + "</strong> com o <strong>" + rmList[m_model] + "</strong> e superiores."
                    "</p>"
                    "<p align='justify'>"
                    "Para um bom funcionamento sugerimos a "
                    "atualiza&#231;&#227;o do software de seu aparelho.<br />"
                    "Caso deseje fazer isso agora, pressione o "
                    "bot&#227;o ATUALIZAR e na tela escolha "
                    "OP&#199;&#213;ES e CHECAR ATUALIZA&#199;&#213;ES."
                    "</p>";
            m_showUpdateButton = true;
        }
        else if (QString::compare(m_firmware, m_homofirmware) == 0) {
            m_message =
                    "<p align='justify'>"
                    "A vers&#227;o do software do seu Nokia <strong>" + m_model + "</strong> &#223; a "
                    "<strong>" + m_firmware + "</strong>, vers&#227;o para a qual esta aplica&#231;&#227;o foi projetada."
                    "</p>";
        }
        else {
            m_message =
                    "A vers&#227;o do software do seu Nokia <strong>" + m_model + "</strong> &#233; a "
                    "<strong>" + m_firmware + "</strong>, vers&#227;o acima da qual sua aplica&#231;&#227;o foi projetada, <strong>" + modelList[m_model] + "</strong>.";
        }
    }
    else {
        m_message =
                "<p align='justify'>"
                "Esta aplica&#231;&#227;o n&#227;o foi homologada para o seu aparelho Nokia <strong>" + m_model + "</strong>. Eventuais problemas poder&#227;o ocorrer durante a sua execu&#231;&#227;o."
                "</p>";
    }
//    qDebug()<< __PRETTY_FUNCTION__ <<"aboutMessage";
}

void AboutController::updateSoftware(){
#if defined (TARGET_SYMBIAN)
    SymbianUtils::openWindowByUID(0x101f6de5);
#elif defined(TARGET_MEEGO)
    QProcess::startDetached("/usr/bin/invoker",
                            QStringList() << "--type=e" <<
                            "--single-instance" << "/usr/bin/package-manager-ui");
#endif
}

QString AboutController::getAboutMessage(){
    setUpAboutMessage();
    return m_message;
}

bool AboutController::showUpdateButton(){
    return m_showUpdateButton;
}

void AboutController::downloadApps()
{
    QDesktopServices::openUrl(QUrl(tr("http://bit.ly/atRQmu")));
}

void AboutController::closeMeegoKeyboard()
{
    QInputContext *ic = qApp->inputContext();
    QEvent closeSIPEvent(QEvent::CloseSoftwareInputPanel);
    ic->filterEvent(&closeSIPEvent);
}
