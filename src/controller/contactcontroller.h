#ifndef CONTACTCONTROLLER_H
#define CONTACTCONTROLLER_H

#include <QDeclarativeItem>

class ContactController : public QDeclarativeItem
{
    Q_OBJECT
public:
    explicit ContactController(QDeclarativeItem *parent = 0);
    QString m_contactAboutText;
    Q_INVOKABLE QString getContactsText();

public slots:
    Q_INVOKABLE void openTwitter();
    Q_INVOKABLE void openFacebook();

};

#endif // CONTACTCONTROLLER_H
