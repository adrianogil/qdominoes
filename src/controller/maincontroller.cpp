#include <QtDeclarative/QDeclarativeContext>
#include <QtGui/QApplication>
#include <QtGui/QDesktopWidget>

#include "maincontroller.h"
#include "qmlapplicationviewer.h"

#include "handcontroller.h"
#include "gamecontroller.h"
#include "table/tablecontroller.h"

#include "profilescontroller.h"
#include "aboutcontroller.h"
#include "contactcontroller.h"
#include "player/playermodel.h"

#include "gallerycontroller.h"
#include "galleryimageprovider.h"

#include "appdebug.h"

MainController::MainController(QObject *parent) :
    QObject(parent)
{
}

void MainController::setupUi()
{
    m_viewer = new QmlApplicationViewer;
    m_viewer->setOrientation(QmlApplicationViewer::ScreenOrientationAuto);

    connect(QApplication::desktop(), SIGNAL(resized(int)),
            this, SLOT(onResize()));

    m_profileController = new ProfilesController(this);

//    m_galleryImageProvider = new GalleryImageProvider;
//    m_galleryController = new GalleryController(m_galleryImageProvider, this);

//    m_viewer->engine()->addImageProvider("gallery", m_galleryImageProvider);


#if defined (TARGET_MEEGO)
    EventFilter ef;
    m_viewer->installEventFilter(&ef);
#endif

    m_viewer->rootContext()->setContextProperty("mainController", this);
    m_viewer->rootContext()->setContextProperty("profileController", m_profileController);
    m_viewer->rootContext()->setContextProperty("aboutController", new AboutController());
    m_viewer->rootContext()->setContextProperty("contactController", new ContactController());

    onResize();

    m_viewer->setSource(QUrl(QLatin1String("qrc:/src/qml/qdominoes/Main.qml")));
    m_viewer->showExpanded();
}

void MainController::startGame()
{
    m_gameController = new GameController(this);
    m_viewer->rootContext()->setContextProperty("gameController", m_gameController);

    connect(m_gameController, SIGNAL(gameStarted()),
            this, SIGNAL(gameStarted()));

    m_gameController->startGame();
}

void MainController::onResize()
{
    int screenWidth = QApplication::desktop()->width();
    int screenHeight = QApplication::desktop()->height();

#ifdef TARGET_DESKTOP
    screenWidth = 854;
    screenHeight = 480;
#endif

    m_viewer->rootContext()->setContextProperty("screenWidth",screenWidth);
    m_viewer->rootContext()->setContextProperty("screenHeight",screenHeight);
}
