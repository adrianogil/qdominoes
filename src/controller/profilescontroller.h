#ifndef PROFILESHANDLER_H
#define PROFILESHANDLER_H

#include <QtCore/QList>
#include <QtCore/QObject>

//#include "profilecard.h"
//#include "xmlhandler.h"

#include "qtsynthesize.h"

class PlayerModel;

class ProfilesController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QList<QObject*> playerListModel READ playerListModel NOTIFY playerListModelChanged)

public:
    explicit ProfilesController(QObject *parent = 0);

    static int chosenProfile;

    bool createProfile(PlayerModel *player);

    QList<QObject*> playerListModel();

    Q_INVOKABLE bool existAnyProfile();
    Q_INVOKABLE bool createProfile(const QString &name, const QString &avatarPath);
    Q_INVOKABLE void updateListProfiles();
    bool editProfile(PlayerModel *oldPlayer, PlayerModel *newPlayer);
    Q_INVOKABLE bool editProfile(const QString &oldName,const QString &oldAvatarPath,
                                 const QString &newName, const QString &newAvatarPath);
    Q_INVOKABLE bool deleteProfile(QObject *player);
    //Q_INVOKABLE bool deleteProfile(const QString &name);

    static QList<QObject*> convertToObj(QList<PlayerModel*> playersList);

signals:
    void playerListModelChanged();

public slots:

private:
    QList<QObject*> m_playerListModel;

};

#endif // PROFILESCONTROLLER_H
