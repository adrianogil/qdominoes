#include "contactcontroller.h"
#include <QtCore/QUrl>
#include <QtGui/QDesktopServices>

ContactController::ContactController(QDeclarativeItem *parent) :
    QDeclarativeItem(parent),
    m_contactAboutText("")
{
    setFlag(QGraphicsItem::ItemHasNoContents,false);
}

QString ContactController::getContactsText()
{


    m_contactAboutText =
                    "<html>"
                        "<head>"
                            "<meta http-equiv=\"Content-Type\" content=\"text/html\" charset=\"ISO-8859-1\">"
                        "</head>"
                        "<body>"
                            "<p align='justify'>"
                                "Saiba mais sobre os aplicativos desenvolvidos pelo CETELI atrav&#233;s do perfil do Projeto Tukanos no Twitter e Facebook."
                            "</p>"
                            "<p align='justify'>"
                                "Acompanhe nossos canais de comunica&#231;&#227;o para estar atualizado sobre novidades e lan&#231;amentos, enviar sugest&#245;es e muito mais. Qualquer d&#250;vida &#233; s&#243; falar com a gente!"
                            "</p>"
                        "</body>"
                    "</html>"
                    ;
    return m_contactAboutText;
}

void ContactController::openTwitter()
{
    QDesktopServices::openUrl(QUrl(tr("http://mobile.twitter.com/projetoTukanos")));
}

void ContactController::openFacebook()
{
    QDesktopServices::openUrl(QUrl(tr("http://m.facebook.com/projetotukanos")));
}
