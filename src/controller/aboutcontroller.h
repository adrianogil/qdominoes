#ifndef ABOUTCONTROLLER_H
#define ABOUTCONTROLLER_H

#include <QObject>

class AboutController : public QObject
{
    Q_OBJECT
public:
    explicit AboutController(QObject *parent = 0);
    Q_INVOKABLE void setUpAboutMessage();
    Q_INVOKABLE QString getAboutMessage();
    Q_INVOKABLE void updateSoftware();
    Q_INVOKABLE bool showUpdateButton();

private:
    QString m_model;
    QString m_firmware;
    QString m_homofirmware;
    QString m_message;
    bool m_showUpdateButton;

public slots:
    void closeMeegoKeyboard();
    Q_INVOKABLE void downloadApps();

};

#endif // ABOUTCONTROLLER_H
