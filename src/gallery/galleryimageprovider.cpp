#include "galleryimageprovider.h"

#include "appdebug.h"

QImage GalleryImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    int index = id.toInt();

    debugMsg << id << index << m_pictureList.length();

    if (index >= 0 && index < m_pictureList.length()) {
        PictureData* data = m_pictureList.at(index);
        QImage image = data->image;
        debugMsg << index << data->path << image.size().height() << image.size().width();
        return image;
    }

    QImage img;
    return img;
}
