/*
 * Copyright (c) 2009 Nokia Corporation.
 */

#ifndef PICTURELOADERTHREAD_H
#define PICTURELOADERTHREAD_H

#include <QThread>
#include <QImageReader>
#include <QSize>
#include <QImage>
#include <QMetaType>
#include <QPixmap>
#include <QMutex>
#include <QWaitCondition>

class PictureData
{
public:
    PictureData()
    {
        size = QSizeF(0, 0);
    }
    QString path;
    QSizeF size;
    QPixmap p;
    int indexOnScreen;
    QImage image;
};

// This macro makes the type PictureData known to QMetaType
Q_DECLARE_METATYPE( PictureData)

class PictureLoaderThread: public QThread
{
Q_OBJECT

public:
    PictureLoaderThread(QObject *parent = 0);
    ~PictureLoaderThread();

    void loadImages(QList<PictureData*>&);
    void loadImage(PictureData&);
    QSize imageScaledSize(const QSize& preferredSize, QString file);

protected:
    void run();

signals:
    void imageLoaded(QImage*, PictureData*);
    
private:
    QList<PictureData*> m_loadingCache;
    QImageReader m_imageReader;
    int m_tid;
    QMutex m_mutex;
    QWaitCondition m_waitCondition;
    bool m_sleep;
};

#endif // PICTURELOADERTHREAD_H
