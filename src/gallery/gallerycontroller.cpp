/*
 * Copyright (c) 2009 Nokia Corporation.
 */
#include <QtGui/QDesktopServices>
#include <QtCore/QDir>
#include "galleryimageprovider.h"
#include "gallerycontroller.h"
#include "PictureLoaderThread.h"
#include "appdebug.h"

GalleryController::GalleryController(GalleryImageProvider *imageProvider, QObject *parent)
    : QObject(parent)
    , m_galleryImageProvider(imageProvider)
{
    m_galleryState = ENone;

    // Picture loader thread
    m_pictureLoaderThread = new PictureLoaderThread(this);
    qRegisterMetaType<PictureData>("PictureData");
    qRegisterMetaType<QImage>("QImage");
    QObject::connect(m_pictureLoaderThread, SIGNAL(imageLoaded(QImage*,PictureData*)),
                     this, SLOT(imageLoaded(QImage*,PictureData*)));

    m_pictureIndex = 1;
    m_startAnimCounter = 0;
    m_tid_startAnimation = 0;
    m_tid_showThumbnailPics = 0;
    m_tid_loadPicturePaths = 0;
    m_leftDirection = true;

    // Start animation...
    m_tid_loadPicturePaths = startTimer(3000);

    searchPicPaths();
    loadThumbnailPictures();
}

GalleryController::~GalleryController()
{
    PictureData * key;
    foreach(key,m_pictureDataList) {
            delete key;
        }
    m_pictureDataList.clear();
}

void GalleryController::timerEvent(QTimerEvent *event)
{
    if (m_tid_loadPicturePaths == event->timerId()) {
        killTimer(m_tid_loadPicturePaths);
        m_tid_loadPicturePaths = 0;
        // Search available images
        // searchPicPaths();
        // Show thumbnail data
        showThumbnailData(1);
    }
}

void GalleryController::showThumbnailData(int firstPicIndex)
{
    debugMsg << firstPicIndex;
    m_galleryState = ThumbDataAnimation;
    m_pictureIndex = firstPicIndex; // First picture index to show

    // Show thumbnail data

    // Calculate previous pictures index if needed
    if (!m_leftDirection) {
        // Previous pictures
        int div = (m_pictureIndex - 1) % 6;
        if (div > 0) {
            // some pic
            m_pictureIndex = m_pictureIndex - 6 - div;
        }
        else {
            // whole six coming
            m_pictureIndex = m_pictureIndex - 6 - 6;
        }
    }

    // No more pictures, show last 6
    if (m_pictureIndex > m_pictureDataList.count()) {
        int div = m_pictureDataList.count() % 6;
        if (div>0)
            m_pictureIndex = m_pictureDataList.count() - div + 1;
        else
            m_pictureIndex = m_pictureDataList.count()-5;
    }

    // For sure
    if (m_pictureIndex < 1) {
        m_pictureIndex = 1;
    }

    int counter = 0;
    QList<PictureData*> pictureList;
    while (m_pictureIndex + counter <= m_pictureDataList.count() && counter < 6) {
        // Update picture data: size, indexOnScreen
        PictureData* data = m_pictureDataList[m_pictureIndex - 1 + counter];
        data->indexOnScreen = counter + 1;
        // Scale image size (not image) to near preferred image size
        if (data->size.width() == 0) {
            data->size = m_pictureLoaderThread->imageScaledSize(m_thumbnailSize, data->path);
        }
        // Add thumbnail without picture to scene
        pictureList.append(data);
        counter++;
    }
    debugMsg << pictureList;
    m_galleryImageProvider->setPictureList(pictureList);
}

void GalleryController::searchImages(QString path)
{
    // Read all JPG JPEG image paths from the given path
    // Read images from the subfolders
    if (path.isNull()) {
        path = QDesktopServices::storageLocation(QDesktopServices::PicturesLocation);
    }
    QDir dir;
    dir.setFilter(QDir::Files | QDir::Dirs);
    dir.setPath(path);

    QFileInfoList list = dir.entryInfoList();
    for (int i = 0; i < list.size(); ++i) {
        QFileInfo fileInfo = list.at(i);
        qDebug() << fileInfo.filePath();
        if (fileInfo.isFile()) {
            QString s = fileInfo.filePath();
            if (s.indexOf(QString(".jpg"), 0, Qt::CaseInsensitive) > 0) {
                PictureData* data = new PictureData;
                data->path = s;
                data->p = 0;
                m_pictureDataList.append(data);
            }
            else if (s.indexOf(QString(".jpeg"), 0, Qt::CaseInsensitive) > 0) {
                PictureData* data = new PictureData;
                data->path = s;
                data->p = 0;
                m_pictureDataList.append(data);
            }
        }
        else {
            QString s = fileInfo.filePath().mid(fileInfo.filePath().length() - 1, 1);
            if (!s.contains(".") && !s.contains(".."))
                searchImages(fileInfo.filePath());
        }
    }
}

void GalleryController::loadThumbnailPictures()
{
    debugMsg;
    m_galleryState = NewThumbPicAnimation;

    // How many thymbs we are waiting
    m_thumbLoadLoopCount = m_pictureDataList.count() - (m_pictureIndex - 1);
    if (m_thumbLoadLoopCount>6)
        m_thumbLoadLoopCount=6;

    // Load images for thumbnail
    int counter = 0;
    while (m_pictureIndex <= m_pictureDataList.count()) {
        PictureData* data = m_pictureDataList[m_pictureIndex - 1];
        m_pictureLoaderThread->loadImage(*data);
        m_pictureIndex++;
        counter++;
        if (counter == 6)
            break;
    }
}

void GalleryController::imageLoaded(QImage* image, PictureData* data)
{
    debugMsg << data->path << image->size().width() << image->size().height();
    QPixmap pic = QPixmap::fromImage(*image);
    data->p = pic;
    data->size = pic.size();
    data->image = *image;

    // One loaded
    m_thumbLoadLoopCount--;

}

void GalleryController::searchPicPaths()
{
#ifdef Q_OS_SYMBIAN
    QString path = QDesktopServices::storageLocation(QDesktopServices::PicturesLocation);
    // Search from default picture folder c:/data/images
    searchImages(path);
    // Search from E drive
    path = "E:/images";
    searchImages(path);
#else
    QString path = QDesktopServices::storageLocation(QDesktopServices::PicturesLocation);
    searchImages(path);
#endif
}

void GalleryController::animationsDone()
{
    if (m_galleryState == StartAnimation) {
        if (m_startAnimCounter == 4) {
            // Load picture paths and show first empty thumb pics
            m_tid_loadPicturePaths = this->startTimer(100);
        }
    }
    else if (m_galleryState == ThumbDataAnimation) {
        // Load picture for the thumbnail
        m_tid_showThumbnailPics = this->startTimer(0);
    }
    else if (m_galleryState == NewThumbPicAnimation) {
        if (m_thumbLoadLoopCount==0) {
            m_galleryState = ENone;
        }
    }
    else if (m_galleryState == OldThumbPicAnimation) {
        showThumbnailData(m_pictureIndex);
    }
}
