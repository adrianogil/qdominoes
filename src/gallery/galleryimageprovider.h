#ifndef GALLERYIMAGEPROVIDER_H
#define GALLERYIMAGEPROVIDER_H

#include <qdeclarativeextensionplugin.h>
#include <qdeclarativeengine.h>
#include <qdeclarative.h>
#include <qdeclarativeitem.h>
#include <qdeclarativeimageprovider.h>
#include <qdeclarativeview.h>
#include <QImage>
#include "qtsynthesize.h"

#include "PictureLoaderThread.h"

class GalleryImageProvider : public QDeclarativeImageProvider
{
    Q_NSYNTHESIZE(QList<PictureData*>, pictureList, PictureList)
public:
    GalleryImageProvider(): QDeclarativeImageProvider(QDeclarativeImageProvider::Image){}
    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize);
};

#endif // GALLERYIMAGEPROVIDER_H
