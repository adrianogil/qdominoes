/*
 * Copyright (c) 2009 Nokia Corporation.
 */

#ifndef GALLERYCONTROLLER_H
#define GALLERYCONTROLLER_H

#include <QtCore/QObject>
#include <QtCore/QMutex>
#include <QtCore/QPoint>
#include <QtCore/QSize>
#include <QtGui/QImage>

#ifdef Q_OS_SYMBIAN
#include "symbianpicloader.h"
#endif

class GalleryImageProvider;
class PictureLoaderThread;
class PictureData;

class GalleryController: public QObject
{
    Q_OBJECT

public:
    GalleryController(GalleryImageProvider *imageProvider, QObject *parent = 0);
    ~GalleryController();

protected:
    void timerEvent(QTimerEvent *event);

private:
    void searchPicPaths();
    void searchImages(QString path = 0);
    void showThumbnailData(int firstPicIndex = 0);
    void loadThumbnailPictures();

public slots:
    void imageLoaded(QImage*, PictureData*);
    void animationsDone();

private:

    enum GalleryState
    {
        ENone,
        StartAnimation,
        ThumbDataAnimation,
        NewThumbPicAnimation,
        OldThumbPicAnimation,
        BigPicState
    };

    GalleryImageProvider *m_galleryImageProvider;
    
    PictureLoaderThread* m_pictureLoaderThread;
    QList<PictureData*> m_pictureDataList;
    GalleryState m_galleryState;
    QSize m_thumbnailSize;
    int m_pictureIndex;
    int m_startAnimCounter;

    int m_tid_startAnimation;
    int m_tid_loadPicturePaths;
    int m_tid_showThumbnailPics;

    QPoint m_mouseDown;
    QPoint m_mouseUp;
    bool m_leftDirection;

    QMutex m_mutex;

    int m_thumbLoadLoopCount;
};

#endif // GALLERYCONTROLLER_H
