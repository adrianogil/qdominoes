/*
 * Copyright (c) 2009 Nokia Corporation.
 */

#include <QTimerEvent>
#include "PictureLoaderThread.h"
#include <QDebug>

#include "appdebug.h"

PictureLoaderThread::PictureLoaderThread(QObject *parent) :
    QThread(parent)
{
    start(IdlePriority);
}

PictureLoaderThread::~PictureLoaderThread()
{
    m_loadingCache.clear();
}

QSize PictureLoaderThread::imageScaledSize(const QSize& preferredSize, QString file)
{
    // Set image name
    QImageReader* reader = new QImageReader();
    reader->setFileName(file);
    // Set/scale image size into preferred size
    QSize imageSize = reader->size();
    imageSize.scale(preferredSize, Qt::KeepAspectRatio);
    delete reader;
    return imageSize;
}

void PictureLoaderThread::loadImages(QList<PictureData*>& data)
{
    QMutexLocker locker(&m_mutex);

    m_loadingCache << data;

    m_waitCondition.wakeAll();
}


void PictureLoaderThread::loadImage(PictureData& d)
{
    QMutexLocker locker(&m_mutex);

    m_loadingCache.append(&d);

    m_waitCondition.wakeAll();
}

void PictureLoaderThread::run()
{
    forever {

        while(!m_loadingCache.isEmpty()) {
            PictureData* d = m_loadingCache.takeFirst();

            // Read image
            QImageReader* reader = new QImageReader();
            // Set image name
            reader->setFileName(d->path);
            // Set/scale image size into screen
            QSize imageSize = reader->size();
            imageSize.scale(d->size.toSize(), Qt::KeepAspectRatio);
            reader->setScaledSize(imageSize);
            // Read image
            QImage* image = new QImage();
            reader->read(image);
            debugMsg << image << d->path
                     << image->width() << image->height()
                     << reader->errorString();
            delete reader;
            m_mutex.lock();
            emit imageLoaded(image, d);
            m_mutex.unlock();
        }

        m_mutex.lock();
        m_waitCondition.wait(&m_mutex);
        m_mutex.unlock();
    }
}

