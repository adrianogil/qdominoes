/*
 * Copyright (c) 2009 Nokia Corporation.
 */

#ifndef QANIMATEDGALLERY_H
#define QANIMATEDGALLERY_H

#include <QtGui>
#include "PictureLoaderThread.h"

#ifdef Q_OS_SYMBIAN
#include "symbianpicloader.h"
#endif

class GalleryGraphicsScene;
class GalleryGraphicsView;

//#ifdef Q_OS_SYMBIAN
//class QAnimatedGallery: public QMainWindow, public MSymbianPicLoader
//#else
class QAnimatedGallery: public QMainWindow
//#endif

{
    Q_OBJECT

public:
    QAnimatedGallery(QWidget *parent = 0);
    ~QAnimatedGallery();

    void resizeEvent(QResizeEvent*);
    void timerEvent(QTimerEvent *);
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);

    GalleryGraphicsScene* getScene();

private:
    void showStartAnimation();
    void searchPicPaths();
    void searchImages(QString path = 0);
    void showThumbnailData(int firstPicIndex = 0);
    void loadThumbnailPictures();
//#ifdef Q_OS_SYMBIAN
//    void loadExifThumbnailPictures();
//#endif

//#ifdef Q_OS_SYMBIAN
//    // MSymbianPicLoader
//    void thumbLoaded(QPixmap, PictureData*);
//#endif

public slots:
    void imageLoaded(QImage*, PictureData*);
    void animationsDone();
    void animationDone(int);

private:

    enum GalleryState
    {
        ENone,
        StartAnimation,
        ThumbDataAnimation,
        NewThumbPicAnimation,
        OldThumbPicAnimation,
        BigPicState
    };

    GalleryGraphicsScene* m_scene;
    GalleryGraphicsView * m_view;
    
    PictureLoaderThread* m_pictureLoaderThread;
    QList<PictureData*> m_pictureDataList;
    GalleryState m_galleryState;
    QSize m_thumbnailSize;
    int m_pictureIndex;
    int m_startAnimCounter;

    int m_tid_startAnimation;
    int m_tid_loadPicturePaths;
    int m_tid_showThumbnailPics;

    QPoint m_mouseDown;
    QPoint m_mouseUp;
    bool m_leftDirection;

    QMutex m_mutex;

    int m_thumbLoadLoopCount;

//#ifdef Q_OS_SYMBIAN
//    SymbianPicLoader* m_symbianPicLoader;
//#endif

};

#endif // QANIMATEDGALLERY_H
