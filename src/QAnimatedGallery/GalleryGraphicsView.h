/*
 * Copyright (c) 2009 Nokia Corporation.
 */

#ifndef GALLERYGRAPHICSVIEW_H
#define GALLERYGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QGraphicsScene>

class GalleryGraphicsView: public QGraphicsView
{
Q_OBJECT

public:
    GalleryGraphicsView(QWidget* parent = 0);
    GalleryGraphicsView(QGraphicsScene* scene, QWidget* parent = 0);    
    ~GalleryGraphicsView();

    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    
signals:
    void pointerPressed(QPointF);
    
private:
    QPointF m_mouseDown;
    QPointF m_mouseUp;
    
};

#endif // GALLERYGRAPHICSVIEW_H
