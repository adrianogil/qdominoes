/*
 * Copyright (c) 2009 Nokia Corporation.
 */

#ifndef SYMBIANPICLOADER_H
#define SYMBIANPICLOADER_H

#include "PictureLoaderThread.h"
#include <e32std.h>
#include <e32base.h>
#include <f32file.h>
//#include <ImageConversion.h>

class MSymbianPicLoader
{
public:
    // Sends exif thumbnail to the listener
    virtual void thumbLoaded(QPixmap, PictureData*) = 0;
};

class SymbianPicLoader: public CActive
{
public:
    SymbianPicLoader(MSymbianPicLoader*);
    ~SymbianPicLoader();
    
    // Loads exif thumbnail from the jpeg picture
    void loadImage(PictureData*);

private:
//    void convertImage();

protected:
    // From CActive
    void DoCancel();
    void RunL();
    TInt RunError(TInt aError);

private:
    RFs m_Fs;
    HBufC8* m_thumbnail;
    HBufC8* m_exif;
    CFbsBitmap* m_cfbsBitmap;
//    CImageDecoder* m_imageDecoder;
    PictureData* m_pictureData;
    MSymbianPicLoader* m_observer;

};

#endif // SYMBIANPICLOADER_H
