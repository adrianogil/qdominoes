/*
 * Copyright (c) 2009 Nokia Corporation.
 */

#ifndef GALLERYGRAPHICSSCENE_H
#define GALLERYGRAPHICSSCENE_H

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QMap>

class PictureItem;
class AnimationHandler;
class PictureData;

class GalleryGraphicsScene: public QGraphicsScene
{
    Q_OBJECT

public:
    GalleryGraphicsScene(QObject *parent = 0);
    ~GalleryGraphicsScene();

    int randInt(int low, int high);

    void setPreferredThumbSize(QSize);

    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

    void addStartAnimation(int animCounter);
    void addThumbnailData(PictureData& data);
    bool addThumbnail(PictureData& data);
    bool addBigPic(PictureData& data);
    bool moveCurrentThumbs(bool leftDirection);

    PictureItem* getTopMostPictureitem();
    PictureData* getPicData(PictureItem*);

public slots:
    void updatesceneRect(QRectF);
    void animationFinished(int);
    void allAnimationsFinished();
    void pointerPressed(QPointF);

    signals:
    void animationsDone();
    void animationDone(int);
    void picSelected(PictureItem *item);

private:
    void calculateThumbPosition(QPoint& result, const QSizeF& thumbSize, int index);
    qreal getItemsTopMostZOrder();

private:
    AnimationHandler* m_animationHandler;
    QMap<PictureData*, PictureItem*> m_pictureToDataMap;

    // Thumbnail positioning
    QSize m_preferredThumbSize;
    int w_thumb_width_cap;
    int w_thumb_height_cap;

    int m_moveThumbCounter;
    bool m_leftDirection;
};

#endif // GALLERYGRAPHICSSCENE_H
