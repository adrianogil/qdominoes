/*
 * Copyright (c) 2009 Nokia Corporation.
 */

#include "GalleryGraphicsScene.h"
#include "PictureItem.h"
#include "AnimationHandler.h"
#include "PictureLoaderThread.h"

#include <QTime>
#include <QDebug>

GalleryGraphicsScene::GalleryGraphicsScene(QObject *parent) :
    QGraphicsScene(parent)
{
    m_animationHandler = new AnimationHandler(this, this);
    // Listen animation finished signals
    QObject::connect(m_animationHandler, SIGNAL(animationFinished(int)),
    this, SLOT(animationFinished(int)));
    QObject::connect(m_animationHandler, SIGNAL(allAnimationsFinished()),
    this, SLOT(allAnimationsFinished()));

    // Create seed for the random
    QTime time = QTime::currentTime();
    qsrand((uint) time.msec());

    // Listen scene size changes
    QObject::connect(this, SIGNAL(sceneRectChanged(QRectF)),
    this, SLOT(updatesceneRect(QRectF)));
}

GalleryGraphicsScene::~GalleryGraphicsScene()
{
    m_pictureToDataMap.clear();
}

void GalleryGraphicsScene::calculateThumbPosition(QPoint& result, const QSizeF& thumbSize,
    int index)
{
    result = QPoint((m_preferredThumbSize.width() - thumbSize.width()) / 2,
        (m_preferredThumbSize.height() - thumbSize.height()) / 2);

    switch (index) {
    case 1:
    {
        result.setX(w_thumb_width_cap + result.x());
        result.setY(w_thumb_height_cap + result.y());
        break;
    }
    case 2:
    {
        result.setX(w_thumb_width_cap + m_preferredThumbSize.width() + w_thumb_width_cap
            + result.x());
        result.setY(w_thumb_height_cap + result.y());
        break;
    }
    case 3:
    {
        result.setX(w_thumb_width_cap + m_preferredThumbSize.width() + w_thumb_width_cap
            + m_preferredThumbSize.width() + w_thumb_width_cap + result.x());
        result.setY(w_thumb_height_cap + result.y());
        break;
    }
    case 4:
    {
        result.setX(w_thumb_width_cap + result.x());
        result.setY(w_thumb_height_cap + m_preferredThumbSize.height() + w_thumb_height_cap
            + result.y());
        break;
    }
    case 5:
    {
        result.setX(w_thumb_width_cap + m_preferredThumbSize.width() + w_thumb_width_cap
            + result.x());
        result.setY(w_thumb_height_cap + m_preferredThumbSize.height() + w_thumb_height_cap
            + result.y());
        break;
    }
    case 6:
    {
        result.setX(w_thumb_width_cap + m_preferredThumbSize.width() + w_thumb_width_cap
            + m_preferredThumbSize.width() + w_thumb_width_cap + result.x());
        result.setY(w_thumb_height_cap + m_preferredThumbSize.height() + w_thumb_height_cap
            + result.y());
        break;
    }
    };
}

void GalleryGraphicsScene::updatesceneRect(QRectF newRect)
{
    int w = newRect.width();
    int h = newRect.height();

    // Calculate thumbnail caps
    w_thumb_height_cap = h * 0.065;
    w_thumb_width_cap = w * 0.05;
}

int GalleryGraphicsScene::randInt(int low, int high)
{
    // Random number between low and high
    return qrand() % ((high + 1) - low) + low;
}

void GalleryGraphicsScene::setPreferredThumbSize(QSize s)
{
    m_preferredThumbSize = s;
}

void GalleryGraphicsScene::addStartAnimation(int animCounter)
{
    Q_UNUSED(animCounter)

    QPixmap p;

    if (animCounter == 1) {
        p.load(":/images/qt1.png");
        p = p.scaled(QSize(150, 150), Qt::KeepAspectRatio, Qt::SmoothTransformation);
    }
    else if (animCounter == 2) {
        p.load(":/images/qt-1codeless.png");
        p = p.scaled(QSize(200, 200), Qt::KeepAspectRatio, Qt::SmoothTransformation);
    }
    else if (animCounter == 3) {
        p.load(":/images/qt-2createmore.png");
        p = p.scaled(QSize(250, 200), Qt::KeepAspectRatio, Qt::SmoothTransformation);
    }
    else {
        p.load(":/images/qt-3deploy.png");
        p = p.scaled(QSize(350, 250), Qt::KeepAspectRatio, Qt::SmoothTransformation);
    }

    PictureItem* picture = new PictureItem(p);
    picture->notNeeded();
    addItem(picture);

    QRect r = this->sceneRect().toRect();
    QPoint start = QPoint((r.width() - p.width()) / 2, 0 - p.height());
    QPoint end = QPoint((r.width() - p.width()) / 2, r.height());

    m_animationHandler->addPicturePositionAnimation(*picture, 1000, start, end,
        QEasingCurve::OutInCirc);
}

void GalleryGraphicsScene::addThumbnailData(PictureData& data)
{
    PictureItem* thumbDraft = new PictureItem(data.size);

    // Store data as key to picture combination into QMap
    m_pictureToDataMap.insert(&data, thumbDraft);

    // Add picture to scene, note that scene owns PictureItem
    addItem(thumbDraft);

    QPoint pos;
    calculateThumbPosition(pos, data.size, data.indexOnScreen);

    // Aminate picture
    QPointF endPoint = sceneRect().bottomRight();
    endPoint = endPoint + QPointF(+50,+50);
    m_animationHandler->addPicturePositionAnimation(*thumbDraft, 600 * m_pictureToDataMap.count(),
        endPoint, pos, QEasingCurve::InOutBack);

}

bool GalleryGraphicsScene::addBigPic(PictureData& data)
{
    if (data.p.isNull())
        return false;

    PictureItem* thumb = m_pictureToDataMap.value(&data);
    if (thumb) {
        thumb->setScale(1.0);
        thumb->setPixmap(data.p);
    }
    return true;
}

bool GalleryGraphicsScene::addThumbnail(PictureData& data)
{
    if (data.p.isNull())
        return false;

    // Find thumbnail data for this picture
    PictureItem* thumbDraft = m_pictureToDataMap.value(&data);
    if (thumbDraft) {
        // Add picture for thumbnail
        PictureItem* picture = new PictureItem(data.p);
        data.p = 0;

        m_pictureToDataMap.remove(&data);
        m_pictureToDataMap.insert(&data, picture);

        // Add picture to scene, note that scene owns PictureItem
        addItem(picture);

        QPoint pos;
        calculateThumbPosition(pos, data.size, data.indexOnScreen);

        // Aminate picture
        picture->setPos(pos);
        m_animationHandler->addPic2PicRotationAnimation(*thumbDraft, *picture, 500);
    }
    return true;
}

void GalleryGraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    event->ignore();

}
void GalleryGraphicsScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    event->ignore();
}
void GalleryGraphicsScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    event->ignore();
}

void GalleryGraphicsScene::pointerPressed(QPointF posf)
{
    // Pointer pointed, no dragged
    // Is some picture selected?
    QGraphicsItem* selected = itemAt(posf);
    if (selected && selected->type() == PictureItem::PictureItemType) {
        if (m_animationHandler->isAnimationsRunning())
            return;

        // Picture selected
        PictureItem* pic = static_cast<PictureItem*> (selected);
        emit picSelected(pic);
//        int scale = pic->storedScale();
//        pic->setZValue(getItemsTopMostZOrder() + 0.1);
//        if (scale == 1.0) {
//            // Scale up
//            QSize scaledNewSize = pic->pixmap().size();
//            QSize s = sceneRect().size().toSize();

//            // Calculate scaling value
//            qreal scaleVal = qreal(s.height()) / qreal(scaledNewSize.height());
//            scaledNewSize.setHeight(scaledNewSize.height() * scaleVal);
//            scaledNewSize.setWidth(scaledNewSize.width() * scaleVal);

//            // Store original size, position and scale value before zooming picture to big
//            pic->storeSizePosScale(pic->pixmap().size(), pic->pos(), scaleVal);
//            // Store thumbnail picture
//            pic->storeThumbPic();

//            // Start scaling animation
//            int x = (s.width() - scaledNewSize.width()) / 2;
//            int y = (s.height() - scaledNewSize.height()) / 2;
//            m_animationHandler->addPictureScaleAndPosAnimation(*pic, 1000, 1.0, scaleVal,
//                pic->pos(), QPointF(x, y), QEasingCurve::OutQuart, 1002);
//        }
//        else {
//            // Scale back to down
//            QSize s;
//            QPointF p;
//            qreal scaleVal;
//            // Get stored original size and position before zooming picture to big
//            pic->getStoredSizePosScale(s, p, scaleVal);
//            pic->setScale(scaleVal);
//            // Use stored thumbnail back to use
//            pic->useStoredThumbPic();

//            // Start scaling animation
//            m_animationHandler->addPictureScaleAndPosAnimation(*pic, 1000, scaleVal, 1.0,
//                pic->pos(), p, QEasingCurve::InQuart, 1003);

//        }
    }
}

qreal GalleryGraphicsScene::getItemsTopMostZOrder()
{
    qreal z = 0;
    QGraphicsItem* item = 0;
    foreach (item, items()) 
        {
            if (item->zValue() > z) {
                z = item->zValue();
            }
        }
    return z;
}

PictureItem* GalleryGraphicsScene::getTopMostPictureitem()
{
    qreal z = 0;
    QGraphicsItem* item = 0;
    QGraphicsItem* ret = 0;
    foreach (item, items()) 
        {
            if (item->zValue() > z) {
                z = item->zValue();
                ret = item;
            }
        }
    if (ret && ret->type() == PictureItem::PictureItemType) {
        return static_cast<PictureItem*> (ret);
    }
    else {
        return 0;
    }
}

PictureData* GalleryGraphicsScene::getPicData(PictureItem* pic)
{
    PictureData* ret = 0;
    ret = m_pictureToDataMap.key(pic);
    return ret;
}

void GalleryGraphicsScene::allAnimationsFinished()
{
    emit animationsDone();
}

void GalleryGraphicsScene::animationFinished(int id)
{
    if (id == 1001) {
        // Group move animation finished
        m_moveThumbCounter--;
        if (m_moveThumbCounter == 0) {
            // All moved
            // Clear moved data
            m_pictureToDataMap.clear();
            // Remove moved thumbs from scene
            QGraphicsItem* i = 0;
            foreach(i,items()) {
                    if (i->type() == PictureItem::PictureItemType) {
                        if (static_cast<PictureItem*> (i)->isNeeded() == false) {
                            removeItem(i);
                            delete i;
                        }
                    }
                }
        }
    }
    else {
        // Emit rest 
        emit animationDone(id);
    }
}

bool GalleryGraphicsScene::moveCurrentThumbs(bool leftDirection)
{
    // Do not move is there is animation running
    if (m_animationHandler->isAnimationsRunning()) {
        return false;
    }

    // Move direction
    m_leftDirection = leftDirection;

    QGraphicsItem* item;
    for (int i = 0; i < items().count(); i++) {
        item = items().at(i);

        // Remove unvisible draftThumbnails
        if (!item->isVisible()) {
            removeItem(item);
            delete item;
            i--;
        }
        else {
            if (item->type() == PictureItem::PictureItemType) {
                if ((static_cast<PictureItem*> (item))->isNeeded() == false) {
                    // Remove pictures marked as not needed
                    removeItem(item);
                    delete item;
                    i--;
                }
                else {
                    // Mark picture thumbs to not needed to be removed later
                    (static_cast<PictureItem*> (item))->notNeeded();
                }
            }
        }
    }

    // Start move animation as a group
    m_moveThumbCounter = 0;
    m_animationHandler->clearGroup();
    PictureItem* pic = 0;
    int counter = 0;
    foreach (item,items()) {
            counter++;
            // Find picture for animation
            if (item->type() == PictureItem::PictureItemType) {
                pic = static_cast<PictureItem*> (item);

                // Move pic to left/right
                if (m_leftDirection) {
                    if (pic) {
                        m_animationHandler->addPictureToGroupPosAnimation(*pic, 200, 300 * counter,
                            pic->pos(), QPoint(pic->boundingRect().size().toSize().width()*-1, pic->pos().y() - 50), QEasingCurve::InOutBack,
                            1001);
                    }
                }
                else {
                    if (pic) {
                        m_animationHandler->addPictureToGroupPosAnimation(*pic, 200, 300 * counter,
                            pic->pos(), QPoint(sceneRect().width() + pic->boundingRect().size().toSize().width(), pic->pos().y() - 50),
                            QEasingCurve::InOutBack, 1001);
                    }
                }

                m_moveThumbCounter++;
            }
        }
    m_animationHandler->startGroupAnimation();
    return true;
}

