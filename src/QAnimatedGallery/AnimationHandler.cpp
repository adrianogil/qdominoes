/*
 * Copyright (c) 2009 Nokia Corporation.
 */

#include "AnimationHandler.h"
#include "PictureItem.h"
#include "GalleryGraphicsScene.h"

#include <QPropertyAnimation>
#include <QParallelAnimationGroup>
#include <QDebug>

/*****************************************************************************
 * AnimationEvent
 */
AnimationEvent::AnimationEvent()
{
}

AnimationEvent::AnimationEvent(QPropertyAnimation* event, const AnimationType& type, int delay,
    int id)
{
    adelay = delay;
    aid = id;
    animation = event;
    animationType = type;
}

/*****************************************************************************
 * AnimationHandler
 */
AnimationHandler::AnimationHandler(GalleryGraphicsScene* scene, QObject *parent) :
    QObject(parent)
{
    m_scene = scene;
    groupTid = 0;
}

AnimationHandler::~AnimationHandler()
{
    animationsRunning.clear();
    m_animationGroup.clear();
}

void AnimationHandler::clearGroup()
{
    m_animationGroup.clear();
}

void AnimationHandler::addPictureToGroupPosAnimation(PictureItem& picture, int delay, int duration,
    const QPointF& start, const QPointF& end, const QEasingCurve& easing, int id)
{
    QPropertyAnimation* propertyAnimation = new QPropertyAnimation(&picture, "pos");
    propertyAnimation->setDuration(duration);
    propertyAnimation->setStartValue(start);
    propertyAnimation->setEndValue(end);
    propertyAnimation->setEasingCurve(easing);

    QObject::connect(propertyAnimation, SIGNAL(finished()), this, SLOT(finished()));

    m_animationGroup.append(new AnimationEvent(propertyAnimation, MovePic, delay, id));
}

void AnimationHandler::startGroupAnimation()
{
    groupTid = this->startTimer(0);
}

void AnimationHandler::doGroupAnimation()
{
    if (m_animationGroup.count() > 0) {
        AnimationEvent* e = m_animationGroup.last();
        m_animationGroup.removeLast();

        QObject::connect(e->animation, SIGNAL(finished()), this, SLOT(finished()));
        animationsRunning.append(e);

        e->animation->start(QAbstractAnimation::DeleteWhenStopped);
        groupTid = startTimer(e->adelay); // delay to start next animation for the group
    }
}

void AnimationHandler::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == groupTid) {
        killTimer(groupTid);
        groupTid = 0;
        doGroupAnimation();
    }
}

void AnimationHandler::addPicturePositionAnimation(PictureItem& picture, int duration,
    const QPointF& start, const QPointF& end, const QEasingCurve& easing, int id)
{
    QPropertyAnimation* propertyAnimation = new QPropertyAnimation(&picture, "pos");
    propertyAnimation->setDuration(duration);
    propertyAnimation->setStartValue(start);
    propertyAnimation->setEndValue(end);
    propertyAnimation->setEasingCurve(easing);

    QObject::connect(propertyAnimation, SIGNAL(finished()), this, SLOT(finished()));
    animationsRunning.append(new AnimationEvent(propertyAnimation, MovePic, 0, id));

    propertyAnimation->start(QAbstractAnimation::DeleteWhenStopped);
}

void AnimationHandler::addPictureRotationAnimation(PictureItem& picture, int duration, int angle,
    const QEasingCurve& easing, int id)
{
    QPropertyAnimation* propertyAnimation = new QPropertyAnimation(&picture, "rotationAngleY");
    propertyAnimation->setDuration(duration);
    propertyAnimation->setStartValue(0);
    propertyAnimation->setEndValue(angle);
    propertyAnimation->setEasingCurve(easing);

    QObject::connect(propertyAnimation, SIGNAL(finished()), this, SLOT(finished()));
    animationsRunning.append(new AnimationEvent(propertyAnimation, RotatePic, 0, id));

    propertyAnimation->start(QAbstractAnimation::DeleteWhenStopped);
}

void AnimationHandler::addPictureScaleAnimation(PictureItem& picture, int duration, qreal start,
    qreal end, const QEasingCurve& easing, int id)
{
    QPropertyAnimation* propertyAnimation = new QPropertyAnimation(&picture, "scale");
    propertyAnimation->setDuration(duration);
    propertyAnimation->setStartValue(start);
    propertyAnimation->setEndValue(end);
    propertyAnimation->setEasingCurve(easing);

    QObject::connect(propertyAnimation, SIGNAL(finished()), this, SLOT(finished()));
    animationsRunning.append(new AnimationEvent(propertyAnimation, ScalePic, 0, id));

    propertyAnimation->start(QAbstractAnimation::DeleteWhenStopped);
}

void AnimationHandler::addPictureScaleAndPosAnimation(PictureItem& picture, int duration,
    qreal scaleStart, qreal scaleEnd, const QPointF& start, const QPointF& end,
    const QEasingCurve& easing, int id)
{
    QParallelAnimationGroup *group = new QParallelAnimationGroup;

    QPropertyAnimation* propertyAnimation = new QPropertyAnimation(&picture, "scale");
    propertyAnimation->setDuration(duration);
    propertyAnimation->setStartValue(scaleStart);
    propertyAnimation->setEndValue(scaleEnd);
    propertyAnimation->setEasingCurve(easing);
    QObject::connect(propertyAnimation, SIGNAL(finished()), this, SLOT(finished()));
    animationsRunning.append(new AnimationEvent(propertyAnimation, ScaleAndPocPic, 0, id));
    group->addAnimation(propertyAnimation);

    QPropertyAnimation* propertyAnimation2 = new QPropertyAnimation(&picture, "pos");
    propertyAnimation2->setDuration(duration);
    propertyAnimation2->setStartValue(start);
    propertyAnimation2->setEndValue(end);
    propertyAnimation2->setEasingCurve(easing);
    QObject::connect(propertyAnimation2, SIGNAL(finished()), this, SLOT(finished()));
    animationsRunning.append(new AnimationEvent(propertyAnimation2, ScaleAndPocPic, 0, id + 1000));
    group->addAnimation(propertyAnimation2);

    group->start();
}

void AnimationHandler::addPic2PicRotationAnimation(PictureItem& from, PictureItem& to,
    int duration, int id)
{
    AnimationEvent* event = new AnimationEvent();
    event->animationType = RotatePic2Pic;
    event->aid = id;

    QPropertyAnimation* propertyAnimation1 = new QPropertyAnimation(&from, "rotationAngleY");
    propertyAnimation1->setDuration(duration);
    propertyAnimation1->setStartValue(0);
    propertyAnimation1->setEndValue(90);
    propertyAnimation1->setEasingCurve(QEasingCurve::InCirc);
    to.setVisible(true);
    event->animation = propertyAnimation1;
    QObject::connect(propertyAnimation1, SIGNAL(finished()), this, SLOT(finished()));

    QPropertyAnimation* propertyAnimation2 = new QPropertyAnimation(&to, "rotationAngleY");
    propertyAnimation2->setDuration(duration);
    propertyAnimation2->setStartValue(-90);
    propertyAnimation2->setEndValue(0);
    propertyAnimation2->setEasingCurve(QEasingCurve::OutBack);
    to.setVisible(false);
    event->animation2 = propertyAnimation2;
    QObject::connect(propertyAnimation2, SIGNAL(finished()), this, SLOT(finished()));

    animationsRunning.append(event);
    propertyAnimation1->start(QAbstractAnimation::DeleteWhenStopped);
}

void AnimationHandler::addPictureFadingAnimation(PictureItem& picture, int duration, bool fadeIn,
    int id)
{
    QPropertyAnimation* propertyAnimation = new QPropertyAnimation(&picture, "fade");
    propertyAnimation->setDuration(duration);
    if (fadeIn) {
        propertyAnimation->setStartValue(255);
        propertyAnimation->setEndValue(0);
        animationsRunning.append(new AnimationEvent(propertyAnimation, FadeInPic, 0, id));
    }
    else {
        propertyAnimation->setStartValue(0);
        propertyAnimation->setEndValue(255);
        animationsRunning.append(new AnimationEvent(propertyAnimation, FadeOutPic, 0, id));
    }
    propertyAnimation->setEasingCurve(QEasingCurve::Linear);

    QObject::connect(propertyAnimation, SIGNAL(finished()), this, SLOT(finished()));

    propertyAnimation->start(QAbstractAnimation::DeleteWhenStopped);
}

bool AnimationHandler::isAnimationsRunning()
{
    if (animationsRunning.count() > 0)
        return true;
    else
        return false;
}

void AnimationHandler::finished()
{
    // Handle animation finish
    AnimationEvent* a;

    for (int i = 0; i < animationsRunning.count(); i++) {
        a = animationsRunning[i];

        if (!a->animation.isNull() && a->animation->state() == QAbstractAnimation::Stopped) {
            // First Animation stopped
            // Is there second animation coming after that?
            if (!a->animation2.isNull()) {
                // Second animation coming...
                // If second animation is another picture it have to set visible and
                // pervious picture to unvisible
                if (a->animationType == RotatePic2Pic) {
                    PictureItem* pic1 = static_cast<PictureItem*> (a->animation->targetObject());
                    pic1->setVisible(false);
                    PictureItem* pic2 = static_cast<PictureItem*> (a->animation2->targetObject());
                    pic2->setVisible(true);
                }
                // Start second animation
                a->animation2->start(QAbstractAnimation::DeleteWhenStopped);
            }
            // Remove animation for the list
            else {
                animationsRunning.removeAt(i);emit
                animationFinished(a->aid);
                i--;
            }
        }
        else if (!a->animation2.isNull() && a->animation2->state() == QAbstractAnimation::Stopped) {
            // Second animation stopped
            if (a->animationType == RotatePic2Pic) {
                // Is second picture shown?
                PictureItem* pic2 = static_cast<PictureItem*> (a->animation2->targetObject());
                if (pic2 && pic2->isVisible()) {
                    // Remove animation for the list
                    animationsRunning.removeAt(i);emit
                    animationFinished(a->aid);
                    i--;
                }
            }
        }
        else if (a->animation.isNull() && a->animation2.isNull()) {
            animationsRunning.removeAt(i);emit
            animationFinished(a->aid);
            i--;
        }
        else if (a->animation.isNull()) {
            animationsRunning.removeAt(i);emit
            animationFinished(a->aid);
            i--;
        }
        else if (a->animation2.isNull() && a->animationType == RotatePic2Pic) {
            animationsRunning.removeAt(i);emit
            animationFinished(a->aid);
            i--;
        }
    }

    if (animationsRunning.count() == 0) {
        emit allAnimationsFinished();
    }

}

