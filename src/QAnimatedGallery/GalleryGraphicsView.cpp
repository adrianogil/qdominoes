/*
 * Copyright (c) 2009 Nokia Corporation.
 */

#include "GalleryGraphicsView.h"
#include <QMouseEvent>
#include <QtGlobal>

GalleryGraphicsView::GalleryGraphicsView(QWidget *parent) :
    QGraphicsView(parent)
{
}

GalleryGraphicsView::GalleryGraphicsView(QGraphicsScene* scene, QWidget* parent) :
    QGraphicsView(scene,parent)
{
}

GalleryGraphicsView::~GalleryGraphicsView()
{
}

void GalleryGraphicsView::mousePressEvent(QMouseEvent *event)
{
    m_mouseDown = event->posF();
    event->ignore();
}
void GalleryGraphicsView::mouseMoveEvent(QMouseEvent *event)
{
    event->ignore();
}
void GalleryGraphicsView::mouseReleaseEvent(QMouseEvent *event)
{
    m_mouseUp = event->posF();
    // If user has moved pointer less than 50 pixels, app desides that
    // to be as pointer press
    int x = qAbs(m_mouseUp.x() - m_mouseDown.x());
    if (x < 50) {
        event->accept();
        emit pointerPressed(event->posF());
    }
    else {
        event->ignore();
    }

}
