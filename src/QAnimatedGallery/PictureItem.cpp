/*
 * Copyright (c) 2009 Nokia Corporation.
 */

#include "PictureItem.h"
#include <QPainter>

PictureItem::PictureItem(const QPixmap& pixmap, bool showPictureInfo, QObject* parent) :
    QObject(parent), QGraphicsPixmapItem(pixmap)
{
    m_showPictureInfo = showPictureInfo;
    m_size = pixmap.size();
    setCacheMode(DeviceCoordinateCache);
    m_originalPicForFade = 0;
    m_isNeeded = true;
    resetScale();
}

PictureItem::PictureItem(QSizeF size, const QPixmap& pixmap, bool showPictureInfo, QObject* parent) :
    QObject(parent), QGraphicsPixmapItem(pixmap)
{
    m_showPictureInfo = showPictureInfo;
    m_size = size;
    setCacheMode(DeviceCoordinateCache);
    m_originalPicForFade = 0;
    m_isNeeded = true;
    resetScale();
}

PictureItem::~PictureItem()
{
}

void PictureItem::storeSizePosScale(const QSize size, const QPointF pos, qreal scale)
{
    m_storedSize = size;
    m_storedPos = pos;
    m_scale = scale;
}

void PictureItem::getStoredSizePosScale(QSize& size, QPointF& pos, qreal& scale)
{
    size = m_storedSize;
    pos = m_storedPos;
    scale = m_scale;
}

qreal PictureItem::storedScale()
{
    return m_scale;
}

void PictureItem::resetScale()
{
    m_scale = 1.0;
}

void PictureItem::notNeeded()
{
    m_isNeeded = false;
}

bool PictureItem::isNeeded()
{
    return m_isNeeded;
}

void PictureItem::showPictureInfo(bool enable)
{
    m_showPictureInfo = enable;
}

QRectF PictureItem::boundingRect() const
{
if (pixmap().isNull())
    return QRectF(0, 0, m_size.width(), m_size.height());
else
    return QRectF(0,0, pixmap().size().width(), pixmap().size().height());
}

void PictureItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if (m_showPictureInfo) {
        // Show thumbnail data, waiting for get final picture
        painter->setBrush(QBrush(Qt::white));
        painter->drawRect(QRectF(QPointF(0,0),m_size));

        painter->setPen(Qt::SolidLine);
        painter->setPen(QColor(Qt::black));
        QSize s = QSize(m_size.width()-6,m_size.height()-6);
        painter->drawRect(QRectF(QPointF(3,3),s));
    }
    else {
        // Show actual final picture
        QGraphicsPixmapItem::paint(painter, option, widget);
    }
}

qreal PictureItem::rotationAngleY() const
{
    return m_rotationAngleY;
}

void PictureItem::setRotationAngleY(qreal angle)
{
    if (m_rotationAngleY != angle) {
        m_rotationAngleY = angle;
        QPointF c = boundingRect().center();
        QTransform t;
        t.translate(c.x(), c.y());
        t.rotate(m_rotationAngleY, Qt::YAxis);
        t.translate(-c.x(), -c.y());
        setTransform(t);
    }
}

void PictureItem::setFade(int f)
{
    m_fade = f;

    if ((m_fade == 255 || m_fade == 0) && m_originalPicForFade.isNull()) {
        m_originalPicForFade = pixmap();
    }

    QPixmap pic;
    if (m_originalPicForFade.isNull())
        pic = pixmap();
    else
        pic = m_originalPicForFade;

    QPixmap transparent(pic.size());
    transparent.fill(Qt::transparent);
    QPainter p(&transparent);
    p.setCompositionMode(QPainter::CompositionMode_Source);
    p.drawPixmap(0, 0, pic);
    p.setCompositionMode(QPainter::CompositionMode_DestinationIn);
    p.fillRect(transparent.rect(), QColor(0, 0, 0, m_fade));
    p.end();
    pic = transparent;
    transparent = 0;

    setPixmap(pic);
}

int PictureItem::fade() const
{
    return m_fade;
}

void PictureItem::storeThumbPic()
{
    m_exifThumbPic = this->pixmap();
}
void PictureItem::useStoredThumbPic()
{
    if (!m_exifThumbPic.isNull())
        this->setPixmap(m_exifThumbPic);
}
    

