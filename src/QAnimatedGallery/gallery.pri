QT += svg

INCLUDEPATH += $$PWD
HEADERS += $$PWD/PictureItem.h \
    $$PWD/PictureLoaderThread.h \
    $$PWD/AnimationHandler.h \
    $$PWD/GalleryGraphicsScene.h \
    $$PWD/GalleryGraphicsView.h \
    $$PWD/QAnimatedGallery.h
    
SOURCES += $$PWD/PictureItem.cpp \
    $$PWD/PictureLoaderThread.cpp \
    $$PWD/AnimationHandler.cpp \
    $$PWD/GalleryGraphicsScene.cpp \
    $$PWD/GalleryGraphicsView.cpp \
    $$PWD/QAnimatedGallery.cpp

#symbian: {
#    HEADERS += $$PWD/symbianpicloader.h
#    SOURCES += $$PWD/symbianpicloader.cpp

#    LIBS += -lfbscli -lefsrv -lexiflib.dll -limageconversion
#}
