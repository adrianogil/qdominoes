/*
 * Copyright (c) 2009 Nokia Corporation.
 */

#include "QAnimatedGallery.h"
#include "GalleryGraphicsScene.h"
#include "GalleryGraphicsView.h"
#include "PictureLoaderThread.h"
#include "PictureItem.h"
#include <QDebug>

QAnimatedGallery::QAnimatedGallery(QWidget *parent) :
    QMainWindow(parent)
{
//#ifdef Q_OS_SYMBIAN
//    // Symbian version uses exif loader for reading thumbnail picture from jpeg pictures
//    m_symbianPicLoader = new SymbianPicLoader(this);
//#endif

    m_galleryState = ENone;
    // Create graphics scene
    m_scene = new GalleryGraphicsScene(this);
    m_scene->setItemIndexMethod(QGraphicsScene::NoIndex);
    // Listen animations done signals
    QObject::connect(m_scene, SIGNAL(animationsDone()), this, SLOT(animationsDone()));
    QObject::connect(m_scene, SIGNAL(animationDone(int)),this, SLOT(animationDone(int)));

    // Create graphics view for scene
    m_view = new GalleryGraphicsView(m_scene, this);
    m_view->setFrameShape(QFrame::NoFrame);
    m_view->setCacheMode(QGraphicsView::CacheBackground);
    m_view->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
    m_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_view->setBackgroundBrush(QPixmap(":/menu/background.png"));
    QObject::connect(m_view, SIGNAL(pointerPressed(QPointF)), m_scene, SLOT(pointerPressed(QPointF)));

    // Picture loader thread
    m_pictureLoaderThread = new PictureLoaderThread(this);
    qRegisterMetaType<PictureData>("PictureData");
    qRegisterMetaType<QImage>("QImage");
    QObject::connect(m_pictureLoaderThread, SIGNAL(imageLoaded(QImage*,PictureData*)),this, SLOT(imageLoaded(QImage*,PictureData*)));

    m_pictureIndex = 1;
    m_startAnimCounter = 0;
    m_tid_startAnimation = 0;
    m_tid_showThumbnailPics = 0;
    m_tid_loadPicturePaths = 0;
    m_leftDirection = true;

    setCentralWidget(m_view);

    // Start animation...
    m_tid_startAnimation = startTimer(2000);
}

QAnimatedGallery::~QAnimatedGallery()
{
    PictureData * key;
    foreach(key,m_pictureDataList) {
            delete key;
        }
    m_pictureDataList.clear();
}

void QAnimatedGallery::mousePressEvent(QMouseEvent *e)
{
    m_mouseDown = e->pos();
}

void QAnimatedGallery::mouseReleaseEvent(QMouseEvent *e)
{
    m_mouseUp = e->pos();

    if (m_mouseDown.x() < m_mouseUp.x()) {
        // swipe right
        if ((m_mouseUp.x() - m_mouseDown.x()) > 50) {
            if (m_galleryState == ENone) {
                m_leftDirection = false;
//#ifdef Q_OS_SYMBIAN
//                m_symbianPicLoader->Cancel();
//#endif
                if (m_scene->moveCurrentThumbs(false)) {
                    m_galleryState = OldThumbPicAnimation;
                }
            }
        }
    }
    else if (m_mouseDown.x() > m_mouseUp.x()) {
        // swipe left
        if ((m_mouseDown.x() - m_mouseUp.x()) > 50) {
            if (m_galleryState == ENone) {
                m_leftDirection = true;
//#ifdef Q_OS_SYMBIAN
//                m_symbianPicLoader->Cancel();
//#endif
                if (m_scene->moveCurrentThumbs(true)) {
                    m_galleryState = OldThumbPicAnimation;
                }
            }
        }
    }
}

void QAnimatedGallery::timerEvent(QTimerEvent *event)
{
    if (m_tid_startAnimation == event->timerId()) {
        killTimer(m_tid_startAnimation);
        m_tid_startAnimation = 0;
        if (m_startAnimCounter < 4) {
            showStartAnimation();
        }
    }
    else if (m_tid_showThumbnailPics == event->timerId()) {
        killTimer(m_tid_showThumbnailPics);
        m_tid_showThumbnailPics = 0;
        // Load thumbnail pictures
//#ifdef Q_OS_SYMBIAN
//        loadExifThumbnailPictures();
//#else
        loadThumbnailPictures();
//#endif
    }
    else if (m_tid_loadPicturePaths == event->timerId()) {
        killTimer(m_tid_loadPicturePaths);
        m_tid_loadPicturePaths = 0;
        // Search available images
        searchPicPaths();
        // Show thumbnail data
        showThumbnailData(1);
    }
}

void QAnimatedGallery::showStartAnimation()
{
    if (m_galleryState == ENone)
        m_galleryState = StartAnimation;

    if (m_galleryState == StartAnimation) {
        m_startAnimCounter++;
        m_scene->addStartAnimation(m_startAnimCounter);
        m_tid_startAnimation = startTimer(1000);
    }
}

void QAnimatedGallery::showThumbnailData(int firstPicIndex)
{
    m_galleryState = ThumbDataAnimation;
    m_pictureIndex = firstPicIndex; // First picture index to show

    // Show thumbnail data

    // Calculate previous pictures index if needed
    if (!m_leftDirection) {
        // Previous pictures
        int div = (m_pictureIndex - 1) % 6;
        if (div > 0) {
            // some pic
            m_pictureIndex = m_pictureIndex - 6 - div;
        }
        else {
            // whole six coming
            m_pictureIndex = m_pictureIndex - 6 - 6;
        }
    }

    // No more pictures, show last 6
    if (m_pictureIndex > m_pictureDataList.count()) {
        int div = m_pictureDataList.count() % 6;
        if (div>0)
            m_pictureIndex = m_pictureDataList.count() - div + 1;
        else
            m_pictureIndex = m_pictureDataList.count()-5;
    }

    // For sure
    if (m_pictureIndex < 1) {
        m_pictureIndex = 1;
    }

    int counter = 0;
    while (m_pictureIndex + counter <= m_pictureDataList.count() && counter < 6) {
        // Update picture data: size, indexOnScreen
        PictureData* data = m_pictureDataList[m_pictureIndex - 1 + counter];
        data->indexOnScreen = counter + 1;
        // Scale image size (not image) to near preferred image size
        if (data->size.width() == 0) {
            data->size = m_pictureLoaderThread->imageScaledSize(m_thumbnailSize, data->path);
        }
        // Add thumbnail without picture to scene
        m_scene->addThumbnailData(*data);
        counter++;
    }
}

void QAnimatedGallery::resizeEvent(QResizeEvent* event)
{
    QMainWindow::resizeEvent(event);

    // Calculating preferred size of the thumbnails
    QSize s = event->size();
    m_thumbnailSize.setHeight(s.height() * 0.40);
    m_thumbnailSize.setWidth(s.width() * 0.27);
    m_scene->setPreferredThumbSize(m_thumbnailSize);

    // Setting new screen rect to scene
    m_scene->setSceneRect(m_view->rect());
}

void QAnimatedGallery::searchImages(QString path)
{
    // Read all JPG JPEG image paths from the given path
    // Read images from the subfolders
    if (path.isNull()) {
        path = QDesktopServices::storageLocation(QDesktopServices::PicturesLocation);
    }
    QDir dir;
    dir.setFilter(QDir::Files | QDir::Dirs);
    dir.setPath(path);

    QFileInfoList list = dir.entryInfoList();
    for (int i = 0; i < list.size(); ++i) {
        QFileInfo fileInfo = list.at(i);
        qDebug() << fileInfo.filePath();
        if (fileInfo.isFile()) {
            QString s = fileInfo.filePath();
            if (s.indexOf(QString(".jpg"), 0, Qt::CaseInsensitive) > 0) {
                PictureData* data = new PictureData;
                data->path = s;
                data->p = 0;
                m_pictureDataList.append(data);
            }
            else if (s.indexOf(QString(".jpeg"), 0, Qt::CaseInsensitive) > 0) {
                PictureData* data = new PictureData;
                data->path = s;
                data->p = 0;
                m_pictureDataList.append(data);
            }
        }
        else {
            QString s = fileInfo.filePath().mid(fileInfo.filePath().length() - 1, 1);
            if (!s.contains(".") && !s.contains(".."))
                searchImages(fileInfo.filePath());
        }
    }
}

void QAnimatedGallery::loadThumbnailPictures()
{
    m_galleryState = NewThumbPicAnimation;

    // How many thymbs we are waiting
    m_thumbLoadLoopCount = m_pictureDataList.count() - (m_pictureIndex - 1);
    if (m_thumbLoadLoopCount>6)
        m_thumbLoadLoopCount=6;

    // Load images for thumbnail
    int counter = 0;
    while (m_pictureIndex <= m_pictureDataList.count()) {
        PictureData* data = m_pictureDataList[m_pictureIndex - 1];
        m_pictureLoaderThread->loadImage(*data);
        m_pictureIndex++;
        counter++;
        if (counter == 6)
            break;
    }
}

void QAnimatedGallery::imageLoaded(QImage* image, PictureData* data)
{
    QPixmap pic = QPixmap::fromImage(*image);
    delete image;
    data->p = pic;
    data->size = pic.size();

    // One loaded
    m_thumbLoadLoopCount--;

    if (m_galleryState == NewThumbPicAnimation) {
        // Add thumbnail to the scene
        m_scene->addThumbnail(*data);
    }
    else if (m_galleryState == BigPicState) {
        // Add final big picture to the scene
        m_scene->addBigPic(*data);
    }
}

//#ifdef Q_OS_SYMBIAN
//void QAnimatedGallery::loadExifThumbnailPictures()
//{
//    // Reset loading counter on first time
//    if (m_galleryState != NewThumbPicAnimation) {
//        // How many thymbs we are waiting
//        m_thumbLoadLoopCount = m_pictureDataList.count() - (m_pictureIndex - 1);
//        if (m_thumbLoadLoopCount>6)
//            m_thumbLoadLoopCount=6;
//    }

//    m_galleryState = NewThumbPicAnimation;

//    if (m_pictureIndex <= m_pictureDataList.count()) {
//        PictureData* data = m_pictureDataList[m_pictureIndex - 1];
//        m_pictureIndex++;
//        m_symbianPicLoader->loadImage(data);
//    }
//}

//void QAnimatedGallery::thumbLoaded(QPixmap pic, PictureData* data)
//{
//    // exif jpeg thumbnail loaded
//    data->p = pic;
//    pic = 0;
//    if (!data->p.isNull()) {
//        // Add thumbnail to the scene
//        data->size = data->p.size();
//        m_thumbLoadLoopCount--; // One get
//        if (m_scene->addThumbnail(*data)==false) {
//            animationsDone();
//        }
//    }
//    else {
//        // exif thumbnail does not found, we have to load whole jpeg
//        m_pictureLoaderThread->loadImage(*data);
//    }
//}
//#endif

void QAnimatedGallery::searchPicPaths()
{
#ifdef Q_OS_SYMBIAN
    QString path = QDesktopServices::storageLocation(QDesktopServices::PicturesLocation);
    // Search from default picture folder c:/data/images
    searchImages(path);
    // Search from E drive
    path = "E:/images";
    searchImages(path);
#else
    QString path = QDesktopServices::storageLocation(QDesktopServices::PicturesLocation);
    searchImages(path);
#endif
}

void QAnimatedGallery::animationsDone()
{
    if (m_galleryState == StartAnimation) {
        if (m_startAnimCounter == 4) {
            // Load picture paths and show first empty thumb pics
            m_tid_loadPicturePaths = this->startTimer(100);
        }
    }
    else if (m_galleryState == ThumbDataAnimation) {
        // Load picture for the thumbnail
        m_tid_showThumbnailPics = this->startTimer(0);
    }
    else if (m_galleryState == NewThumbPicAnimation) {
        // Load and animate more new pictures if needed
//#ifdef Q_OS_SYMBIAN
//        // Is all loaded?
//        if (m_thumbLoadLoopCount==0) {
//            m_galleryState = ENone;
//        } else {
//            // More
//            m_tid_showThumbnailPics = this->startTimer(0);
//        }
//#else
        // Is all loaded?
        if (m_thumbLoadLoopCount==0) {
            m_galleryState = ENone;
        }
//#endif
    }
    else if (m_galleryState == OldThumbPicAnimation) {
        showThumbnailData(m_pictureIndex);
    }
}

void QAnimatedGallery::animationDone(int id)
{
    if (id == 1002) {
        // Picture scaled up
        PictureItem* pic = m_scene->getTopMostPictureitem();
        m_galleryState = BigPicState;
        PictureData* data = m_scene->getPicData(pic);
        // Load big picture
        if (data) {
            // Get scaled size for loading big final picture
            qreal scale = pic->scale();
            data->size.setHeight(data->size.height() * scale);
            data->size.setWidth(data->size.width() * scale);
            // Load picture
            m_pictureLoaderThread->loadImage(*data);
        }
    }
    else if (id == 1003) {
        // Picture scaled back to down
        m_galleryState = ENone;
        PictureItem* pic = m_scene->getTopMostPictureitem();
        PictureData* data = m_scene->getPicData(pic);
        if (data) {
            data->size = pic->pixmap().size();
        }
        pic->resetScale();
    }
}

GalleryGraphicsScene* QAnimatedGallery::getScene()
{
    return m_scene;
}
