/*
 * Copyright (c) 2009 Nokia Corporation.
 */

#ifndef ANIMATIONHANDLER_H
#define ANIMATIONHANDLER_H

#include <QObject>
#include <QPoint>
#include <QPointer>
#include <QEasingCurve>

class PictureItem;
class QVariantAnimation;
class QPropertyAnimation;
class QParallelAnimationGroup;
class GalleryGraphicsScene;

// Different type of animations
enum AnimationType
{
    None, MovePic, RotatePic, RotatePic2Pic, FadeOutPic, FadeInPic, ScalePic, ScaleAndPocPic
};

/*****************************************************************************
 * AnimationEvent
 */
class AnimationEvent
{
public:
    AnimationEvent();
    AnimationEvent(QPropertyAnimation*, const AnimationType&, int delay=0, int id=0);

    // Animation even can have 2 different animations that are executed in a flow
    QPointer<QPropertyAnimation> animation; // First animation
    QPointer<QPropertyAnimation> animation2; // Executed after first is finished
    AnimationType animationType;
    int aid;
    int adelay;
};

/*****************************************************************************
 * AnimationHandler
 */
class AnimationHandler: public QObject
{
Q_OBJECT

public:
    AnimationHandler(GalleryGraphicsScene* scene, QObject *parent = 0);
    ~AnimationHandler();

    void clearGroup();
    void addPictureToGroupPosAnimation(PictureItem& picture, int delay, int duration,
        const QPointF& start, const QPointF& end, const QEasingCurve& easing, int id=0);
    void startGroupAnimation();
    
    void timerEvent(QTimerEvent *);
    
    void addPicturePositionAnimation(PictureItem&, int duration, const QPointF&, const QPointF&,
        const QEasingCurve&, int id=0);
    
    void addPictureRotationAnimation(PictureItem&, int duration, int angle, const QEasingCurve&, int id=0);

    void addPictureScaleAnimation(PictureItem&, int duration, qreal start, qreal end, const QEasingCurve&, int id=0);

    void addPictureScaleAndPosAnimation(PictureItem&, int duration, qreal scaleStart,
        qreal scaleEnd, const QPointF& start, const QPointF& end, const QEasingCurve& easing, int id=0);
    
    void addPic2PicRotationAnimation(PictureItem& from, PictureItem& to, int duration, int id=0);

    void addPictureFadingAnimation(PictureItem&, int duration, bool fadeIn, int id=0);
    
    bool isAnimationsRunning();
    
public slots:
    void finished();
    
signals:
    void animationFinished(int);
    void allAnimationsFinished();

private:
    void doGroupAnimation();
    
private:
    QList<AnimationEvent*> m_animationGroup;
    QList<AnimationEvent*> animationsRunning;
    QPointer<GalleryGraphicsScene> m_scene;
    int groupTid;
};

#endif // ANIMATIONHANDLER_H
