/*
 * Copyright (c) 2009 Nokia Corporation.
 */

#ifndef PICTUREITEM_H
#define PICTUREITEM_H

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QFont>
#include <QPointer>

class PictureItem: public QObject, public QGraphicsPixmapItem
{
Q_OBJECT
    // For Property animation: 

    // Change picture position
    Q_PROPERTY(QPointF pos READ pos WRITE setPos)

    // Rotating picture
    Q_PROPERTY(qreal rotationAngleY READ rotationAngleY WRITE setRotationAngleY)

    // Fade picture
    Q_PROPERTY(int fade READ fade WRITE setFade)
    
    // Scale picture
    Q_PROPERTY(qreal scale READ scale WRITE setScale)
    
public:    
    enum
    {
        PictureItemType = UserType + 1
    };

public:
    // Does not have pixmap, size of item have to define
    PictureItem(QSizeF size, const QPixmap& pixmap = 0, bool showPictureInfo = true,
        QObject* parent = 0);
    // Have pixmap
    PictureItem(const QPixmap&, bool showPictureInfo = false, QObject* parent = 0);

    ~PictureItem();

    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*);

    QRectF boundingRect() const;

    void showPictureInfo(bool enable);

    void setRotationAngleY(qreal angle);
    qreal rotationAngleY() const;

    void setFade(int f);
    int fade() const;

    int type() const
    {
        return PictureItemType;
    }
    
    void notNeeded();
    bool isNeeded();
    
    void storeThumbPic();
    void useStoredThumbPic();
    
    void storeSizePosScale(const QSize size, const QPointF pos, qreal scale);
    void getStoredSizePosScale(QSize& size, QPointF& pos, qreal& scale);
    
    qreal storedScale();
    void resetScale();

private:
    bool m_showPictureInfo;
    QSizeF m_size;
    qreal m_rotationAngleY;
    int m_fade;
    QPixmap m_originalPicForFade;
    bool m_isNeeded;
    QSize m_storedSize;
    QPointF m_storedPos;
    QPixmap m_exifThumbPic;
    qreal m_scale;
};

#endif // PICTUREITEM_H
