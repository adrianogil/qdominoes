#include "handcontroller.h"

#include "appdebug.h"

HandController::HandController(QObject *parent) :
    QObject(parent)
{
}

void HandController::addTileToHand(TileModel *tileModel)
{
    m_tilesModel.append((QObject*) tileModel);
    emit tilesModelChanged();
}

void HandController::addTileToHand(int sideA, int sideB)
{
    addTileToHand(new TileModel(sideA, sideB));
}

void HandController::removeTileFromHand(TileModel *tileModel)
{
    QObjectList objList;
    objList.append(m_tilesModel);
    foreach(QObject* obj, objList) {
        TileModel *tile = qobject_cast<TileModel*>(obj);
        if (tile->isEqual(tileModel)) {
            m_tilesModel.removeAll(tile);
        }
    }
    emit removedTile((QObject*)tileModel);
    emit tilesModelChanged();
}

void HandController::removeTileFromHand(int sideA, int sideB)
{
    removeTileFromHand(new TileModel(sideA, sideB));
}

void HandController::reorderTiles(QObject *tile, const int &newPosition)
{
    m_tilesModel.removeAll(tile);
    m_tilesModel.insert(newPosition, tile);
    emit tilesModelChanged();
}
