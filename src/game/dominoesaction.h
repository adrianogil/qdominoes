#ifndef DOMINOESACTION_H
#define DOMINOESACTION_H

#include "gameaction.h"

class PlayerModel;
class TableModel;
class TileModel;

class DominoesAction : public GameAction
{
    Q_OBJECT

    Q_SYNTHESIZE(PlayerModel*, playerModel, PlayerModel, playerModelChanged)
public:
    explicit DominoesAction(QObject *parent = 0);

    virtual void applyToState(GameState *gameState);
    virtual void onActionFinished(GameState *gameState) { Q_UNUSED(gameState)}
    virtual void onActionFailed(GameState *gameState) { Q_UNUSED(gameState)}

signals:
    void playerModelChanged();

};

class TilePlayAction : public DominoesAction
{
    Q_OBJECT

    Q_SYNTHESIZE(TileModel*, tileModel, TileModel, tileModelChanged)
    Q_SYNTHESIZE(int, cornerPlayed, CornerPlayed, cornerPlayedChanged)
public:
    TilePlayAction(PlayerModel* playerModel, TileModel *tileModel, const int &cornerPlayed, QObject *parent = 0);

    void applyToState(GameState *gameState);
    void onActionFinished(GameState *gameState) {Q_UNUSED(gameState)}

signals:
    void tileModelChanged();
    void cornerPlayedChanged();
};

class PlayerPassAction : public DominoesAction
{
    Q_OBJECT

public:
    PlayerPassAction(PlayerModel *playerModel, QObject *parent = 0);

    bool verifyConditions(GameState *gameState);
};

class PlayerTurnBeginAction : public DominoesAction
{
    Q_OBJECT

public:
    PlayerTurnBeginAction(PlayerModel* playerModel, QObject *parent = 0);

    bool verifyConditions(GameState *gameState);
    void applyToState(GameState *gameState);
    void onActionFailed(GameState *gameState);
};


class PlayerTurnEndAction : public DominoesAction
{
    Q_OBJECT

public:
    PlayerTurnEndAction(PlayerModel* playerModel, QObject *parent = 0);

    bool verifyConditions(GameState *gameState);
    void applyToState(GameState *gameState);
};

class GaloYellAction : public DominoesAction
{
    Q_OBJECT

public:
    GaloYellAction(PlayerModel* playerModel, QObject *parent = 0);

    bool verifyConditions(GameState *gameState);
};

class StartMatchAction : public DominoesAction
{
    Q_OBJECT

    Q_SYNTHESIZE(int, matchNumber, MatchNumber, matchNumberChanged)
public:
    StartMatchAction(PlayerModel* playerModel, int matchNumber, QObject *parent = 0);

    bool verifyConditions(GameState *gameState);
    void applyToState(GameState *gameState);
signals:
    void matchNumberChanged();
};

class FinishMatchAction : public DominoesAction
{
    Q_OBJECT

    Q_SYNTHESIZE(int, matchNumber, MatchNumber, matchNumberChanged)
public:
    FinishMatchAction(PlayerModel* playerModel, int matchNumber, QObject *parent = 0);

    bool verifyConditions(GameState *gameState);
signals:
    void matchNumberChanged();
};


#endif // DOMINOESACTION_H
