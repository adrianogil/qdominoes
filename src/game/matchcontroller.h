#ifndef MATCHCONTROLLER_H
#define MATCHCONTROLLER_H

#include "gameobjectcontroller.h"

class StartMatchAction;
class FinishMatchAction;

class MatchController : public GameObjectController
{
    Q_OBJECT

    Q_SYNTHESIZE(int, matchNumber, MatchNumber, matchNumberChanged)
public:
    explicit MatchController(QObject *parent = 0);

    void receiveAction(GameAction *action);

    void startMatch();
signals:
    void matchNumberChanged();

private:
    void onStartMatchActionReceived(StartMatchAction *startMatchAction);
    void onFinishMatchActionReceived(FinishMatchAction *finishMatchAction);

};

#endif // MATCHCONTROLLER_H
