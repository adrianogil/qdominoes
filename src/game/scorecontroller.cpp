#include "player/playermodel.h"
#include "table/tablecontroller.h"
#include "table/tablemodel.h"
#include "scorecontroller.h"
#include "dominoesaction.h"
#include "dominoesutils.h"
#include "gamehost.h"
#include "team.h"

#include "appdebug.h"

#define POINTS_WHEN_PASSING 20
#define POINTS_FOR_GALO 50

ScoreController::ScoreController(QObject *parent)
    : GameObjectController(parent)
{
}

void ScoreController::receiveAction(GameAction *action)
{
    CHECK_CAST(action, playerPassAction, PlayerPassAction*, onPlayerPassActionReceived);
    CHECK_CAST(action, galoYellAction, GaloYellAction*, onGaloYellActionReceived);
    CHECK_CAST(action, tilePlayAction, TilePlayAction*, onTilePlayActionReceived);
    CHECK_CAST(action, finishMatchAction, FinishMatchAction*, onFinishMatchActionReceived);
}

void ScoreController::onPlayerPassActionReceived(PlayerPassAction *playerPassAction)
{
    if (!playerPassAction)
        return;

    DominoesUtils::addPointsToOpposingTeamOfPlayer(playerPassAction->playerModel(),
                                                   POINTS_WHEN_PASSING,
                                                   m_gameState);
}

void ScoreController::onGaloYellActionReceived(GaloYellAction *galoYellAction)
{
    if (!galoYellAction)
        return;

    Team *playerTeam = DominoesUtils::teamOfPlayer(galoYellAction->playerModel(), m_gameState);
    if (playerTeam)
        playerTeam->setScore(playerTeam->score() + POINTS_FOR_GALO);
}

void ScoreController::onTilePlayActionReceived(TilePlayAction *tilePlayAction)
{

    if (!tilePlayAction)
        return;

    TableController *tableController = qobject_cast<TableController*>(GameHost::sharedControllers("tableController"));
    if (!tableController)
        return;

    TableModel *tableModel = qobject_cast<TableModel*>(tableController->tableModel());
    if (!tableModel)
        return;

    QMap<QString, TileModel*> tilesOnCorner = tableModel->mapTilesOnCorners();

    // Find out if there is any open corner
    TileModel *firstTile = qobject_cast<TileModel*>(tableModel->tileModel());
    if (!firstTile)
        return;

    int points = 0;

    for (int c = 0; c <= 1; c++) {
        QString direction1 = TableModel::strDirection(2*c);
        QString direction2 = TableModel::strDirection(2*c + 1);
        if (tilesOnCorner[direction1] && tilesOnCorner[direction1] != firstTile) {
            if (tilesOnCorner[direction1]->isDouble())
                points = points + 2 * tilesOnCorner[direction1]->sideA();
            else points = points + tilesOnCorner[direction1]->sideB();
        }
        if (tilesOnCorner[direction2]) {
            if (tilesOnCorner[direction2] != firstTile) {
                if (tilesOnCorner[direction2]->isDouble())
                    points = points + 2 * tilesOnCorner[direction2]->sideA();
                else points = points + tilesOnCorner[direction2]->sideB();
            } else points = points + 2 * firstTile->sideA();
        }
    }

    // Only multiples of 5
    points = points % 5 == 0? points : 0;

    // Set up points
    DominoesUtils::addPointsToTeamOfPlayer(tilePlayAction->playerModel(), points,
                                           m_gameState);

    tableController->doScoreAnimation(tilePlayAction->tileModel()->sideA(),
                                         tilePlayAction->tileModel()->sideB(),
                                         points,
                                         tilePlayAction->cornerPlayed());
    debugMsg << tilePlayAction->playerModel()->name() << points
             << "(" << tilePlayAction->tileModel()->sideA()
             << "," << tilePlayAction->tileModel()->sideB()
             << ")";
}

void ScoreController::onFinishMatchActionReceived(FinishMatchAction *finishMatchAction)
{
    if (!finishMatchAction && !finishMatchAction->playerModel())
        return;

    DominoesUtils::lastTilePointsFromOpposingTeamOfPlayer(finishMatchAction->playerModel(),
                                                          m_gameState);
}
