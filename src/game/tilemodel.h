#ifndef TILEMODEL_H
#define TILEMODEL_H

#include <QtCore/QObject>
#include "qtsynthesize.h"

class TileModel;

typedef QList<TileModel*> TileModelList;

class TileModel : public QObject
{
    Q_OBJECT

public:

    enum TileState {OnHand, OnTable};

    Q_ENUMS(TileState)
    //! sideA of a Tile: a int number between 0 and 6
    Q_PROPERTY(int sideA READ sideA WRITE setSideA NOTIFY sideAChanged)
    //! sideB of a Tile: a int number between 0 and 6
    Q_PROPERTY(int sideB READ sideB WRITE setSideB NOTIFY sideBChanged)
    //! Particular state of a tile
    Q_PROPERTY(TileState tileState READ tileState WRITE setTileState NOTIFY tileStateChanged)

    Q_CSYNTHESIZE(int, sideA, SideA, sideAChanged)
    Q_CSYNTHESIZE(int, sideB, SideB, sideBChanged)
    Q_CSYNTHESIZE(TileState, tileState, TileState, tileStateChanged)

    TileModel(QObject *parent = 0)
        : QObject(parent) {

    }

    TileModel(const TileModel &tileModel, QObject *parent = 0) : QObject(parent){
        setSideA(tileModel.sideA());
        setSideB(tileModel.sideB());
        setTileState(tileModel.tileState());
    }

    TileModel(int sideA, int sideB, QObject *parent = 0)
        : QObject(parent)
    {
        setSideA(sideA);
        setSideB(sideB);
    }

    Q_INVOKABLE bool isEqual(TileModel *tileModel) {
        return (tileModel->sideA() == sideA() && tileModel->sideB() == sideB()) ||
                (tileModel->sideB() == sideA() && tileModel->sideA() == sideB());
    }
    Q_INVOKABLE bool isEqual(QObject *tileModel) {
        return isEqual(qobject_cast<TileModel*>(tileModel));
    }

    //@TODO: let check out what side is free
    Q_INVOKABLE int freeSide() {
        return sideB();
    }

    Q_INVOKABLE bool isDouble() {
        return sideA() == sideB();
    }

    bool isValidTile() {
        return (m_sideA >= 0 && m_sideA <= 6) &&
                (m_sideB >= 0 && m_sideB <= 6);
    }

    static QObjectList convertToObj(TileModelList tileList) {
        QObjectList objList;

        foreach (TileModel* tile, tileList) {
            objList.append(tile);
        }

        return objList;
    }

    static TileModelList convertFromObj(QObjectList objList) {
        TileModelList tileList;

        foreach (QObject *obj, objList) {
            TileModel *tile = qobject_cast<TileModel*>(obj);
            if (tile)
                tileList.append(tile);
        }

        return tileList;
    }


signals:
    void sideAChanged();
    void sideBChanged();
    void tileStateChanged();
};

#endif // TILEMODEL_H
