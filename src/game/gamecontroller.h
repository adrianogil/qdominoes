#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H

#include <QtCore/QObject>
#include <QtCore/QTime>
#include <QtCore/QVariant>
#include <QtCore/QBasicTimer>
#include "gamehost.h"
#include "gameaction.h"

#include "tilemodel.h"

class GameState;
class GameConfig;
class TableController;
class ScoreController;
class PlayerController;
class MatchController;
class PlayerTurnController;

class GameController : public GameHost
{
    Q_OBJECT

    //! Allow playerController (a PlayerController object) be called from QML
    Q_PROPERTY(QObject* playerController READ playerController NOTIFY playerControllerChanged)
    //! Allow tableController (a TableController object) be called from QML
    Q_PROPERTY(QObject* tableController READ tableController NOTIFY tableControllerChanged)
    //! Allow gameState (a GameState object) be called from QML
    Q_PROPERTY(QObject* gameState READ gameStateObj NOTIFY gameStateMayHaveChanged)
    //! Allos gameConfig (a GameConfig object) be called from QML
    Q_PROPERTY(QObject* gameConfig READ gameConfigObj NOTIFY gameConfigChanged)

    Q_SYNTHESIZE(GameConfig*, gameConfig, GameConfig, gameConfigChanged)
public:
    GameController(QObject* parent = 0);

    //! Start Game internal parameters
    Q_INVOKABLE void startGame();

    Q_INVOKABLE void startLoop();

    //! PlayerController as a QObject*
    QObject* playerController();

    //! TableController as a QObject*
    QObject* tableController();

    //! GameConfig as a QObject*
    QObject* gameConfigObj();

    //! Verify if a player can play current turn based on its hand tiles
    bool canPlay(PlayerController* player);

    //! GameState as a QObject*
    QObject* gameStateObj() {
        return gameState();
    }

    bool isThereAPlayerWithEmptyHand();

    QList<PlayerController*> players() { return m_players; }

    void warnsPlayerHasPassed(PlayerModel* player);
    void warnsDrawback() {
        emit drawbackWarning();
    }

    void warnsTeamAWins() {
        emit teamAWinsWarning();
    }

    void warnsTeamBWins() {
        emit teamBWinsWarning();
    }

protected:
    void updateAll(const GameTime &gameTime);

signals:
    void playerControllerChanged();
    void tableControllerChanged();
    // Assure the game state is always updated properly to QML side
    void gameStateMayHaveChanged();
    void gameConfigChanged();
    void gameStarted();
    void playerPassed(const QString &playerName);
    void drawbackWarning();
    void teamAWinsWarning();
    void teamBWinsWarning();

private slots:
    void updateTableModel();

private:
    QList<PlayerController*> m_players;
    PlayerController *m_localPlayerController;
    TableController *m_tableController;
    ScoreController *m_scoreController;
    MatchController *m_matchController;
    PlayerTurnController *m_playerTurnController;
};

#endif // GAMECONTROLLER_H
