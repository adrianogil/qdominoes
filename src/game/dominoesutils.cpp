#include "dominoesutils.h"
#include "tilemodel.h"

#include <QtCore/QDateTime>

#include "team.h"
#include "tilemodel.h"
#include "gameconfig.h"
#include "gamecontroller.h"
#include "player/playercontroller.h"
#include "player/playermodel.h"

QList< QList<TileModel*> > DominoesUtils::shuffleTiles()
{
    qsrand(QDateTime::currentDateTime().toTime_t());
    QList< QList<TileModel*> > auxList;
    QList <TileModel*> allTiles;

    for (int i = 0; i < 7; i++)
        for (int j = i; j < 7; j++)
            allTiles << new TileModel(i, j, GameHost::sharedControllers("gameController"));

    for (int i = 0; i  < 4; i ++) {
        QList <TileModel*> someHand;
        while (someHand.count() < 7) {
            TileModel* someTile = allTiles.takeAt(qrand() % allTiles.count());
            someHand.append(someTile);
        }
        auxList.append(someHand);
    }

    return auxList;
}

PlayerController* DominoesUtils::firstPlayerInCurrentMatch(const TileModel &initialTile)
{
    GET_SHARED_CONTROLLER("gameController", GameController*, gameController);

    if (!gameController) {
        debugMsg << "Error when getting shared controller. Null reference of 'GameController'!";
        return 0;
    }

    QList<PlayerController*> players = gameController->gameConfig()->players();

    foreach(PlayerController* player, players)
        foreach(TileModel* tile, player->handTiles())
            if (tile && tile->sideA() == initialTile.sideA()
                    && tile->sideB() == initialTile.sideB())
                return player;

    return 0;
}

PlayerController * DominoesUtils::getPlayerFromModel(PlayerModel *playerModel)
{
    GET_SHARED_CONTROLLER("gameController", GameController*, gameController);

    if (!gameController) {
        debugMsg << "Error when getting shared controller. Null reference of 'GameController'!";
        return 0;
    }

    QList<PlayerController*> players = gameController->gameConfig()->players();

    foreach(PlayerController *player, players)
        if (player->playerModel() == playerModel)
            return player;

    return 0;
}

Team * DominoesUtils::teamOfPlayer(PlayerModel *player, GameState *state)
{
    if (!player || !state)
        return 0;

    Team *playerTeam = 0;

    Team *playerTeamA = qobject_cast<Team*>(state->teamA());
    Team *playerTeamB = qobject_cast<Team*>(state->teamB());

    if (playerTeamA->players1() == player || playerTeamA->players2() == player)
        playerTeam = playerTeamA;
    else if (playerTeamB->players1() == player || playerTeamB->players2() == player)
        playerTeam = playerTeamB;
    else playerTeam = 0;

    return playerTeam;
}

Team * DominoesUtils::opposingTeamOfPlayer(PlayerModel *player, GameState *state)
{
    if (!player || !state)
        return 0;

    Team *playerTeam = 0;

    Team *playerTeamA = qobject_cast<Team*>(state->teamA());
    Team *playerTeamB = qobject_cast<Team*>(state->teamB());

    if (playerTeamA->players1() == player || playerTeamA->players2() == player)
        playerTeam = playerTeamB;
    else if (playerTeamB->players1() == player || playerTeamB->players2() == player)
        playerTeam = playerTeamA;
    else playerTeam = 0;

    return playerTeam;
}

void DominoesUtils::addPointsToTeamOfPlayer(PlayerModel *player, const int &points, GameState *state)
{
    Team* team = teamOfPlayer(player, state);
    team->setScore(team->score() + points);
    state->updateAll();
}

void DominoesUtils::addPointsToOpposingTeamOfPlayer(PlayerModel *player, const int &points, GameState *state)
{
    Team* team = opposingTeamOfPlayer(player, state);
    team->setScore(team->score() + points);
    state->updateAll();
}

void DominoesUtils::lastTilePointsFromOpposingTeamOfPlayer(PlayerModel *player, GameState *state)
{
    Team *opposingTeam = opposingTeamOfPlayer(player, state);
    PlayerController *p1 = getPlayerFromModel(qobject_cast<PlayerModel*>(opposingTeam->players1()));
    PlayerController *p2 = getPlayerFromModel(qobject_cast<PlayerModel*>(opposingTeam->players2()));

    if (!p1 || !p2)
        return;

    // Counting points
    int points = 0;
    foreach(TileModel* tile, p1->handTiles())
        points = points + tile->sideA() + tile->sideB();
    foreach(TileModel* tile, p2->handTiles())
        points = points + tile->sideA() + tile->sideB();

    points = points % 5;

    Team *playerTeam = teamOfPlayer(player, state);
    playerTeam->setScore(playerTeam->score() + points);
}
