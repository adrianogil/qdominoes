#ifndef GAMECONFIG_H
#define GAMECONFIG_H

#include <QObject>
#include "qtsynthesize.h"

class PlayerController;
class GameState;

#define PLAYERS_NUMBER 4

class GameConfig : public QObject
{
    Q_OBJECT
public:
    enum PlayerType {
        LocalHumanPlayer = 0,
        NPCPlayer = 1,
        RemotePlayer = 2
    };
    explicit GameConfig(QObject *parent = 0);

    Q_SYNTHESIZE(PlayerType, player1, Player1, player1Changed)
    Q_SYNTHESIZE(PlayerType, player2, Player2, player2Changed)
    Q_SYNTHESIZE(PlayerType, player3, Player3, player3Changed)
    Q_SYNTHESIZE(PlayerType, player4, Player4, player4Changed)

    Q_SYNTHESIZE(int, player1Int, Player1Int, player1Changed)
    Q_SYNTHESIZE(int, player2Int, Player2Int, player1Changed)
    Q_SYNTHESIZE(int, player3Int, Player3Int, player1Changed)
    Q_SYNTHESIZE(int, player4Int, Player4Int, player1Changed)

    QList<PlayerController*> createPlayers();
    GameState* createInitialGameState();

    PlayerController* localPlayer() { return m_localPlayerController; }
    QList<PlayerController*> players() { return m_players; }
signals:
    void player1Changed();
    void player2Changed();
    void player3Changed();
    void player4Changed();

private:
    PlayerController* m_localPlayerController;
    QList<PlayerController*> m_players;

};

#endif // GAMECONFIG_H
