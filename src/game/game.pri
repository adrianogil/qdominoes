INCLUDEPATH += $$PWD 

HEADERS += \ 
    $$PWD/handcontroller.h \
    $$PWD/gamecontroller.h \
    $$PWD/tilemodel.h \
    $$PWD/team.h \
    $$PWD/dominoesaction.h \
    $$PWD/table/tablemodel.h \
    $$PWD/table/tablecontroller.h \
    $$PWD/scorecontroller.h \
    $$PWD/matchcontroller.h \
    $$PWD/dominoesutils.h \
    $$PWD/gameconfig.h \
    $$PWD/player/playercontroller.h \
    $$PWD/player/npccontroller.h \
    $$PWD/player/localplayercontroller.h \
    $$PWD/player/playermodel.h \
    $$PWD/player/playerturncontroller.h

SOURCES += \ 
    $$PWD/table/tablecontroller.cpp \
    $$PWD/handcontroller.cpp \
    $$PWD/gamecontroller.cpp \
    $$PWD/team.cpp \
    $$PWD/dominoesaction.cpp \
    $$PWD/scorecontroller.cpp \
    $$PWD/matchcontroller.cpp \
    $$PWD/dominoesutils.cpp \
    $$PWD/gameconfig.cpp \
    $$PWD/player/playercontroller.cpp \
    $$PWD/player/npccontroller.cpp \
    $$PWD/player/localplayercontroller.cpp \
    $$PWD/player/playerturncontroller.cpp


















