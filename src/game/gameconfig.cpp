#include "gameconfig.h"
#include "gamestate.h"
#include "team.h"
#include "player/localplayercontroller.h"
#include "player/playercontroller.h"
#include "player/npccontroller.h"
#include "player/playermodel.h"

GameConfig::GameConfig(QObject *parent) :
    QObject(parent)
{
}

QList<PlayerController *> GameConfig::createPlayers()
{
    // Clear old player configuration
    m_players.clear();

    /** Creating and configuring Players **/
    // Create Player tied with local screen
    m_localPlayerController = new LocalPlayerController(this);

    m_localPlayerController->setPlayerModel(new PlayerModel("Player", "", 0, this));

    m_players.append(m_localPlayerController);

    // Create bots
    for (int p = 1; p < PLAYERS_NUMBER; p++) {
        NPCController *bot = new NPCController(this);
        bot->setPlayerModel(new PlayerModel("Bot" + QString::number(p), "", p, this));
        m_players.append(bot);
    }


    return m_players;
}

GameState * GameConfig::createInitialGameState()
{
    GameState *gameState = new GameState(this);
    gameState->setTeamA(new Team(m_players[0]->playerModel(),
                                   m_players[2]->playerModel(), 0, this));
    gameState->setTeamB(new Team(m_players[1]->playerModel(),
                                   m_players[3]->playerModel(), 0, this));

    return gameState;
}
