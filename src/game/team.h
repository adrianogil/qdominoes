#ifndef TEAM_H
#define TEAM_H

#include <QObject>

class Team : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QObject* players1 READ players1 WRITE setPlayers1 NOTIFY player1Changed)
    Q_PROPERTY(QObject* players2 READ players2 WRITE setPlayers2 NOTIFY player2Changed)
    Q_PROPERTY(int score READ score WRITE setScore NOTIFY scoreChanged)

public:
    explicit Team(QObject *parent = 0);

    Team(QObject *player1, QObject *player2, int score, QObject *parent = 0)
        : QObject(parent) {
        setPlayers1(player1);
        setPlayers2(player2);
        setScore(score);
    }

    void setPlayers1(QObject* player) {
        m_player1 = player;
        emit player1Changed();
    }

    QObject* players1() {return m_player1;}

    void setPlayers2(QObject* player) {
        m_player2 = player;
        emit player2Changed();
    }

    QObject* players2() {return m_player2;}

    void setScore(int score) {m_score = score;}
    int score() {return m_score;}

signals:
    void player1Changed();
    void player2Changed();
    void scoreChanged();
    
public slots:

private:
    QObject* m_player1;
    QObject* m_player2;
    int m_score;
    
};

#endif // TEAM_H
