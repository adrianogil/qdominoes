#include "team.h"
#include "gamehost.h"
#include "tilemodel.h"
#include "gamecontroller.h"
#include "dominoesaction.h"
#include "table/tablemodel.h"
#include "player/playercontroller.h"
#include "player/playermodel.h"
#include "dominoesutils.h"

#define POINTS_WHEN_PASSING 20
#define POINTS_FOR_GALO 50

#define NUMBER_OF_PLAYERS 4

DominoesAction::DominoesAction(QObject *parent) :
    GameAction(parent)
{
}

void DominoesAction::applyToState(GameState *gameState)
{
    GameAction::applyToState(gameState);
}

TilePlayAction::TilePlayAction(PlayerModel *playerModel, TileModel *tileModel, const int &cornerPlayed, QObject *parent)
    : DominoesAction(parent)
{
    setTileModel(tileModel);
    setPlayerModel(playerModel);
    setCornerPlayed(cornerPlayed);

    setHasEcho(true);
}

void TilePlayAction::applyToState(GameState *gameState)
{
    DominoesAction::applyToState(gameState);
    if (!gameState)
        return;
    TableModel *tableModel = qobject_cast<TableModel*>(gameState->tableModel());

    if (!tableModel) {
        // Adding first Tile
        gameState->setTableModel(new TableModel(tileModel()));
    } else {
        TableModel *t = tableModel;
        TableModel *pT = 0;
        QObject *lastTileObj = tableModel->tileModel();
        while (t) {
            if (pT)
                lastTileObj = pT->tileModel();
            pT = t;
            t = t->corner(m_cornerPlayed);
        }
        if (pT) {
            TileModel *lastTile = qobject_cast<TileModel*>(lastTileObj);
            if (lastTile && lastTile->sideB() == m_tileModel->sideB()) {
                m_tileModel->setSideB(m_tileModel->sideA());
                m_tileModel->setSideA(lastTile->sideB());
            }
            pT->setCorner(cornerPlayed(), m_tileModel);
        }
    }
    playerModel()->setNumberOfTilesOnHand(playerModel()->numberOfTilesOnHand() - 1);
}

PlayerPassAction::PlayerPassAction(PlayerModel *playerModel, QObject *parent)
    : DominoesAction(parent)
{
    setPlayerModel(playerModel);
}

bool PlayerPassAction::verifyConditions(GameState *gameState)
{

    debugMsg;
    if (!gameState)
        return false;

    debugMsg << (gameState->currentPlayer() == playerModel());

    return gameState->currentPlayer() == playerModel();
}

PlayerTurnBeginAction::PlayerTurnBeginAction(PlayerModel *playerModel, QObject *parent)
    : DominoesAction(parent)
{
    setPlayerModel(playerModel);
    setHasEcho(true);
}

bool PlayerTurnBeginAction::verifyConditions(GameState *gameState)
{
    Q_UNUSED(gameState);

    QObject *controller = GameHost::sharedControllers("gameController");
    GameController *gameController = qobject_cast<GameController*>(controller);

    if (!gameController)
        return false;

    PlayerController *player = DominoesUtils::getPlayerFromModel(playerModel());

    if (!player)
        return false;

    return gameController->canPlay(player);
}

void PlayerTurnBeginAction::applyToState(GameState *gameState)
{
    DominoesAction::applyToState(gameState);
    gameState->setCurrentPlayer(playerModel());
}

void PlayerTurnBeginAction::onActionFailed(GameState *gameState)
{
    Q_UNUSED(gameState);

    QObject *controller = GameHost::sharedControllers("gameController");
    GameController *gameController = qobject_cast<GameController*>(controller);

    if (!gameController)
        return;

    PlayerController *player = DominoesUtils::getPlayerFromModel(playerModel());

    if (!player)
        return;

    player->passCurrentTurn();
}

PlayerTurnEndAction::PlayerTurnEndAction(PlayerModel *playerModel, QObject *parent)
    : DominoesAction(parent)
{
    setPlayerModel(playerModel);
    setHasEcho(true);
}

bool PlayerTurnEndAction::verifyConditions(GameState *gameState)
{
    if (!gameState)
        return false;

    return gameState->currentPlayer() == playerModel();
}
void PlayerTurnEndAction::applyToState(GameState *gameState)
{
    DominoesAction::applyToState(gameState);
    gameState->setCurrentPlayer(0);
}

GaloYellAction::GaloYellAction(PlayerModel *playerModel, QObject *parent)
    : DominoesAction(parent)
{
    setPlayerModel(playerModel);
}

bool GaloYellAction::verifyConditions(GameState *gameState)
{
    if (!gameState)
        return false;

    return gameState->currentPlayer() == playerModel();
}

FinishMatchAction::FinishMatchAction(PlayerModel *playerModel, int matchNumber, QObject *parent)
    : DominoesAction(parent)
{
    setPlayerModel(playerModel);
    setMatchNumber(matchNumber);
}

bool FinishMatchAction::verifyConditions(GameState *gameState)
{
    if (!gameState)
        return false;

    QObject *controller = GameHost::sharedControllers("gameController");
    GameController *gameController = qobject_cast<GameController*>(controller);

    bool result = gameState->matchNumber() == matchNumber();

    if (gameController)
        result = result && gameController->isThereAPlayerWithEmptyHand();

    return result;
}

StartMatchAction::StartMatchAction(PlayerModel *playerModel, int matchNumber, QObject *parent)
{
    setPlayerModel(playerModel);
    setMatchNumber(matchNumber);
    setHasEcho(true);
}

bool StartMatchAction::verifyConditions(GameState *gameState)
{
    Q_UNUSED(gameState);

    QObject *controller = GameHost::sharedControllers("gameController");
    GameController *gameController = qobject_cast<GameController*>(controller);

    bool result = false;

    if (gameController)
        result = gameController->players().length() == NUMBER_OF_PLAYERS;

    return result;
}

void StartMatchAction::applyToState(GameState *gameState)
{
    gameState->setTableModel(0);
}







