#include <QtCore/QDateTime>
#include <QtCore/QDebug>

#include "matchcontroller.h"
#include "scorecontroller.h"
#include "gamecontroller.h"
#include "handcontroller.h"
#include "dominoesaction.h"
#include "gamestate.h"
#include "team.h"

#include "gameconfig.h"

#include "player/playerturncontroller.h"
#include "player/playercontroller.h"
#include "player/playermodel.h"

#include "table/tablecontroller.h"
#include "table/tablemodel.h"

#include "appdebug.h"

GameController::GameController(QObject* parent)
    : GameHost(parent)
{
    debugMsg;
    m_gameConfig = new GameConfig(this);
}


void GameController::startGame() {

    // Clear old player configuration
    m_players.clear();

    /** Initialize Controllers and States **/
    m_players = m_gameConfig->createPlayers();

    // Get local player controller - available after creating all players controllers
    m_localPlayerController = m_gameConfig->localPlayer();

    // Creating a variable to resume the overall gameState
    m_gameState = m_gameConfig->createInitialGameState();
    emit gameStateMayHaveChanged();

    // Creating a controller of game table
    m_tableController = new TableController(this);

    // Creating a controller of game score
    m_scoreController = new ScoreController(this);

    // Creating a controller of all players turn
    m_playerTurnController = new PlayerTurnController(this);

    // Creating a controller of all created Matches
    m_matchController = new MatchController(this);

    /** Configuring GameHost **/
    // Adding Game Objects that must receive actions
    for (int p = 0; p < PLAYERS_NUMBER; p++)
        gameObjects.append(m_players[p]);

    gameObjects.append(m_scoreController);
    gameObjects.append(m_matchController);
    gameObjects.append(m_playerTurnController);

    // Adding shared controllers
    GameHost::addSharedController("gameController", this);
    GameHost::addSharedController("tableController", m_tableController);
    GameHost::addSharedController("scoreController", m_scoreController);
    GameHost::addSharedController("matchController", m_matchController);
    GameHost::addSharedController("handController", m_localPlayerController->handController());

    /** Starting Game **/
    // Start Match
    m_matchController->startMatch();

    emit gameStarted();
}

QObject * GameController::playerController()
{
    return (QObject*) m_localPlayerController;
}

QObject * GameController::tableController()
{
    return (QObject*) m_tableController;
}

void GameController::updateTableModel()
{
    if (m_tableController) {
        m_tableController->setTableModel(m_gameState->tableModel());
    }
}

// Main loop
void GameController::updateAll(const GameTime &gameTime)
{
    debugMsg;
    GameHost::updateAll(gameTime);

    // Update Table Model
    m_tableController->updateTableModel(m_gameState->tableModel());

    emit gameStateMayHaveChanged();
}

bool GameController::canPlay(PlayerController *player)
{
    bool result = false;

    foreach (TileModel* tile, player->handTiles()) {
        if (m_tableController->canFitTileOnCorners(tile))
            result = true;
    }

    return result;
}

bool GameController::isThereAPlayerWithEmptyHand()
{
    bool result = false;

    foreach (PlayerController *player, m_players)
        if (player->handTiles().isEmpty()) {
            result = true;
            break;
        }

    return result;
}

QObject * GameController::gameConfigObj()
{
    return (QObject*) m_gameConfig;
}

void GameController::startLoop()
{
    startGameLoop(1);
}

void GameController::warnsPlayerHasPassed(PlayerModel *player)
{
    if (player)
        emit playerPassed(player->name());
}
