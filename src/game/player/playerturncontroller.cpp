#include "playerturncontroller.h"
#include "playercontroller.h"
#include "dominoesaction.h"
#include "dominoesutils.h"
#include "gamecontroller.h"
#include "gameconfig.h"

PlayerTurnController::PlayerTurnController(QObject *parent) :
    GameObjectController(parent)
{
}


void PlayerTurnController::startGame(PlayerController *firstPlayer)
{
    if (!firstPlayer) {
        debugMsg << "Error when find out first player";
        return;
    }

    // Setting first player
    setupPlayerTurn(firstPlayer);
}

void PlayerTurnController::receiveAction(GameAction *action)
{
    debugMsg << action;
    CHECK_CAST(action, playerTurnBeginAction, PlayerTurnBeginAction*, onPlayerTurnBeginActionReceived);
    CHECK_CAST(action, playerTurnEndAction, PlayerTurnEndAction*, onPlayerTurnEndActionReceived);
    CHECK_CAST(action, playerPassAction, PlayerPassAction*, onPlayerPassActionReceived);
    CHECK_CAST(action, startMatchAction, StartMatchAction*, onStartMatchActionReceived);
}

void PlayerTurnController::onPlayerPassActionReceived(PlayerPassAction *playerPassAction)
{
    if (!playerPassAction && !playerPassAction->playerModel())
        return;

    setupPlayerTurn(nextPlayer(DominoesUtils::getPlayerFromModel(playerPassAction->playerModel())));

    GET_SHARED_CONTROLLER("gameController", GameController*, gameController);

    if (gameController)
        gameController->warnsPlayerHasPassed(playerPassAction->playerModel());
}

void PlayerTurnController::onPlayerTurnEndActionReceived(PlayerTurnEndAction *playerTurnEndAction)
{
    if (!playerTurnEndAction || !playerTurnEndAction->playerModel())
        return;

    PlayerController *currentPlayer = DominoesUtils::getPlayerFromModel(playerTurnEndAction->playerModel());

    GET_SHARED_CONTROLLER("gameController", GameController*, gameController);

    if (gameController)
        gameController->removeActionSource(currentPlayer);

    setupPlayerTurn(nextPlayer(currentPlayer));
}

void PlayerTurnController::onPlayerTurnBeginActionReceived(PlayerTurnBeginAction *playerTurnBeginAction)
{
}

void PlayerTurnController::setupPlayerTurn(PlayerController *player)
{
    if (!player) {
        debugMsg << "Error when setup player turn: Null Player reference!";
        return;
    }

    player->startTurn();
    m_gameState->setCurrentPlayer(player->playerModel());

    GET_SHARED_CONTROLLER("gameController", GameController*, gameController);

    if (gameController)
        gameController->setAsActionSource(player, -1);
}

void PlayerTurnController::onStartMatchActionReceived(StartMatchAction *startMatchAction)
{
    if (!startMatchAction) {
        debugMsg << "Error when receiving action. Null reference of 'StartMatchAction'!";
        return;
    }
    debugMsg << startMatchAction->playerModel();
    startGame(DominoesUtils::getPlayerFromModel(startMatchAction->playerModel()));
}

PlayerController * PlayerTurnController::nextPlayer(PlayerController *player)
{
    GET_SHARED_CONTROLLER("gameController", GameController*, gameController);

    if (!gameController) {
        debugMsg << "Error when getting shared controller. Null reference of 'GameController'!";
        return 0;
    }

    QList<PlayerController*> players = gameController->gameConfig()->players();

    int i = players.indexOf(player);

    PlayerController *nextPlayer = players[(i+1)%players.length()];

    return nextPlayer;
}
