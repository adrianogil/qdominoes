#include "npccontroller.h"
#include "dominoesaction.h"
#include "playermodel.h"
#include "table/tablemodel.h"

#include "appdebug.h"

NPCController::NPCController(QObject *parent)
    : PlayerController(parent)
    , m_aiBehavior(Randomic)
{
    initializeInternalState();
}

void NPCController::updatePlayerActions()
{
    if (m_gameState && m_gameState->currentPlayer() == playerModel()) {
        debugMsg << m_currentState << "My Bot Turn" << playerModel() << playerModel()->name();
        // Current Player
        // Turn States:
        // -> begin
        // --> where must be verified if player can play or must pass
        // -> Wait player play tile
        // -> Tiled Played
        // --> verify if the current turn must be ended
        // -> end
        TableModel *tableModel = m_gameState->tableModel();
        switch(m_currentState) {
        case BeginTurn:
            debugMsg << m_currentState << tableModel;
            if (!tableModel || !tableModel->tileModel()) {
                m_actionBuffer.append(new TilePlayAction(playerModel(), new TileModel(6,6,this), -1, this));
            } else
                chooseTile();
            break;
        case TiledPlayed:
            break;
        case EndTurn:
            m_actionBuffer.append(new PlayerTurnEndAction(playerModel(),this));
            break;
        default:
            break;
        }
    }
}

void NPCController::startTurn()
{
    debugMsg << m_currentState << playerModel()->name();
    if (m_currentState == OutsideTurn) {
        GameAction *gameAction = new PlayerTurnBeginAction(playerModel(),this);
        debugMsg << gameAction << playerModel()->name();
        m_actionBuffer.append(gameAction);
    }
}

void NPCController::receiveAction(GameAction *action)
{
    PlayerController::receiveAction(action);

    CHECK_CAST(action, tilePlayAction, TilePlayAction*, updateInternalState);
    CHECK_CAST(action, playPassAction, PlayerPassAction*, updateInternalState);
}

GameAction* NPCController::takeNextAction()
{
    debugMsg << playerModel()->name();
    return GameObjectController::takeNextAction();
}

void NPCController::initializeInternalState()
{
    // Initialize the vectors os Game Status
    m_tilesTable.fill(0, 7);
    m_tilesHand.fill(0, 7);
    m_tilesFreeTips.fill(0, 7);
    m_playedTilesPartner.fill(0, 7);
    m_tilesPassedNextEnemy.fill(0, 7);
    m_tilesPassedPreviousEnemy.fill(0, 7);
    m_tilesPassedPartner.fill(0, 7);

    m_totalPlayed.fill(0,4);

    m_bestTile = new TileModel(-1,-1,this);
    m_bestCorner = -1;
    m_initialTile = new TileModel(-1,-1,this);

    m_freeTips.clear();
    m_freePointsTips.clear();

    m_alreadyPlayedAtUpSide = false;
    m_alreadyPlayedAtRightSide = false;
    m_alreadyPlayedAtDownSide = false;
    m_alreadyPlayedAtLeftSide = false;
}

void NPCController::updateInternalState(TilePlayAction *tilePlayAction)
{
    if (!tilePlayAction)
        return;
 \
    PlayerModel *playerModel = qobject_cast<PlayerModel*>(tilePlayAction->playerModel());

    if (playerModel) {
        m_totalPlayed[playerModel->order()]++;

        TileModel *tile = qobject_cast<TileModel*>(tilePlayAction->tileModel());

        if (tile && tile->isValidTile()) {

            // Update tiles on tables
            if (tile->sideA() == tile->sideB()) {
                m_tilesTable[tile->sideA()]++;
            } else {
                m_tilesTable[tile->sideA()]++;
                m_tilesTable[tile->sideB()]++;
            }

            if (m_freeTips.length() < 4) { // Table is empty
                m_freeTips.append(tile->sideA());
                m_freeTips.append(tile->sideA());
                m_freeTips.append(tile->sideA());
                m_freeTips.append(tile->sideA());

                m_freePointsTips.append(tile);
                m_freePointsTips.append(tile);
                m_freePointsTips.append(tile);
                m_freePointsTips.append(tile);

                m_initialTile = tile;
            } else {
                int corner = tilePlayAction->cornerPlayed();

                if (corner == 0)
                    m_alreadyPlayedAtUpSide = true;
                else if (corner == 1)
                    m_alreadyPlayedAtRightSide = true;
                else if (corner == 2)
                    m_alreadyPlayedAtDownSide = true;
                else if (corner == 3)
                    m_alreadyPlayedAtLeftSide = true;
                else if (corner > 3 || corner < 0)
                    return;

                m_freeTips.replace(corner, tile->sideA() == m_freeTips[corner]? tile->sideB() : tile->sideA());
                m_freePointsTips.replace(corner,tile);
            }
        }
    }
}

void NPCController::updateInternalState(PlayerPassAction *playPassAction)
{
    if (!playPassAction || playPassAction->playerModel() == playerModel())
        return;

    PlayerModel *passedPlayerModel = qobject_cast<PlayerModel*>(playPassAction->playerModel());

    if (!passedPlayerModel)
        return;

    PlayerModel *npcPlayerModel = qobject_cast<PlayerModel*>(playerModel());

    if (!npcPlayerModel)
        return;

    if (passedPlayerModel->order() == (npcPlayerModel->order() + 1) % 4) {
        // The Next Opposing player
        foreach (int a, m_freeTips)
            m_tilesPassedNextEnemy[a] = 1;
    } else if (passedPlayerModel->order() == (4 + npcPlayerModel->order() - 1) % 4) {
        // The Previous Opposing player
        foreach (int a, m_freeTips)
            m_tilesPassedPreviousEnemy[a] = 1;
    } else if (passedPlayerModel->order() == (npcPlayerModel->order() + 2) % 4) {
        // The NPC partner
        foreach (int a, m_freeTips)
            m_tilesPassedPartner[a] = 1;
    }
}

/**
 * Tell whether the bot can yell Galo
 *
 * @param int l1 -> side 1 of the choosed tile
 * @param int l2 -> side 2 of the choosed tile
 * @param int direction -> tell one of following directions:
 *              0 => UP
 *              1 => RIGHT
 *              2 => DOWN
 *              3 => LEFT
 * @return bool -> true if the bot can yell Galo, false otherwise
 **/
bool NPCController::willBeGalo(int l1, int l2, int direction)
{
    Q_UNUSED(l1);

    if ((m_initialTile && m_initialTile->sideA() == -1) || m_freeTips.length() == 0 ||
            m_freePointsTips.length() ==  0)
        return false;

    bool galoForSure = true;

    for(int d = 0; d < 4; d++) {
        int totalsum = 0;
        if (d == direction) {
            totalsum = totalsum + m_tilesTable[l2] + m_tilesHand[l2];
        } else {
            totalsum = totalsum + m_tilesTable[m_freeTips.at(d)] + m_tilesHand[m_freeTips.at(d)];
        }
        galoForSure = galoForSure && (totalsum == 7);
    }

    return galoForSure;
}

double NPCController::applyRandomicHeuristic(int l1, int l2, int direction)
{
    Q_UNUSED(l2);

    if (m_freeTips.length() == 4 && m_freeTips.at(direction) == l1) {
        return qrand() % 30;
    }
    return -1;
}

int NPCController::calculatePointsPassing(int l1, int l2, int direction)
{
    if (m_freePointsTips.length() != 4 || m_freeTips.length() != 4)
        return 0;

    Q_UNUSED(direction);

    // Vector 4 -
    QVector<int> beliefTilesPassNextEnemy;
    foreach (int a, m_tilesPassedNextEnemy)
        beliefTilesPassNextEnemy << a;

    // Vector 5 -
    QVector<int> beliefTilesPassPreviousEnemy;
    foreach (int a, m_tilesPassedPreviousEnemy)
        beliefTilesPassPreviousEnemy << a;

    // Vector 6 -
    QVector<int> beliefTilesPassPartner;
    foreach (int a, m_tilesPassedPartner)
        beliefTilesPassPartner << a;

    int sum1 = 0;
    int sum2 = 0;
    int sum3 = 0;
    bool alreadyUsedFreeTip = false;

    foreach(int side, m_freeTips)
    {
        int tileValue = 0;
        if (!alreadyUsedFreeTip && side == l1)
        {
            tileValue = l2;
            alreadyUsedFreeTip = true;
        }
        else tileValue = side;
        int count = m_tilesTable[tileValue] + m_tilesFreeTips[tileValue];

        if (count == 7) {
            beliefTilesPassNextEnemy[tileValue] = 1;
            beliefTilesPassPreviousEnemy[tileValue] = 1;
            beliefTilesPassPartner[tileValue] = 1;
        }

        sum1 = sum1 + beliefTilesPassNextEnemy[tileValue];
        sum2 = sum2 + beliefTilesPassPreviousEnemy[tileValue];
        sum3 = sum3 + beliefTilesPassPartner[tileValue];
    }

    int sum = sum1 + sum2 + sum3;

    if (sum == 21)
        return 50;
    else if (sum1 == 7)
        return 20;

    return 0;
}

int NPCController::calculateMaxPoints(int l1, int l2, int direction)
{
    if (m_freePointsTips.length() != 4 || m_freeTips.length() != 4)
        return 0;

    int points = 0;

    if (m_freeTips.at(direction) != l1)
        return -1;
    else if (!(m_alreadyPlayedAtRightSide && m_alreadyPlayedAtLeftSide) && (direction == 0 || direction == 2)) {
        return -1; // CAN'T DO SUCH MOVEMENT
    } else if (m_alreadyPlayedAtRightSide && m_alreadyPlayedAtLeftSide) {

        if (l1 == l2)
            points = l1 + l2;
        else points = l2;

        for (int b = 0; b < m_freeTips.length(); b++)
            if (b != direction) {

                if ((m_freePointsTips.at(b)->sideA() == m_initialTile->sideA() &&
                     m_freePointsTips.at(b)->sideB() == m_initialTile->sideB())) {
                    points = points + 0;
                } else if(m_freePointsTips.at(b)->sideA() == m_freePointsTips.at(b)->sideB()) {
                    // Carroça Tile means double points
                    points = points + 2 * m_freeTips.at(b);
                }else points = points + m_freeTips.at(b);
            }
    } else { //Playing with right or left side open
        if (l1 == l2)
            points = l1 + l2;
        else points = l2;

        if (!m_alreadyPlayedAtRightSide && direction == 3) {
            points = points + 2*m_initialTile->sideA();
        } else if (!m_alreadyPlayedAtLeftSide && direction == 1) {
            points = points + 2*m_initialTile->sideB();
        } else {
            int b = (direction + 2) % 4;
            if(m_freePointsTips.at(b)->sideA() == m_freePointsTips.at(b)->sideB() &&
                    !(m_freePointsTips.at(b)->sideA() == m_initialTile->sideA() &&
                      m_freePointsTips.at(b)->sideB() == m_initialTile->sideB()))
                points = points + 2 * m_freeTips.at(b);
            else points = points + m_freeTips.at(b);
        }

    }

    return (points % 5 == 0 ? points : 0);
}

double NPCController::evaluateHeuristic(int l1, int l2, int direction, double alpha)
{
    if (m_freeTips.length() < 4)
        return l1 + l2;

    if (m_freeTips.at(direction) != l1)
        return -1;

    double k1 = 0.1;
    double k2 = 0.3;
    double k3 = 0.6;
    double sigma = 0;

    int points_evaluation = 0;
    int points_type1 = 0;
    int points_typePassing = 0;
    int points_type5 = 0;

    double strategic_evaluation = alpha;
    double heuristicEvaluation = 0;
    double evaluation_term1 = 0;
    double evaluation_term2 = 0;
    double evaluation_term3 = 0;


    // Calculating points type 1
    // Max points can be done with such tile configuration
    points_type1 = calculateMaxPoints(l1, l2, direction);

    //Calculating points type 2 and 3
    points_typePassing = calculatePointsPassing(l1, l2, direction);

    // Calculating points type 5
    // Finish the game with a double
    TileModelList hand = handTiles();
    if (l1 == l2 && hand.length() == 1)
        points_type5 = 20;
    else points_type5 = 0;

    points_evaluation = points_type1 + points_typePassing + points_type5;

    evaluation_term1 = k1 * (m_tilesHand[l1] + m_tilesTable[l1] + m_tilesFreeTips[l1]);
    evaluation_term2 = k2 * (m_tilesHand[l2] + m_tilesTable[l2] + m_tilesFreeTips[l2]);

    int total_hand = hand.length();
    if (total_hand > 3)
        sigma = 0;
    else sigma = 1;
    evaluation_term3 = k3 * m_tilesHand[l2];

    strategic_evaluation = alpha * ( - evaluation_term1 + evaluation_term2 + sigma * evaluation_term3 );

    heuristicEvaluation = strategic_evaluation + points_evaluation;

    return heuristicEvaluation;
}

double NPCController::applyGreedyHeuristic(int l1, int l2, int direction)
{
    return evaluateHeuristic(l1, l2, direction, 0);
}

double NPCController::applyStrategicHeuristic(int l1, int l2, int direction)
{
    return evaluateHeuristic(l1, l2 , direction, 0.2);
}

double NPCController::applyHeuristic(int l1, int l2, int direction)
{
    if (m_aiBehavior == Greedy)
        return applyGreedyHeuristic(l1, l2, direction);
    else if (m_aiBehavior == Randomic)
        return applyRandomicHeuristic(l1, l2, direction);
    else if (m_aiBehavior == StrategicHeuristic)
        return applyStrategicHeuristic(l1, l2, direction);

    return -10;
}

void NPCController::chooseTile(bool onlyDouble)
{
    debugMsg << "Start";

    // Initialize choices
    m_bestTile = new TileModel(-1,-1,this);
    m_bestCorner = -1;
    int greaterPoints = -1;

    // Get all tiles on hand
    TileModelList hand = handTiles();

    for (int dir = 0; dir <= 3; dir++) {

        // Can't play at up or down until the both side are opened
        if (!(m_alreadyPlayedAtRightSide && m_alreadyPlayedAtLeftSide) &&
                (dir == 0 || dir == 2))
            continue;

        // Heuristic values for each hand tile
        QList<int> handHeuristicEvaluation;

        for(int b = 0; b < hand.length(); b++) {
            TileModel *tile = hand.at(b);

            if (tile->sideA() == tile->sideB()) {
                if (onlyDouble)
                    handHeuristicEvaluation.append((tile->sideA() + tile->sideB()) % 5 == 0?
                                                       (tile->sideA() + tile->sideB()) : 0);
                else handHeuristicEvaluation.append(applyHeuristic(tile->sideA(), tile->sideB(), dir));
            } else if (onlyDouble) {
                handHeuristicEvaluation.append(-1);
            } else {
                int l1 = tile->sideA();
                int l2 = tile->sideB();
                int h1 = applyHeuristic(l1, l2, dir);
                int h2 = applyHeuristic(l2, l1, dir);
                if (h2 > h1) {
                    // @TODO Verify when the best heuristic value is with switched the tile side (sideA <=> sideB)
                    tile->setSideA(l2);
                    tile->setSideB(l1);
                    handHeuristicEvaluation.append(h2);
                } else
                    handHeuristicEvaluation.append(h1);
            }

            if (handHeuristicEvaluation[b] != -1)
            debugMsg << "(" << tile->sideA() << "," << tile->sideB() << ") direction: " << dir
                     << " Heuristic value: " << handHeuristicEvaluation[b];

        }

        int bigger = -1;
        int greaterTile = -1;

        for(int a = 0; a < handHeuristicEvaluation.length(); a++)
        {
            if(handHeuristicEvaluation.at(a) > bigger)
            {
                bigger = handHeuristicEvaluation.at(a);
                greaterTile = a;
            }

        }

        if (bigger > greaterPoints) {
            greaterPoints = bigger;
            m_bestCorner = dir;
            m_bestTile = new TileModel(hand.at(greaterTile)->sideA(), hand.at(greaterTile)->sideB(), this);
        }
    }

    if (greaterPoints == -1) {
        m_bestTile->setSideA(-1);
        m_bestTile->setSideB(-1);
        // @TODO Bot must pass
    }

    if (m_bestTile->isValidTile()) {
        debugMsg << "Choosed Tile (" << m_bestTile->sideA() << "," << m_bestTile->sideB() << ")";
        m_actionBuffer.append(new TilePlayAction(playerModel(),m_bestTile,m_bestCorner,this));
        m_currentState = TiledPlayed;
    } else debugMsg << "Failed when choosing a tile!";
}
