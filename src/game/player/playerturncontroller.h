#ifndef PLAYERTURNCONTROLLER_H
#define PLAYERTURNCONTROLLER_H

#include "gameobjectcontroller.h"

class PlayerTurnBeginAction;
class PlayerTurnEndAction;
class PlayerPassAction;
class PlayerController;
class StartMatchAction;

class PlayerTurnController : public GameObjectController
{
    Q_OBJECT
public:
    explicit PlayerTurnController(QObject *parent = 0);

    void startGame(PlayerController *firstPlayer);

    void receiveAction(GameAction *action);
private:
    void onPlayerPassActionReceived(PlayerPassAction *playerPassAction);
    void onPlayerTurnEndActionReceived(PlayerTurnEndAction *playerTurnEndAction);
    void onPlayerTurnBeginActionReceived(PlayerTurnBeginAction *playerTurnBeginAction);
    void onStartMatchActionReceived(StartMatchAction *startMatchAction);

    void setupPlayerTurn(PlayerController* player);
    PlayerController* nextPlayer(PlayerController* player);

};

#endif // PLAYERTURNCONTROLLER_H
