#ifndef PLAYERCONTROLLER_H
#define PLAYERCONTROLLER_H

#include <QObject>
#include "tilemodel.h"
#include "gameobjectcontroller.h"

class PlayerModel;
class HandController;
class GameState;
class PlayerTurnBeginAction;
class PlayerTurnEndAction;
class TilePlayAction;

class PlayerController : public GameObjectController
{
    Q_OBJECT

    Q_PROPERTY(QObject *handController READ handController NOTIFY handControllerChanged)
    Q_PROPERTY(QObject *playerModel READ playerModelObj NOTIFY playerModelChanged)

    Q_SYNTHESIZE(PlayerModel*, playerModel, PlayerModel, playerModelChanged)
public:
    explicit PlayerController(QObject *parent = 0);

    Q_INVOKABLE QObject *handController();

    void setHandTiles(TileModelList tileList);
    TileModelList handTiles();

    QObject* playerModelObj() { return (QObject*) m_playerModel; }

    virtual void startTurn() {}
    virtual void passCurrentTurn();
    virtual void receiveAction(GameAction* action);

protected:
    void updateState(GameState *gameState);
    virtual void updatePlayerActions() = 0;

    enum PlayerStates {
        BeginTurn = 0,
        WaitingTile = 1,
        TiledPlayed = 2,
        EndTurn = 3,
        OutsideTurn = 4
    };
    PlayerStates m_currentState;

signals:
    void handControllerChanged();
    void playerModelChanged();

public slots:

private slots:
    void updateActionBuffer();
    void onPlayerTurnBeginActionReceived(PlayerTurnBeginAction *playerTurnBeginAction);
    void onTilePlayActionReceived(TilePlayAction *tilePlayAction);
    void onPlayerTurnEndActionReceived(PlayerTurnEndAction *playerTurnEndAction);

private:
    HandController *m_handController;

};

#endif // PLAYERCONTROLLER_H
