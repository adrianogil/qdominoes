#include "handcontroller.h"
#include "localplayercontroller.h"
#include "dominoesaction.h"
#include "appdebug.h"
#include "playermodel.h"

LocalPlayerController::LocalPlayerController(QObject *parent)
    : PlayerController(parent)
    , m_allowMoveHandTiles(false)
{
    m_currentState = OutsideTurn;
    HandController* hand = qobject_cast<HandController*>(handController());
    connect(hand, SIGNAL(dropTileOnTable(QObject*,int)),
            this, SLOT(droppedTileOnTable(QObject*,int)));
}

void LocalPlayerController::updatePlayerActions()
{
    if (m_gameState->currentPlayer() == playerModel()) {
        debugMsg << playerModel() << m_currentState;
        // Current Player
        // Turn States:
        // -> begin
        // --> where must be verified if player can play or must pass
        // -> Wait player play tile
        // -> Tiled Played
        // --> verify if the current turn must be ended
        // -> end
        switch(m_currentState) {
        case OutsideTurn:
            break;
        case BeginTurn:
            setAllowMoveHandTiles(true);
            m_currentState = WaitingTile;
            debugMsg << "Allowed Move hand tiles: " << allowMoveHandTiles();
            break;
        case WaitingTile:
            break;
        case TiledPlayed:
            break;
        case EndTurn:
            setAllowMoveHandTiles(false);
            m_actionBuffer.append(new PlayerTurnEndAction(playerModel(),this));
            break;
        default:
            break;
        }
    }
}

void LocalPlayerController::startTurn()
{
    debugMsg << m_currentState << playerModel()->name();
    if (m_currentState == OutsideTurn)
        m_actionBuffer.append(new PlayerTurnBeginAction(playerModel(),this));
}

void LocalPlayerController::droppedTileOnTable(QObject *tileModel, const int &corner)
{
    debugMsg << tileModel << corner;
    TileModel *tile = qobject_cast<TileModel*>(tileModel);
    m_actionBuffer.append(new TilePlayAction(playerModel(),tile, corner, this));
    m_currentState = TiledPlayed;
}
