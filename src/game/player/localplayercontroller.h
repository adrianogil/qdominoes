#ifndef LOCALPLAYERCONTROLLER_H
#define LOCALPLAYERCONTROLLER_H

#include "playercontroller.h"
#include "qtsynthesize.h"

class LocalPlayerController : public PlayerController
{
    Q_OBJECT

    Q_PROPERTY(bool allowMoveHandTiles READ allowMoveHandTiles NOTIFY allowMoveHandTilesChanged)

    Q_SYNTHESIZE(bool, allowMoveHandTiles, AllowMoveHandTiles, allowMoveHandTilesChanged)
public:
    explicit LocalPlayerController(QObject *parent = 0);

    void startTurn();

protected:
    void updatePlayerActions();

signals:
    void allowMoveHandTilesChanged();

public slots:
    void droppedTileOnTable(QObject *tileModel, const int &corner);

private:

};

#endif // LOCALPLAYERCONTROLLER_H
