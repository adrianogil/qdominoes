#ifndef NPCCONTROLLER_H
#define NPCCONTROLLER_H

#include "playercontroller.h"
#include "qtsynthesize.h"
#include "tilemodel.h"

#include <QtCore/QVector>

class TilePlayAction;
class PlayerPassAction;

class NPCController : public PlayerController
{
    Q_OBJECT

public:
    enum AIBehaviorType {
        Greedy,
        Randomic,
        StrategicHeuristic
    };

private:
    Q_ENUMS(AIBehaviorType)

    Q_SYNTHESIZE(AIBehaviorType, aiBehavior, AiBehavior, aiBehaviorChanged)
public:
    explicit NPCController(QObject *parent = 0);

    void startTurn();

    void receiveAction(GameAction *action);
    GameAction* takeNextAction();

protected:
    void updatePlayerActions();

signals:
    void aiBehaviorChanged();

public slots:

private:
    void initializeInternalState();

    void updateInternalState(TilePlayAction *tilePlayAction);
    void updateInternalState(PlayerPassAction *playPassAction);

    /** Heuristic search related-function **/
    void chooseTile(bool onlyDouble = false);
    double applyHeuristic(int l1, int l2, int direction);
    double applyStrategicHeuristic(int l1, int l2, int direction);
    double applyGreedyHeuristic(int l1, int l2, int direction);
    double evaluateHeuristic(int l1, int l2, int direction,double alpha = 0.2);
    int calculateMaxPoints(int l1, int l2, int direction);
    int calculatePointsPassing(int l1, int l2, int direction);
    double applyRandomicHeuristic(int l1, int l2, int direction);
    bool willBeGalo(int l1, int l2, int direction);

    /** Variables necessary in order to running AI algorithms **/
    // Vector 0 - All tiles on the table
    QVector<int> m_tilesTable;
    // Vector 1 - All tiles on hand
    QVector<int> m_tilesHand;
    // Vector 2 -
    QVector<int> m_tilesFreeTips;
    // Vector 3 -
    QVector<int> m_playedTilesPartner;
    // Vector 4 -
    QVector<int> m_tilesPassedNextEnemy;
    // Vector 5 -
    QVector<int> m_tilesPassedPreviousEnemy;
    // Vector 6 -
    QVector<int> m_tilesPassedPartner;

    QVector<int> m_totalPlayed;

    // Just the number of each free side
    QList<int> m_freeTips;
    TileModelList m_freePointsTips;

    int m_bestCorner;
    TileModel *m_bestTile;

    TileModel *m_initialTile;

    bool m_alreadyPlayedAtUpSide;
    bool m_alreadyPlayedAtRightSide;
    bool m_alreadyPlayedAtDownSide;
    bool m_alreadyPlayedAtLeftSide;
};

#endif // NPCCONTROLLER_H
