/**************************************************************************
This file is part of QDominoes v0.1 alpha.

QDominoes is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

QDominoes is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with QDominoes.  If not, see <http://www.gnu.org/licenses/>.

You can also see the COPYING and README files for more info on
copying and contacting the original author.

Contacting the authors: projeto_ceteli_nokia@googlegroups.com

Feel free to use the application and to upgrade it's features but remember:
this is a GPL software, so always give credit to the original author.
**************************************************************************/

#ifndef PLAYER_H_
#define PLAYER_H_

#include <QtCore/QObject>
#include <QtCore/QString>
#include "qtsynthesize.h"

class PlayerModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int order READ order NOTIFY orderChanged)
    Q_PROPERTY(QString name READ name NOTIFY nameChanged)
    Q_PROPERTY(QString avatarPath READ avatarPath NOTIFY avatarPathChanged)
    Q_PROPERTY(int numberOfTilesOnHand READ numberOfTilesOnHand NOTIFY numberOfTilesOnHandChanged)


    Q_SYNTHESIZE(int, order, Order, orderChanged)
    Q_SYNTHESIZE(QString, name, Name, nameChanged)
    Q_SYNTHESIZE(QString, avatarPath, AvatarPath, avatarPathChanged)
    Q_SYNTHESIZE(int, numberOfTilesOnHand, NumberOfTilesOnHand, numberOfTilesOnHandChanged)

public:
    PlayerModel(QObject *parent = 0) : QObject(parent) {}
    PlayerModel(QString name, QString avatar="", QObject *parent = 0)
        : QObject(parent) {
        setName(name);
        setAvatarPath(avatar);
    }
    PlayerModel(const QString &name, const QString &avatar, const int &order , QObject *parent = 0)
        : QObject(parent) {
        setName(name);
        setAvatarPath(avatar);
        setOrder(order);
    }

signals:
    void orderChanged();
    void nameChanged();
    void avatarPathChanged();
    void numberOfTilesOnHandChanged();
};

#endif
