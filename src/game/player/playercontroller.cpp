#include "playercontroller.h"
#include "handcontroller.h"
#include "dominoesaction.h"
#include "player/playermodel.h"

#include "appdebug.h"

PlayerController::PlayerController(QObject *parent)
    : GameObjectController(parent)
    , m_handController(0)
    , m_currentState(OutsideTurn)
{
    m_handController = new HandController(this);

    connect(this, SIGNAL(gameStateChanged()),
            this, SLOT(updateActionBuffer()));
}

QObject* PlayerController::handController()
{
    return (QObject*) m_handController;
}

void PlayerController::setHandTiles(TileModelList tileList)
{
    if (m_handController)
        m_handController->setTilesModel(TileModel::convertToObj(tileList));
}

void PlayerController::updateActionBuffer()
{

}

void PlayerController::updateState(GameState *gameState)
{
    GameObjectController::updateState(gameState);

    if (!m_gameState) {
        debugMsg << "Null GameState for current player: " << playerModel()->name();
        return;
    }

    updatePlayerActions();
}

TileModelList PlayerController::handTiles()
{
    return TileModel::convertFromObj(m_handController->tilesModel());
}

void PlayerController::receiveAction(GameAction *action)
{
    if (!action) {
        debugMsg << "Null action";
        return;
    }

    CHECK_CAST(action, playerTurnBeginAction, PlayerTurnBeginAction*, onPlayerTurnBeginActionReceived);
    CHECK_CAST(action, tilePlayAction, TilePlayAction*, onTilePlayActionReceived);
    CHECK_CAST(action, playerTurnEndAction, PlayerTurnEndAction*, onPlayerTurnEndActionReceived);
}

void PlayerController::passCurrentTurn()
{
    debugMsg;
    m_actionBuffer.append(new PlayerPassAction(playerModel(), this));
}

void PlayerController::onPlayerTurnBeginActionReceived(PlayerTurnBeginAction *playerTurnBeginAction)
{
    if (playerTurnBeginAction &&
            playerTurnBeginAction->playerModel() == playerModel()) {
        debugMsg << "My Turn" << playerModel();
        m_currentState = BeginTurn;
    }
}

void PlayerController::onTilePlayActionReceived(TilePlayAction *tilePlayAction)
{
    if (tilePlayAction &&
            tilePlayAction->playerModel() == playerModel()) {
        // Remove tile from hand
        TileModel* tile = qobject_cast<TileModel*>(tilePlayAction->tileModel());
        m_handController->removeTileFromHand(tile);
        // Successfull tile played and then must end the turn
        m_currentState = EndTurn;
        debugMsg << "new state: " << m_currentState << playerModel();
        // If the last tile was played then player must yell end of the match
        if (handTiles().length() == 0)
            m_actionBuffer.append(new FinishMatchAction(playerModel(), m_gameState->matchNumber(),this));
    }
}

void PlayerController::onPlayerTurnEndActionReceived(PlayerTurnEndAction *playerTurnEndAction)
{
    if (playerTurnEndAction && playerTurnEndAction->playerModel() == playerModel()) {
        m_currentState = OutsideTurn;
    }
}


