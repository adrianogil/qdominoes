#include "matchcontroller.h"
#include "dominoesaction.h"
#include "dominoesutils.h"
#include "gamecontroller.h"
#include "player/playercontroller.h"
#include "player/playermodel.h"
#include "team.h"

#define PLAYERS_NUMBER 4

MatchController::MatchController(QObject *parent) :
    GameObjectController(parent)
{
}

void MatchController::receiveAction(GameAction *action)
{
    CHECK_CAST(action, startMatchAction, StartMatchAction*, onStartMatchActionReceived);
    CHECK_CAST(action, finishMatchAction, FinishMatchAction*, onFinishMatchActionReceived);
}

void MatchController::onStartMatchActionReceived(StartMatchAction *startMatchAction)
{
    if (!startMatchAction)
        return;


}

void MatchController::onFinishMatchActionReceived(FinishMatchAction *finishMatchAction)
{
    if (!finishMatchAction)
        return;

    QObject *controller = GameHost::sharedControllers("gameController");
    GameController *gameController = qobject_cast<GameController*>(controller);

    if (!gameController)
        return;

    int pointsTeamA = m_gameState->teamA()->score();
    int pointsTeamB = m_gameState->teamB()->score();

    if (pointsTeamA == pointsTeamB && pointsTeamA >= 200) {
        // Drawback
        gameController->warnsDrawback();
    } if (m_gameState->teamA()->score() >= 200) {
        // Team A winner
        gameController->warnsTeamAWins();
    } else if (m_gameState->teamB()->score()) {
        // Team B winner
        gameController->warnsTeamBWins();
    } else {
        // Create new match
        startMatch();
    }
}

void MatchController::startMatch()
{
    debugMsg;
    GET_SHARED_CONTROLLER("gameController", GameController*, gameController);

    if (!gameController) {
        debugMsg << "Error when getting shared controller. Null reference of 'GameController'";
        return;
    }

    QList<PlayerController*> players = gameController->players();

    // Setting hand tiles
    QList<TileModelList > m_playersHand = DominoesUtils::shuffleTiles();

    for (int p = 0; p < PLAYERS_NUMBER; p++) {
        PlayerController *playerController = players.at(p);
        playerController->setHandTiles(m_playersHand.at(p));
        playerController->playerModel()->setNumberOfTilesOnHand(7);
    }

    PlayerController *firstPlayer = DominoesUtils::firstPlayerInCurrentMatch();

    if (!firstPlayer) {
        debugMsg << "Error when find out first player.";
        return;
    }

    m_actionBuffer.append(new StartMatchAction(firstPlayer->playerModel(), m_matchNumber, this));

    gameController->setAsActionSource(this);
}
