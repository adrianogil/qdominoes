#ifndef DOMINOESUTILS_H
#define DOMINOESUTILS_H

#include <QtCore/QList>
#include "tilemodel.h"

class PlayerController;
class PlayerModel;
class GameState;
class Team;

namespace DominoesUtils {
    //! Shuffle all tiles of all players
    QList< QList<TileModel*> > shuffleTiles();
    //!
    PlayerController* firstPlayerInCurrentMatch(const TileModel &initialTile = TileModel(6,6));
    //!
    PlayerController* getPlayerFromModel(PlayerModel* playerModel);
    //!
    Team* teamOfPlayer(PlayerModel *player, GameState *state);
    //!
    Team* opposingTeamOfPlayer(PlayerModel *player, GameState *state);
    //!
    void addPointsToTeamOfPlayer(PlayerModel *player, const int &points, GameState *state);
    //!
    void addPointsToOpposingTeamOfPlayer(PlayerModel *player, const int &points, GameState *state);
    //! Calculate points from last tile when finishing a match
    void lastTilePointsFromOpposingTeamOfPlayer(PlayerModel *player, GameState *state);
}

#endif // DOMINOESUTILS_H
