#ifndef SCORECONTROLLER_H
#define SCORECONTROLLER_H

#include "gameobjectcontroller.h"

class TilePlayAction;
class PlayerPassAction;
class GaloYellAction;
class FinishMatchAction;

class ScoreController : public GameObjectController
{
    Q_OBJECT
public:
    ScoreController(QObject *parent = 0);

    void receiveAction(GameAction *action);
private:
    void onTilePlayActionReceived(TilePlayAction *tilePlayAction);
    void onPlayerPassActionReceived(PlayerPassAction *playerPassAction);
    void onGaloYellActionReceived(GaloYellAction *galoYellAction);
    void onFinishMatchActionReceived(FinishMatchAction *finishMatchAction);
};

#endif // SCORECONTROLLER_H
