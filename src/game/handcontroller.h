#ifndef HANDCONTROLLER_H
#define HANDCONTROLLER_H

#include <QObject>
#include "qtsynthesize.h"
#include "tilemodel.h"

class HandController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QObjectList tilesModel READ tilesModel NOTIFY tilesModelChanged)

    Q_SYNTHESIZE(QObjectList, tilesModel, TilesModel, tilesModelChanged)
public:
    explicit HandController(QObject *parent = 0);

signals:
    void tilesModelChanged();
    void removedTile(QObject *tileModel);
    // TileModel representing the dropped tile and corner must be a int between 0 and 3
    void dropTileOnTable(QObject *tileModel, const int &corner);

public slots:
    void addTileToHand(TileModel *tileModel);
    void addTileToHand(int sideA, int sideB);
    void removeTileFromHand(TileModel *tileModel);
    void removeTileFromHand(int sideA, int sideB);
    void reorderTiles(QObject* tile, const int &newPosition);
};

#endif // HANDCONTROLLER_H
