#ifndef TABLECONTROLLER_H
#define TABLECONTROLLER_H

#include <QtCore/QObject>
#include "tilemodel.h"
#include "qtsynthesize.h"

class TileModel;

class TableController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QObject* tableModel READ tableModel NOTIFY tableModelChanged)

    Q_SYNTHESIZE(QObject*, tableModel, TableModel, tableModelChanged)
public:
    explicit TableController(QObject *parent = 0);

    Q_INVOKABLE bool isValidMove(const QString &cornerDirection, const int &sideA, const int &sideB);

    bool canFitTileOnCorners(TileModel* tileModel);

    void updateTableModel(QObject* tableModel) {
        setTableModel(tableModel);
        emit tableModelChanged();
    }

signals:
    void tableModelChanged();
    void scoreAnimation(int sideA, int sideB, int points, int corners);

public slots:
    void doScoreAnimation(int sideA, int sideB, int points, int corners);
};

#endif // TABLECONTROLLER_H
