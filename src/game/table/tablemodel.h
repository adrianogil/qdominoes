#ifndef TABLEMODEL_H
#define TABLEMODEL_H

#include <QtCore/QObject>
#include <QtCore/QMap>
#include <QDebug>

#include "qtsynthesize.h"
#include "tilemodel.h"

class CornerModel : public QObject {

    Q_OBJECT

    Q_PROPERTY(QObject* tableModel READ tableModel NOTIFY tableModelChanged)
    Q_PROPERTY(QString direction READ direction NOTIFY directionChanged)

    Q_SYNTHESIZE(QObject*, tableModel, TableModel, tableModelChanged)
    Q_SYNTHESIZE(QString, direction, Direction, directionChanged)
public:
    CornerModel(QObject* tableModel, QString direction, QObject *parent = 0) : QObject(parent) {
        setTableModel(tableModel);
        setDirection(direction);
    }

signals:
    void tableModelChanged();
    void directionChanged();
};

class TableModel : public QObject
{
    Q_OBJECT
    Q_ENUMS(CornerPosition)

    Q_PROPERTY(QObject* cornerUp READ cornerUp WRITE setCornerUp NOTIFY cornerUpChanged)
    Q_PROPERTY(QObject* cornerDown READ cornerDown WRITE setCornerDown NOTIFY cornerDownChanged)
    Q_PROPERTY(QObject* cornerLeft READ cornerLeft WRITE setCornerLeft NOTIFY cornerLeftChanged)
    Q_PROPERTY(QObject* cornerRight READ cornerRight WRITE setCornerRight NOTIFY cornerRightChanged)
    Q_PROPERTY(QObject* tileModel READ tileModel WRITE setTileModel NOTIFY tileModelChanged)

    Q_SYNTHESIZE(QObject*, cornerUp, CornerUp, cornerUpChanged)
    Q_SYNTHESIZE(QObject*, cornerDown, CornerDown, cornerDownChanged)
    Q_SYNTHESIZE(QObject*, cornerLeft, CornerLeft, cornerLeftChanged)
    Q_SYNTHESIZE(QObject*, cornerRight, CornerRight, cornerRightChanged)

    Q_SYNTHESIZE(QObject*, tileModel, TileModel, tileModelChanged)
public:
    enum CornerPosition {
        Up = 0,
        Right = 1,
        Down = 2,
        Left = 3
    };

    TableModel(QObject* tileModel,QObject* parent = 0) : QObject(parent)
    {
        setTileModel(tileModel);
        setCornerUp(0);
        setCornerDown(0);
        setCornerLeft(0);
        setCornerRight(0);
    }

    TableModel* corner(const int &corner) {
        QObject* cornerObj = 0;
        switch(corner) {
        case Up:
            cornerObj = cornerUp();
            break;
        case Down:
            cornerObj = cornerDown();
            break;
        case Left:
            cornerObj = cornerLeft();
            break;
        case Right:
            cornerObj = cornerRight();
            break;
        default:
            cornerObj = 0;
            break;
        }

        return qobject_cast<TableModel*>(cornerObj);
    }

    static int directionFromString(const QString &direction) {
        int directionValue = -1;

        if (direction == "Up")
            directionValue = 0;
        else if (direction == "Right")
            directionValue = 1;
        else if (direction == "Down")
            directionValue = 2;
        else if (direction == "Left")
            directionValue = 3;

        return directionValue;
    }

    static QString strDirection(const int &cornerPosition) {
        QString direction = "";
        switch(cornerPosition) {
        case Up:
            direction = "Up";
            break;
        case Down:
            direction = "Down";
            break;
        case Left:
            direction = "Left";
            break;
        case Right:
            direction = "Right";
            break;
        default:
            direction = "";
            break;
        }

        return direction;
    }

    Q_INVOKABLE QObject* cornerModel(const int &cornerValue) {
        QObject* cornerModel = new CornerModel(corner(cornerValue), strDirection(cornerValue));

        return cornerModel;
    }

    Q_INVOKABLE QObject* cornerModel(const QString &cornerDirection) {
//        qDebug() << __PRETTY_FUNCTION__ << cornerDirection << directionFromString(cornerDirection)
//                 << corner(directionFromString(cornerDirection));
        QObject* cornerModel = new CornerModel(corner(directionFromString(cornerDirection)), cornerDirection);

        return cornerModel;
    }

    Q_INVOKABLE QObject* lastCornerOnDirection(const QString &cornerDirection) {
        int direction = directionFromString(cornerDirection);
        TableModel* tableModel = this;
        while (tableModel->corner(direction) != 0)
            tableModel = tableModel->corner(direction);
        return (QObject*) tableModel;
    }

    Q_INVOKABLE void print(int level = 0, QString direction = "first") {
        QString msg = "";
        for (int i = 0; i <= level; i++)
            msg = msg + '=';
        if (tileModel()) {
            TileModel *tile = qobject_cast<TileModel*>(tileModel());
            msg = msg + " direction: " + direction + " - tile (" + QString::number(tile->sideA()) + "," +
                    QString::number(tile->sideB()) + ")";
        }
        qDebug() << "C++::TableModel: " << msg;
        for (int c = 0; c < 4; c++) {
            if (corner(c)) {
                TableModel *tableModel = qobject_cast<TableModel*>(corner(c));
                tableModel->print(level+1, strDirection(c));
            }
        }
    }

    TileModelList tilesOnCorners() {
        TileModelList tiles;
        for (int c = 0; c < 4; c++) {
            TableModel *tableModel = qobject_cast<TableModel*>(lastCornerOnDirection(strDirection(c)));
            tiles.append(qobject_cast<TileModel*>(tableModel->tileModel()));
        }
        return tiles;
    }

    QMap<QString, TileModel*> mapTilesOnCorners() {
        QMap<QString, TileModel*> mapTiles;
        for (int c = 0; c < 4; c++) {
            TableModel *tableModel = qobject_cast<TableModel*>(lastCornerOnDirection(strDirection(c)));
            mapTiles[strDirection(c)] = qobject_cast<TileModel*>(tableModel->tileModel());
        }
        return mapTiles;
    }

    void setCorner(const int &corner, QObject* tileModel) {
        TableModel* tModel = new TableModel(tileModel, this);
        switch(corner) {
        case Up:
            setCornerUp((QObject*) tModel);
            break;
        case Down:
            setCornerDown((QObject*) tModel);
            break;
        case Left:
            setCornerLeft((QObject*) tModel);
            break;
        case Right:
            setCornerRight((QObject*) tModel);
            break;
        default:
            break;
        }
    }

    bool canPlay(TileModelList handList) {
        foreach (TileModel* tile, handList) {
            for (int d = 0; d < 4; d++) {
                TableModel *model = qobject_cast<TableModel*>(lastCornerOnDirection(strDirection(d)));

                if (model) {
                    TileModel *cornerTile = qobject_cast<TileModel*>(model->tileModel());

                    if (cornerTile && (cornerTile->sideB() == tile->sideA() ||
                                       cornerTile->sideB() == tile->sideB()))
                        return true;
                }
            }
        }

        return false;
    }

signals:
    void cornerUpChanged();
    void cornerDownChanged();
    void cornerLeftChanged();
    void cornerRightChanged();
    void tileModelChanged();
private:

};

#endif // TABLEMODEL_H
