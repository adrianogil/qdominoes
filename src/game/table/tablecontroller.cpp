#include <QtCore/QStringList>
#include "tablecontroller.h"
#include "tablemodel.h"

TableController::TableController(QObject *parent) :
    QObject(parent)
{
    setTableModel(0);
}

bool TableController::isValidMove(const QString &cornerDirection, const int &sideA, const int &sideB)
{
    if (cornerDirection == "first")
        return true;

    if (!m_tableModel) {
        return sideA == sideB && sideA == 6;
    }

    TableModel* tableModel = qobject_cast<TableModel*>(m_tableModel);
    QObject *lastCorner = tableModel->lastCornerOnDirection(cornerDirection);
    TableModel* tableModelFromLastCorner = qobject_cast<TableModel*>(lastCorner);
    TileModel* tileModel = qobject_cast<TileModel*>(tableModelFromLastCorner->tileModel());

    bool validMove = sideA == tileModel->freeSide() || sideB == tileModel->freeSide();

    return validMove;
}

bool TableController::canFitTileOnCorners(TileModel *tileModel)
{
    if (!m_tableModel) {
        return (tileModel->sideA() == 6 && tileModel->sideB() == 6);
    }

    QStringList directions;
    directions << "Up" << "Right" << "Down" << "Left";

    for (int i = 0; i < directions.length(); i++) {
        if (isValidMove(directions[i],tileModel->sideA(), tileModel->sideB()) ||
                isValidMove(directions[i], tileModel->sideB(), tileModel->sideA()))
            return true;
    }

    return false;
}

void TableController::doScoreAnimation(int sideA, int sideB, int points, int corners)
{
    emit scoreAnimation(sideA, sideB, points, corners);
}
