// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item {
    id: tile

    width: tileImg.width; height: tileImg.height
    smooth: true

    // Model
    property QtObject tileModel

    //Point to go back, change when reorder the hand
    property real initialX
    property real initialY

    // Tile Rotation
    property int tileRotation: 0
    property int tileCorrection: 0

    // Top number
    property int firstValue: 0

    // Bottom number
    property int secondValue: 0

    property url tileImage;

    property bool animationRunning: false
    property bool enableAnimation: true
    property bool enableMouseArea: true

    property bool allowMove: false

    signal released
    signal moved
    signal pressed

    function updateImagePath(sideA, sideB) {
        if(sideA > sideB){
            var temp = sideA
            sideA = sideB
            sideB = temp
        };

        return "qrc:/tiles/"+sideA+sideB+".png"
    }

    function doScoreAnimation(points,corners) {
        console.debug('Tile.qml - doScoreAnimation - points: ' + points +
                      'on corner ' + corners);
        scoreAnimation.scored(points)
    }

    onTileModelChanged: {
        firstValue = tileModel.sideA
        secondValue = tileModel.sideB
        tile.tileImage = updateImagePath(tileModel.sideA, tileModel.sideB);
        if (tileModel.sideA > tileModel.sideB)
            tile.tileCorrection = 0;
        else tile.tileCorrection = 180;
    }

    Behavior on opacity {
        enabled: enableAnimation
        NumberAnimation {
            duration: 500
            onRunningChanged: animationRunning = running;
        }
    }

    Behavior on x {
        enabled: enableAnimation
        NumberAnimation {
            duration: 200
            onRunningChanged: animationRunning = running;
        }
    }

    Behavior on y {
        enabled: enableAnimation
        NumberAnimation {
            duration: 200
            onRunningChanged: animationRunning = running;
        }
    }

    Behavior on rotation {
        enabled: enableAnimation
        NumberAnimation {
            duration: 800
            onRunningChanged: {
                animationRunning = running;
            }
        }
    }

    Image {
        id: tileImg
        source: tile.tileImage
        visible: tile.state != "hidden"
        rotation: tile.tileRotation + tile.tileCorrection
    }

    ScoreAnimation {
        id: scoreAnimation
    }

    function setCornerScoreAnimation(corners) {
        switch(corners) {
        case 0:
            scoreAnimation.x = tile.baselineOffset
            scoreAnimation.y =  -(tile.height/2 + tile.y - 10)
            break;
        case 1:
            scoreAnimation.x = tile.x + 2*tile.width - 10
            scoreAnimation.y =  tile.y + tile.height/3
            break;
        case 2:
            scoreAnimation.x = tile.baselineOffset
            scoreAnimation.y =  tile.height + tile.y
            break;
        case 3:
            scoreAnimation.x = -(tile.x + 2*tile.width - 10)
            scoreAnimation.y =  tile.y + tile.height/3
            break;
        default:
            scoreAnimation.x = tile.baselineOffset
            scoreAnimation.y =  -(tile.height/2 + tile.y)
            break;
        }
    }

    MouseArea{
        anchors.fill: parent
        drag.target: parent
        drag.axis: Drag.XandYAxis
        enabled: tile.enableMouseArea && tile.allowMove
        onDoubleClicked: {
            //switch the values and invert the tile
            if (tile.state == 'on-hand') {
                console.debug('Tile.qml - onDoubleClicked - state: ' + tile.state);
                tile.rotation += 180
                var temp = tile.tileModel.sideA
                tile.tileModel.sideA = tile.tileModel.sideB
                tile.tileModel.sideB = temp
            }
        }
        onPressed: {
            tile.pressed()
        }

        onReleased: {
            //must return to hand or stay on the table
            tile.released()
        }
        onPositionChanged: {
            //use it to verify position at the game table
            tile.moved()
        }
    }
}
