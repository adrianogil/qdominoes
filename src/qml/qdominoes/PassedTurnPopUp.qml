// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../ViewsDirector/js/Director.js" as ViewsDirector
import "../../ViewsDirector/qml"

AbstractPopup {
   id: closeAppPopUp
   property variant fontFamily
   property string playerName: "Você"

   Image {
       id: box
       anchors.centerIn: parent
       source: "qrc:/PopUps/PopUp_passturn/caixa-passe-vez.png"

       Text {
           id: text
           font.family: fontFamily.name
           font.pixelSize: 20
           color: "white"
           wrapMode: Text.WordWrap
           text:playerName + qsTr(" passou a vez!")
           anchors {
                verticalCenter: box.verticalCenter
                horizontalCenter: box.horizontalCenter
           }
       }
   }

     Timer {
         id: timerPopUp
         interval: 1000
         repeat: false
         triggeredOnStart: false
         onTriggered: {
             ViewsDirector.closePopup()
         }
     }

     Component.onCompleted: {
         timerPopUp.start()
     }
}
