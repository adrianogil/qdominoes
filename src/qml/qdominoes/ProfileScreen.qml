import QtQuick 1.1
import "../../ViewsDirector/js/Director.js" as ViewsDirector
import "../../ViewsDirector/qml"

AbstractView {

    id: profileScreen

    Image {
        id: menuLogo
        source: "qrc:/Perfil/perfil.png"
        anchors.horizontalCenter: profileScreen.horizontalCenter
        anchors.top: profileScreen.top
        anchors.topMargin: 20

    }

    GridView {
        id: profileList
       // spacing: 30
        width: profileScreen.width*0.6
        height: profileScreen.height*0.4
        y: profileScreen.height*0.3

        cellWidth: width/2; cellHeight: height/2

        interactive: false
       // clip: true

        anchors.centerIn: parent

        delegate: delegate
        model: profileController.playerListModel

    }

    Component{
        id: delegate

        Profile {
            id: profile
            modelItem: model.modelData

            width: profileList.width/2; height: profileList.height/2
            propertyName: modelItem.name ?  modelItem.name  :  "Novo perfil"
            propertyAvatarPath: modelItem.avatarPath ? modelItem.avatarPath : ""
            current: profileList.currentIndex == index
            onClicked: {
                if(modelItem.name && modelItem.avatarPath) {
                    profileList.currentIndex = index
                } else {
                    ViewsDirector.nextViewWithArgs("newProfile",{"newProfile": true })
                }
            }
        }
    }

    Row{
        id: buttons
        spacing: 10
        anchors{
            horizontalCenter: profileScreen.horizontalCenter
            top: profileList.bottom
        }

        Button{
            id: trashButton
            buttonImage: "qrc:/Perfil/excluir.png"
            pressedImage: "qrc:/Perfil/excluir-press.png"
            onClicked:{
                console.log("remove" + profileList.currentItem.data.name)
                ViewsDirector.showPopupWithArgs("deleteProfilePopUp",{"listItem":profileList.currentItem.modelItem})
                profileList.currentIndex = 0
            }
        }

        Button{
            id: editButton
            buttonImage: "qrc:/Perfil/editar.png"
            pressedImage: "qrc:/Perfil/editar-press.png"
            onClicked:{
                if(ViewsDirector.currentView() != "newProfile")
                    ViewsDirector.nextViewWithArgs("newProfile",{"oldProfileName":profileList.currentItem.modelItem.name,
                                                       "oldAvatarPath":profileList.currentItem.modelItem.avatarPath,
                                                       "newProfile": false })
                profileList.currentIndex = 0
            }
        }

        Button{
            id: okButton
            buttonImage: "qrc:/Perfil/ok.png"
            pressedImage: "qrc:/Perfil/ok-press.png"
            MouseArea {
                anchors.fill: okButton
                onClicked: {
                    if(profileController.existAnyProfile()) {
                            if(ViewsDirector.currentView() != "menu")
                                ViewsDirector.nextView("menu")
                    }
                }
            }
        }
    }

    Button{
        id: exitButton
        buttonImage: "qrc:/Fechar.png"
        pressedImage: "qrc:/Fechar.png"
        onClicked: {
            ViewsDirector.showPopup("closeAppPopUp")
        }
        anchors{
            top: profileScreen.top
            topMargin: 15
            rightMargin: 15
            right: profileScreen.right
        }
    }

    Component.onCompleted: {
        profileController.updateListProfiles()
    }

}
