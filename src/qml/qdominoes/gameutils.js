
//! Returns true with the objects t1 and t2 are colliding
function verifyCollision(t1,t2)
{
    var isInYArea = (t1.y < t2.y + t2.height) && (t1.y + t1.height > t2.y);
    var isInXArea = (t1.x < t2.x + t2.width) && (t1.x + t1.width > t2.x);

    return isInXArea && isInYArea;
}
