// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../ViewsDirector/js/Director.js" as ViewsDirector
import "../../ViewsDirector/qml"

AbstractPopup {
   id: moveBackMenuPopUp
   property variant fontFamily
   property string description: "Tem certeza que deseja fechar o jogo e voltar para o menu principal ?"

   Image {
       id: box
       anchors.centerIn: parent
       source: "qrc:/PopUps/PopUp_fechar/Caixa-popup-fechar.png"

       Text {
           id: text
           font.family: fontFamily.name
           font.pixelSize: 17
           color: "white"
           wrapMode: Text.WordWrap
           text:description
           anchors {
               verticalCenter: box.verticalCenter
               verticalCenterOffset: -30
               left: box.left
               leftMargin: 40
               right: box.right
               rightMargin: 30
           }

       }

       Button {
           id: closePopUp
           buttonImage: "qrc:/PopUps/PopUp_fechar/botao-nao.png"
           pressedImage: "qrc:/PopUps/PopUp_fechar/botao-nao-press.png"

           anchors{
               leftMargin: 18
               left: box.left
               bottom: parent.bottom
               bottomMargin: 13
           }

           onClicked: {
               ViewsDirector.closePopup()
           }
       }

       Button {
           id: notClosePopUp
           buttonImage: "qrc:/PopUps/PopUp_fechar/botao-sim.png"
           pressedImage: "qrc:/PopUps/PopUp_fechar/botao-sim-press.png"

           anchors{
               rightMargin: 18
               right: box.right
               bottom: parent.bottom
               bottomMargin: 13
           }

           onClicked: {
               ViewsDirector.closePopup()
               if(ViewsDirector.currentView() != "menu")
                   ViewsDirector.nextView("menu")
           }
       }
   }
}
