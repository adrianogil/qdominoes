// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item {
    id: player

    property url avatarImage
    property string playerName
    property int playerScore

    width: avatarShape.width
    height: avatarShape.height

    Image {
        id: avatarShape
        source: "qrc:/scoreTableImgs/moldura_avatar.png"

        Image {
            id: avatarBackground
            source: "qrc:/scoreTableImgs/avatar.png"
            anchors.centerIn: avatarShape

            Image {
                id: image
                source: avatarImage
                anchors.fill: avatarBackground
            }
        }

    }

}
