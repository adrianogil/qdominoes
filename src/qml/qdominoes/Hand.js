Qt.include("gameutils.js");
Qt.include("Tile.js");

var tileObjList = []
var indexByTileObj = {}

function getTileObj(sideA,sideB) {
    var tileObj = null;

    for (var t = 0; t < tileObjList.length; t++) {
        var tModel = tileObjList[t].tileModel;
        if ((tModel.sideA == sideA && tModel.sideB == sideB) ||
                (tModel.sideA == sideB && tModel.sideB == sideA)) {
            tileObj = tileObjList[t];
        }
    }

    return tileObj;
}

function sendTileToTable(sideA,sideB,corner) {
    console.debug('Hand.js - sendTileToTable - Tile(' + sideA + ',' + sideB + ') to corner: ' + corner);
    var tileObj = getTileObj(sideA,sideB);
    handController.dropTileOnTable(tileObj.tileModel, corner);
    tileObj.visible = false;
    hand.releasedTile();
}

function sendbackTileToHand(sideA,sideB) {
    var tileObj = getTileObj(sideA,sideB);
    // Return to its correct position
    tileObj.state = 'on-hand'
    updateTilePosition(tileObj, indexByTileObj[tileObj]);
    hand.releasedTile();
}

function onTileMoved(tileObj)
{

    // Send a signal to table warning a tile is moving
    if (tileObj.y < 0 || tileObj.y > hand.height) {
        hand.movedTileOnTable(tileObj.x, tileObj.y,
                              tileObj.width, tileObj.height,
                              tileObj.tileModel.sideA, tileObj.tileModel.sideB);
    }

    tileObj.state = 'dragging'
    // Check collision with another tile
    for (var i = 0; i < tileObjList.length; i++) {
        var tile = tileObjList[i];
        if (tile.state == 'on-hand' && !tile.animationRunning &&
                verifyCollision(tileObj,tile)) {
            handController.reorderTiles(tileObj.tileModel, indexByTileObj[tile]);
        }
    }
}

function onTileReleased(tileObj)
{
    hand.releasedTile();
    console.debug('Hand.js - onTileReleased - tileObj: (' + tileObj.x + ',' + tileObj.y + ')');
    if (tileObj.x > 0 && tileObj.x < hand.width && tileObj.y >= 0) {
        // Return to its correct position
        tileObj.state = 'on-hand'
        updateTilePosition(tileObj, indexByTileObj[tileObj]);
    } else {
        // Prepate to drop tile on table
        hand.releasedTileOnTable(tileObj.x, tileObj.y,
                                 tileObj.width, tileObj.height,
                                 tileObj.tileModel.sideA,
                                 tileObj.tileModel.sideB);
    }
}

function onTileRemoved(tileObj)
{
    var id = tileObjList.valueOf(tileObj);
    if (id != -1)
        tileObjList.splice(id,1);
    delete indexByTileObj[tileObj];

}

function updateTilePosition(tileObj, index)
{
    indexByTileObj[tileObj] = index;
    if (tileObj.state == 'on-hand') {
        tileObj.y = 0;
        tileObj.x = index * (tileObj.width + modelContainer.spacing)
    }

}

function invokeTileElement(parent, options, index)
{

    // Check for duplicated tiles
    for (var i = 0; i < tileObjList.length; i++) {
        var tile = tileObjList[i];
        if (tile.tileModel.isEqual(options["tileModel"])) {
            updateTilePosition(tile, index);
            return;
        }
    }

    createTileObj(parent, options, function(tileObj) {
                      tileObj.state = 'on-hand'
                      indexByTileObj[tileObj] = index;
                      tileObjList[tileObjList.length] = tileObj;
                      tileObj.moved.connect(function() { onTileMoved(tileObj) });
                      tileObj.released.connect(function() { onTileReleased(tileObj)});
                      hand.rotateTile.connect(function(angle){
                                                  if(tileObj && tileObj.state == "dragging"){
                                                       tileObj.rotation = angle - tileObj.tileCorrection
                                                  }
                                              });
                      hand.rotateBack.connect(function(){
                                                  if(tileObj && tileObj.state == "dragging"){
                                                    tileObj.rotation = 0
                                                  }
                                              });
                      hand.stateChanged.connect(function() {
                                                    if (tileObj != undefined && tileObj.state == 'on-hand') {
                                                        if (hand.state == 'hide')
                                                            tileObj.opacity = 0;
                                                        else if (hand.state == 'show')
                                                            tileObj.opacity = 1;
                                                    } } );
                      hand.interactionAllowedChanged.connect(function() {
                                                                 if (tileObj != undefined)
                                                                     tileObj.allowMove = hand.interactionAllowed;
                                                             });
                  });
}
