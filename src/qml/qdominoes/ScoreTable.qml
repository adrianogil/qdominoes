import QtQuick 1.1

Item {
    id: scoreTable
    state: "NORMAL"

    width: background.width
    height: background.height

    property QtObject gameState

    property url playerLeftUpAvatar:  gameState.teamA.players1.avatarPath
    property url playerLeftDownAvatar: gameState.teamA.players2.avatarPath
    property url playerRightUpAvatar: gameState.teamB.players1.avatarPath
    property url playerRightDownAvatar: gameState.teamB.players2.avatarPath

    property string playerLeftUpName: gameState.teamA.players1.name
    property string playerLeftDownName: gameState.teamA.players2.name
    property string playerRightUpName: gameState.teamB.players1.name
    property string playerRightDownName: gameState.teamB.players2.name

    property int scorePlayersLeft: gameState.teamA.score
    property int scorePlayersRight: gameState.teamB.score

    property string currentPlayerTurn: ""

    signal playerTurnChanged

    signal scoreTableModelChanged

    Connections {
        target: gameState
        onCurrentPlayerChanged: {
            var currentPlayer = gameState.currentPlayer;
            if (currentPlayer) {
                currentPlayerTurn = currentPlayer.name;
                playerTurnChanged();
            }
        }
    }

    states: [
        State {
            name: "NORMAL"
            PropertyChanges {target: scoreTable; y: 0; x: background.x}
        },
        State {
            name: "RETRACTED"
            PropertyChanges {target: scoreTable; y: 0; x: background.x}
            PropertyChanges {target: resizeButton; anchors.verticalCenterOffset: 13}
            //image divisor
            PropertyChanges {target: playerImageLeftDivider;visible: false}
            PropertyChanges {target: playerImageRightDivider; visible: false}
            //players avatars
            PropertyChanges {target: leftDownPlayer; visible: false}
            PropertyChanges {target: leftUpPlayer; visible: false}
            PropertyChanges {target: rightDownPlayer; visible: false}
            PropertyChanges {target: rightUpPlayer; visible: false}

            PropertyChanges {target: versus; anchors.bottomMargin: 0;}
            //team scores
            PropertyChanges {target: leftScoreShape; anchors.leftMargin: 30;
                anchors.verticalCenter: scoreTable.verticalCenter;anchors.verticalCenterOffset: -25;}
            PropertyChanges {target: rightScoreShape; anchors.rightMargin: 30;
                anchors.verticalCenter: scoreTable.verticalCenter; anchors.verticalCenterOffset: -25;}
            //player names
            PropertyChanges {target:playerLeftDownNameText; anchors.topMargin: 5; anchors.leftMargin: -20}
            PropertyChanges {target: playerRightDownNameText; anchors.topMargin: 5;anchors.rightMargin: -23}
            PropertyChanges {target:playerLeftUpNameText; anchors.leftMargin: -20}
            PropertyChanges {target:playerRightUpNameText; anchors.rightMargin: -20}
            //tiles
            PropertyChanges {target: playerRightDownTiles; anchors.leftMargin: 30; anchors.topMargin: -15}
            PropertyChanges {target:playerRightUpTiles; anchors.leftMargin: 30; anchors.bottomMargin: -20}
            PropertyChanges {target:playerLeftDownTiles; anchors.rightMargin: 30; anchors.topMargin: -15}
            PropertyChanges {target: playerLeftUpTiles; anchors.rightMargin: 30; anchors.bottomMargin: -20}
        }
    ]

    Image {
        id: background
        source: scoreTable.state == "NORMAL" ?
                    "qrc:/scoreTableImgs/score_estendido.png" :
        "qrc:/scoreTableImgs/score_minimizado_bg.png"
    }

    Image {
        id: playerImageLeftDivider
        source: "qrc:/scoreTableImgs/divisoria.png"
        fillMode: Image.PreserveAspectFit
        anchors {
            verticalCenter: scoreTable.verticalCenter
            verticalCenterOffset: -20
            left: scoreTable.left
            leftMargin: 80
        }
    }

    Image {
        id: versus
        source: "qrc:/scoreTableImgs/versus.png"
        fillMode: Image.PreserveAspectFit
        anchors {
            left: playerImageLeftDivider.right
            right: playerImageRightDivider.left
            bottom: resizeButton.top
            bottomMargin: 30
        }
    }

    Image {
        id: playerImageRightDivider
        fillMode: Image.PreserveAspectFit
        source: "qrc:/scoreTableImgs/divisoria.png"
        anchors {
            verticalCenter: scoreTable.verticalCenter
            verticalCenterOffset: -20
            right: scoreTable.right
            rightMargin: 80
        }
    }

    Player { id: leftUpPlayer; opacity: 1;  avatarImage: playerLeftUpAvatar; playerName:playerLeftUpName; playerScore:scorePlayersLeft ;anchors { left: scoreTable.left; leftMargin: 35 ;top: scoreTable.top; topMargin: 15}}
    Player { id: leftDownPlayer; opacity: 1; avatarImage: playerLeftDownAvatar; playerName: playerLeftDownName; playerScore:scorePlayersLeft; anchors { left: scoreTable.left; leftMargin: 35; bottom: scoreTable.bottom; bottomMargin: 55}}
    Player { id: rightUpPlayer; opacity: 1; avatarImage: playerRightUpAvatar;playerName: playerRightUpName; playerScore:scorePlayersRight; anchors { right: scoreTable.right; rightMargin: 35 ;top: scoreTable.top; topMargin: 15}}
    Player { id: rightDownPlayer; opacity: 1;avatarImage: playerRightDownAvatar;playerName: playerRightDownName; playerScore:scorePlayersRight; anchors { right: scoreTable.right; rightMargin: 35;bottom: scoreTable.bottom; bottomMargin: 55 }}


    FontLoader {
        id: textFont
        name: "Chalkduster"
        source: "qrc:/shared/Chalkduster.ttf"
    }


    Text {
        id: playerLeftUpNameText;
        text: leftUpPlayer.playerName; color: "white"
        font.pixelSize: 14; font.bold: true; font.family: textFont.name
        anchors { left: leftUpPlayer.right; bottom:playerImageLeftDivider.bottom; bottomMargin: 20; leftMargin: 10 }
    }
    Text {
        id: playerLeftDownNameText;
        text: leftDownPlayer.playerName; color: "white"
        font.pixelSize: 14; font.bold: true; font.family: textFont.name
        anchors { left:leftDownPlayer.right; top: playerImageLeftDivider.top; topMargin: 20; leftMargin: 10}
    }
    Text {
        id: playerRightUpNameText;
        text: rightUpPlayer.playerName; color: "white"
        font.pixelSize: 14; font.bold: true; font.family: textFont.name
        anchors { right: rightUpPlayer.left; bottom:playerImageRightDivider.bottom; bottomMargin: 20; rightMargin: 10 }
    }
    Text {
        id: playerRightDownNameText;
        text: rightDownPlayer.playerName; color: "white"
        font.pixelSize: 14; font.bold: true; font.family: textFont.name
        anchors {right: rightDownPlayer.left; top: playerImageRightDivider.top; topMargin: 20; rightMargin: 10}
    }

    Image {
        id: leftScoreShape
        source: "qrc:/scoreTableImgs/caixa_pontuacao.png"
        anchors {left: playerImageLeftDivider.right; verticalCenter: scoreTable.verticalCenter; verticalCenterOffset: -20}
        fillMode: Image.PreserveAspectFit
        Text {
            id: playersLeftScoreText
            property int scoreValue: scoreTable.scorePlayersLeft
            Behavior on scoreValue {
                NumberAnimation { duration: 1500 }
            }
            text: scoreValue; color: "#FFFF00"
            font.pixelSize: 18; font.bold: true
            anchors.centerIn: leftScoreShape
        }
    }
    Image {
        id: rightScoreShape
        source: "qrc:/scoreTableImgs/caixa_pontuacao.png"
        anchors {right: playerImageRightDivider.left; verticalCenter: scoreTable.verticalCenter; verticalCenterOffset: -20}
        fillMode: Image.PreserveAspectFit
        Text {
            id: playersRightScoreText
            property int scoreValue: scoreTable.scorePlayersRight
            Behavior on scoreValue {
                NumberAnimation { duration: 1500 }
            }
            text: scoreValue; color: "#FFFF00"
            font.pixelSize: 18; font.bold: true
            anchors.centerIn: rightScoreShape
        }
    }


    Rectangle {
        signal clicked
        id: resizeButton
        color: "transparent"
        width: 120
        height: 30
        anchors { verticalCenter: scoreTable.verticalCenter; verticalCenterOffset: 45;
            horizontalCenter: scoreTable.horizontalCenter; }

        Image{
            source: scoreTable.state == "NORMAL" ?
                        "qrc:/scoreTableImgs/icone-minimiza.png" :
            "qrc:/scoreTableImgs/icone-estende.png"
            anchors.centerIn: parent
        }

        MouseArea {
            id: resizeButtonMouseArea
            anchors.fill: parent
            onClicked: {
                scoreTableModelChanged()
                focusOnPlayer(currentPlayerTurn)
            }
        }

    }

    TilesRow {
        id: playerRightUpTiles
        tilesNumber: gameState.teamA.players1.numberOfTilesOnHand
        opacity: 1
        anchors.left: leftUpPlayer.right
        anchors.bottom: playerLeftUpNameText.top
        anchors.bottomMargin: 5
    }
    TilesRow {
        id: playerRightDownTiles
        tilesNumber: gameState.teamA.players2.numberOfTilesOnHand
        opacity: 1
        anchors.left: leftDownPlayer.right
        anchors.top: playerLeftDownNameText.bottom
        anchors.topMargin: 5
    }
    TilesRow {
        id: playerLeftUpTiles
        tilesNumber: gameState.teamB.players1.numberOfTilesOnHand
        opacity: 1
        anchors.right: rightUpPlayer.left
        anchors.bottom: playerRightUpNameText.top
        anchors.bottomMargin: 5
    }
    TilesRow {
        id: playerLeftDownTiles
        tilesNumber: gameState.teamB.players2.numberOfTilesOnHand
        opacity: 1
        anchors.right: rightDownPlayer.left
        anchors.top: playerRightDownNameText.bottom
        anchors.topMargin: 5
    }

    onPlayerTurnChanged: {
        if(currentPlayerTurn == playerLeftUpName ) {
            focusOnPlayer(leftUpPlayer);
        } else if (currentPlayerTurn == playerRightUpName) {
            focusOnPlayer(rightUpPlayer);
        } else if (currentPlayerTurn == playerRightDownName) {
            focusOnPlayer(rightDownPlayer);
        } else if (currentPlayerTurn == playerLeftDownName) {
            focusOnPlayer(leftDownPlayer);
        }
    }

    function getListOfPlayer() {
        var playersList = new Array()
        playersList.push(leftUpPlayer)
        playersList.push(rightUpPlayer)
        playersList.push(rightDownPlayer)
        playersList.push(leftDownPlayer)
        return playersList
    }

    function focusOnPlayer(player) {
        var list = new Array()
        list = getListOfPlayer()

        for(var i=0; i< list.length; i++) {
            if(list[i] != player) {
                list[i].opacity = 0.3
            } else {
                list[i].opacity = 1
            }

        }
    }

    Component.onCompleted: {
        scoreTable.state = "NORMAL"
    }
}
