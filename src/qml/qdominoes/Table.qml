import QtQuick 1.1
import "Table.js" as TableJS

Item {
    id: table

    property QtObject tableController

    signal acceptedTileOnTable(int sideA, int sideB, int corner);
    signal nonAcceptedTileOnTable(int sideA,int sideB);
    signal rotateTile(int angle)
    signal rotateBack

    function onTileMovedFromHand(x,y,w,h,sideA,sideB) {
        TableJS.onTileMovedFromHand(x,y,w,h,sideA,sideB);
    }

    function onTileReleasedFromHand(x,y,w,h,sideA,sideB) {
        TableJS.onTileReleased(x,y,w,h,sideA,sideB);
    }

    Connections {
        target: tableController
        onTableModelChanged: {
            //console.log('Table.qml - onTableModelChanged');
            TableJS.onTableModelChanged(table, tableController.tableModel);
        }
        onScoreAnimation: {
            console.log('Table.qml - onScoreAnimation: ' + points);
            TableJS.doScoreAnimation(sideA, sideB, points, corners)
        }
    }

    Image {
        id: tableBackground
        width: table.width; height: table.height
        source: "qrc:/Common/background.png"
    }

}
