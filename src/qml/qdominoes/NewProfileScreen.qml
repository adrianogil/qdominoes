import QtQuick 1.1

import "../../ViewsDirector/qml"
import "../../ViewsDirector/js/Director.js" as ViewsDirector


AbstractView {

    id: newProfileScreen

    signal nextButtonClicked
    signal pictureButtonClicked
    signal backButtonClicked

    property bool newProfile
    property string oldProfileName
    property url oldAvatarPath

    property url picturePath: "qrc:/Profiles/Pic.png"

    Rectangle{
        id: bg
        anchors.fill: newProfileScreen
        color: "black"
        opacity: 0.5
    }

    Image{
        id: board
        source: "qrc:/newprofile/caixa-popup_03.png"
        y: 100
        anchors.horizontalCenter: newProfileScreen.horizontalCenter
        Image{
            id: textInputContainer
            source: "qrc:/newprofile/caixa-de-texto.png"
            y: parent.height*0.23
            anchors.right: parent.right
            anchors.rightMargin: 20

            TextInput{
                id: textInput
                anchors.verticalCenter: textInputContainer.verticalCenter
                anchors.verticalCenterOffset: 5
                anchors.horizontalCenter: textInputContainer.horizontalCenter
                anchors.horizontalCenterOffset: 5
                width:parent.width*0.9
                height: parent.height*0.9
                font.pixelSize: height*0.5
                text: ""
                maximumLength: 5
            }
        }

        Image{
            id: relevo
            source: "qrc:/newprofile/avatar.png"
            anchors{
                left: parent.left
                bottom: textInputContainer.bottom
                leftMargin: 20
            }

            Image{
                id: playerPhoto
                source: photos.currentItem.source
                width: parent.width*0.8
                height: parent.height*0.8
                anchors.centerIn: parent
                z: parent.z -1
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    listViewContainer.y = newProfileScreen.height - listViewContainer.height
                }
            }
        }

        Button{
            id: okButton
            buttonImage: "qrc:/newprofile/confirmar-botao.png"
            pressedImage: "qrc:/newprofile/confirmar-botao-press.png"
            anchors{
                horizontalCenter: parent.horizontalCenter
                bottom: parent.bottom
                bottomMargin: 14
            }
            onClicked:  {
                if(textInput.text != "") {
                    if(newProfile) {
                        console.log("newProfile is true")
                        profileController.createProfile(textInput.text, playerPhoto.source)
                    } else {
                        console.log("newProfile is false")
                        profileController.editProfile(oldProfileName, oldAvatarPath, textInput.text, playerPhoto.source)
                    }
                    profileController.updateListProfiles()
                    ViewsDirector.backToDefaultView()
                }
            }
        }

    }

    Image{
        id: listViewContainer
        source: "qrc:/newprofile/pop-up-fotos.png"
        width: newProfileScreen.width
        y: newProfileScreen.height

        Behavior on y{
            NumberAnimation{duration: 200}
        }

        ListView{
            id: photos
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width
            height: parent.height*0.9
            clip: true
            delegate: Image{
                id: delegate
                source: picPath
                width: height
                height: photos.height
                //z: 3
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        photos.currentIndex = index
                    }
                }
                Rectangle{
                    anchors.fill: parent
                    color: "transparent"
                    border.width: 10
                    border.color: "lightblue"
                    visible: photos.currentIndex == index
                }
            }
            model: listModel
            orientation: ListView.Horizontal
            spacing: 30
        }

        Image{
            source: "qrc:/newprofile/fechar-fotos.png"
            anchors.top: listViewContainer.top
            anchors.right: listViewContainer.right
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    listViewContainer.y = newProfileScreen.height

                }
            }
        }
    }

        ListModel{
            id: listModel
            ListElement{ picPath: "qrc:/Meegons/meego_char01.png"}
            ListElement{ picPath: "qrc:/Meegons/meego_char02.png"}
            ListElement{ picPath: "qrc:/Meegons/meego_char03.png"}
            ListElement{ picPath: "qrc:/Meegons/meego_char04.png"}
            ListElement{ picPath: "qrc:/Meegons/meego_char05.png"}
            ListElement{ picPath: "qrc:/Meegons/meego_char06.png"}
            ListElement{ picPath: "qrc:/Meegons/meego_char07.png"}
            ListElement{ picPath: "qrc:/Meegons/meego_char08.png"}
            ListElement{ picPath: "qrc:/Meegons/meego_char09.png"}
            ListElement{ picPath: "qrc:/Meegons/meego_char10.png"}
        }

    Button{
        id: backbutton
        anchors{
            right: newProfileScreen.right
            rightMargin: 15
            topMargin: 15
            top: newProfileScreen.top
        }
        buttonImage: "qrc:/Voltar.png"
        pressedImage: "qrc:/Voltar.png"


        onClicked: {
            ViewsDirector.backToDefaultView()

            //            newProfileScreen.nextButtonClicked()
        }
    }

    Component.onCompleted: {
        var args = ViewsDirector.getViewArgs()
        newProfile = args["newProfile"]

        if(newProfile != true)
        {
            oldProfileName = args["oldProfileName"]
            oldAvatarPath = args["oldAvatarPath"]
            photos.currentItem.source = oldAvatarPath
            textInput.text = oldProfileName
        }
    }

}
