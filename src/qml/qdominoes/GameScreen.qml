// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../ViewsDirector/js/Director.js" as ViewsDirector
import "../../ViewsDirector/qml"

AbstractView {
    id: gameScreen

    QtObject {
        id: props

        property QtObject playerController: gameController.playerController
        property QtObject handController: playerController.handController
        property QtObject tableController: gameController.tableController
        property QtObject gameState: gameController.gameState? gameController.gameState : undefined
    }

    signal galoBPressed
    signal menuButtonPressed

    PinchArea {
        id: pinch

        anchors.fill: parent
        property int xpoint
        property int ypoint
        property int pinchscale
        //property int pinchrotation
        enabled: true
        pinch.target: table
        pinch.minimumScale: 0.5
        pinch.maximumScale: 1
        //pinch.minimumRotation: -150
        //pinch.maximumRotation: 150
        onPinchUpdated: {
            //xpoint = pinch.center.x;
            //ypoint = pinch.center.y;
            //pinchrotation = pinch.rotation;
            pinchscale = pinch.scale;
            //console.log(board.x)
        }
    }

    Flickable {
        id: tableArea
        contentHeight: table.height; contentWidth:table.width
        contentX: (table.width - tableArea.width)/2
        contentY: (table.height - tableArea.height)/2
        anchors.fill: gameScreen
        boundsBehavior: Flickable.StopAtBounds
        Table {
            id: table

            tableController: props.tableController

            width: gameScreen.width*1.3; height: gameScreen.width*1.3
            onAcceptedTileOnTable: hand.sendTileToTable(sideA,sideB,corner);
            onNonAcceptedTileOnTable: hand.sendbackTileToHand(sideA,sideB);
            onRotateTile: {
                hand.rotateTile(angle)
            }
            onRotateBack: {
                hand.rotateBack()
            }
        }
    }

    Timer {
        id: timer
        interval: 1000
        onTriggered: {
            if(scoreTable.state == "NORMAL") {
                scoreTable.state = "RETRACTED"
                dropDownAnimation.start()
            } else {
                scoreTable.state = "NORMAL"
                dropDownAnimation.start()
            }
        }
    }

    ScoreTable {
        id: scoreTable

        gameState: props.gameState

        anchors.horizontalCenter: parent.horizontalCenter

        onScoreTableModelChanged: {
            retractAnimation.start()
            timer.start()
        }

        ParallelAnimation{
            id: retractAnimation
            PropertyAnimation {
                target: scoreTable
                properties: "y"
                to: -scoreTable.height
                duration: 1000
                easing.type: Easing.OutQuad
            }
            PropertyAnimation {
                target:scoreTable
                properties: "opacity"
                to: 0
                duration: 1000
                easing.type: Easing.OutQuad
            }
        }

        ParallelAnimation {
            id: dropDownAnimation
            PropertyAnimation {
                target: scoreTable
                properties: "y"
                from: -scoreTable.height
                to: 0
                duration: 1000
                easing.type: Easing.OutQuad
            }
            PropertyAnimation {
                target:scoreTable
                properties: "opacity"
                to: 1
                duration: 1000
                easing.type: Easing.OutQuad
            }

        }
    }

    Hand {
        id: hand
        y: gameScreen.height - hand.height
        width: parent.width
        handController: props.handController
        interactionAllowed: props.playerController.allowMoveHandTiles
        anchors.horizontalCenter: gameScreen.horizontalCenter


        onMovedTileOnTable: {
            // Convert from Hand coordinate system to Table coordinate system
            var newX = tileX + tableArea.contentX;
            var newY = tileY + hand.y + tableArea.contentY;

            // Send signal to table update Highlights
            table.onTileMovedFromHand(newX,newY,w,h,sideA,sideB)

            // Hide Hand and Score table
            if(scoreTable.state == "NORMAL")
                scoreTable.scoreTableModelChanged();

            hand.state = 'hide';
        }
        onReleasedTileOnTable: {
            // Convert from Hand coordinate system to Table coordinate system
            var newX = tileX + tableArea.contentX;
            var newY = tileY + hand.y + tableArea.contentY;

            // Send signal to table update Highlights
            table.onTileReleasedFromHand(newX,newY,w,h,sideA,sideB);
        }
        onReleasedTile: {
            // Show Hand and Score table
            if(scoreTable.state == "RETRACTED")
                scoreTable.scoreTableModelChanged();

            hand.state = 'show';
            scoreTable.focusOnPlayer(scoreTable.currentPlayerTurn)
        }
        onRotateTile: {

        }
        onRotateBack: {

        }

        Button {
            anchors.right: parent.right
            anchors.rightMargin: 15
            anchors.top: parent.top
            anchors.topMargin: -5
            buttonImage: "qrc:/gameScreen/galo.png"
            pressedImage: "qrc:/gameScreen/galo-p.png"
            onClicked: {
                gameScreen.galoBPressed()
            }
        }

    }

    Button {
        id: closeButton
        anchors.right: parent.right
        anchors.rightMargin: 15
        anchors.top: parent.top
        anchors.topMargin: 15
        buttonImage: "qrc:/Fechar.png"
        pressedImage: "qrc:/Fechar.png"
        onClicked: {
            ViewsDirector.showPopup("closeAppPopUp")
        }
    }

    Button{
        id: menuButton
        anchors.left: parent.left
        anchors.leftMargin: 15
        anchors.top: parent.top
        anchors.topMargin: 15
        buttonImage: "qrc:/Menu.png"
        pressedImage: "qrc:/Menu.png"
        onClicked: {
            ViewsDirector.showPopup("moveBackMenuPopUp")
        }
    }

    Connections {
        target: gameController
        onPlayerPassed: {
            ViewsDirector.showPopupWithArgs("passedTurnPopUp",
                                            {"playerName":
                                                props.playerController.playerModel.name == playerName?
                                                    "Você" : playerName})
        }
        onDrawbackWarning: {
            ViewsDirector.showPopupWithArgs("results",
                                            {"resultText" :
                                                "Ocorreu um empate!"});
        }
        onTeamAWinsWarning: {
            ViewsDirector.showPopupWithArgs("results",
                                            {"player1Name" : props.gameState.teamA.players1.name,
                                             "player2Name" : props.gameState.teamA.players2.name});
        }
        onTeamBWinsWarning: {
            ViewsDirector.showPopupWithArgs("results",
                                            {"player1Name" : props.gameState.teamB.players1.name,
                                             "player2Name" : props.gameState.teamB.players2.name});
        }
    }
}
