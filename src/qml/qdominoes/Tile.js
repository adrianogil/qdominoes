/*
    This file is part of QDominoes.

    QDominoes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QDominoes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QDominoes  If not, see <http://www.gnu.org/licenses/>.

*/

// Including external javascript libraries
Qt.include("utils.js");

/*****************************************************/
// Defining Tile Component
var tileQMLFile = "Tile.qml";

var tileComponent = Qt.createComponent(tileQMLFile);

function createTileObj(parent, options, callback) {
    console.debug("Tile.js::createObj - parent: " + parent +
                  " options:" + options);
    createDynamicComponent(tileComponent, parent,
                           function(tileObj) {
                               configureObj(tileObj, options);
                               if (callback != undefined)
                                   callback(tileObj);
                           });
}
