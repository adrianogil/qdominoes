import QtQuick 1.1

Item {
    id: profile
    clip: true

    property bool current: false

    property QtObject modelItem

    property string propertyName
    property string propertyAvatarPath

    property string modelDataResources

    signal clicked

    Image{
        id: bg
        anchors.fill: profile
        source: "qrc:/Perfil/Add-perfil.png"
        visible: picture.progress == 0
    }

    Image{
        id: picture
        anchors{
            verticalCenter: profile.verticalCenter
            left: profile.left
            leftMargin: 5
        }
        source: propertyAvatarPath
        height: 70//parent.height * 0.9
        width: 75//parent.width * 0.3
    }

    Image{
        id: bgEmpty
        anchors.fill: profile
        source: "qrc:/Perfil/Add-perfil-empty.png"
        visible: picture.progress == 1
    }

    Text{
        id: profileName
        anchors{
            verticalCenter: profile.verticalCenter
            left: picture.right
            leftMargin: 5
        }
        text: propertyName
    }

    MouseArea{
        id: mouseArea
        anchors.fill: profile
        onPressed:{
            if (bg.visible)
                bg.source = "qrc:/Perfil/Add-perfil-press.png"
            else
                bgEmpty.source = "qrc:/Perfil/Add-perfil-empty-press.png"
        }

        onReleased: {
            if (bg.visible)
                bg.source = "qrc:/Perfil/Add-perfil.png"
            else bgEmpty.source = "qrc:/Perfil/Add-perfil-empty-press.png"
            profile.clicked()
        }
    }
}

