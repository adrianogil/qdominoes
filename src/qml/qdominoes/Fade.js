.pragma library

Qt.include("Dictionary.js")

function Fade(){

    var popup = new Dictionary();

    var fadeObject;
    var popupObject;
    var fadeComponent = Qt.createComponent("../qml/FadeElement.qml");
    var currentPopup;

    var color, opacity;

    this.setColor = function(c){
             color = c;
         }

    this.createFade = function(parentElement) {
             fadeObject = fadeComponent.createObject(parentElement,{});
             fadeObject.color = color;
         }

    this.createPopup = function(id,parentElement){
             popupObject = popup.get(id).createObject(parentElement);
             popupObject.show();
        }

    this.registerPopup = function(id, fadeComponent) {
             popup.add(id,fadeComponent);
         }

    this.show = function(){
             fadeObject.show();
         }

    this.hide = function() {
             fadeObject.hide();
             popupObject.hide();
         }

    this.getPopupObject = function(){
             return popupObject;
         }

}
