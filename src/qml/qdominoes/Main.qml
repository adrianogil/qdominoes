import QtQuick 1.1
import "ScreenManager.js" as Manager
import "../../ViewsDirector/js/Director.js" as ViewsDirector

Item {
    id: main
    width: screenWidth; height: screenHeight

    Loader {
        id: screens
        anchors.fill: parent
        opacity: 0

        function fadeIn() {
            fadeAnimation.start();
        }

        SequentialAnimation {
            id: fadeAnimation
            NumberAnimation { target: screens; property: "opacity"; to: 1; duration: 600 }
        }

        onLoaded: {
            if (screens.fadeIn)
                screens.fadeIn();
        }
    }

    Splash {
        id: splash
        width: main.width; height: main.height

        signal loadScreens;

        duration: 4000

        onLoadScreens: {
            screens.source = "Application.qml";
        }

        MouseArea {
            anchors.fill: parent
        }

    }

    Connections {
        target: mainController
        onGameStarted: {
            console.log("Main.qml - Lets start the game")
            gameController.startLoop();
            if(ViewsDirector.currentView() != "gameScreen") {
                ViewsDirector.nextView("gameScreen")
             }
            screens.width = main.width
            screens.height = main.height
        }
    }
}
