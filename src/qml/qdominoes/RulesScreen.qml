// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../ViewsDirector/qml"
import "../../ViewsDirector/js/Director.js" as ViewsDirector


AbstractView {

    id: rulesScreen
    signal backButtonClicked

    property variant fontFamily

    Image{
        id: label
        anchors{
            horizontalCenter: rulesScreen.horizontalCenter
            top: rulesScreen.top
            topMargin: 15
        }
        source: "qrc:/Rules/regras.png"
    }

    Item{
        id: rulesContainer
        width: image.width
        height: image.height
        clip: true
        anchors{
            horizontalCenter: rulesScreen.horizontalCenter
            top: label.bottom
            topMargin: 30
        }

        Image{
            id: image
            source: "qrc:/Rules/caixa.png"
        }

        Flickable{
            id: rules
            anchors{
                left: parent.left
                leftMargin: 35
                right: scroll.left
                rightMargin: 3
                top: parent.top
                topMargin: 25
                bottom: parent.bottom
                bottomMargin: 25
            }
            clip: true
            contentHeight: textRules.height
            contentWidth: textRules.width
            flickableDirection: Flickable.VerticalFlick

            onContentYChanged: {
                scrollBar.y = (scroll.height - scrollBar.height+25)*rules.contentY/textRules.height
            }

            Text{
                id: textRules
                width: parent.width
                wrapMode: Text.WordWrap
                font.family: fontFamily.name
                horizontalAlignment: Text.AlignJustify
               // font.pixelSize: 17
                font.pixelSize: rulesContainer.width*0.03
                text: "O dominó amazonense é uma variação das regras
clássicas do dominó. É um jogo para 4 jogadores,
divididos em 2 duplas. O objetivo é marcar um
número determinado de pontos antes da dupla
adversária. Comumente utiliza-se 200 pontos,
o qual foi adotado para o desenvolvimento do
QDominoes.

O dominó utilizado é chamado de duplo 6, pois
possui no total 28 pedras, cujos valores vão de
(0,0) a (6,6). Todos os jogadores começam com
sete pedras aleatória. As pedras distribuídas
caracterizam a \"mão\" do jogador. Os membros da
dupla se sentam de forma que um fique de frente
pro outro. Desta forma as jogadas são intercaladas
entre os membros de duplas diferentes.

A primeira rodada começa com a \"carroça\" de sena
(6,6). \"Carroça\" é o nome dado às pedras com
números iguais em ambas as pontas. O jogo segue
em sentido horário, sempre encaixando as pedras
com extremidades de mesmo valor. Essa mecânica
de jogo é idêntica à regra geral do dominó.

As pedras jogadas caracterizam a \"mesa\". A partir
da segunda rodada o jogador que \"bateu\", ou seja,
o primeiro jogador que ficou  sem pedras na mão,
deve iniciar a nova rodada com uma carroça, após
a contagem da garagem (veja abaixo) e as pedras
serem novamente embaralhadas e distribuídas. Desta
vez qualquer carroça pode ser jogada. Caso o jogador
não possua nenhuma carroça para iniciar o jogo, isso
é considerado uma situaçao de \"passe\" (que será
explicado a seguir), então o início do jogo é
delegado ao jogador à esquerda. Em caso de \"jogo
trancado\" (também explicado posteriormente) a
partida deve ser iniciada pelo jogador que possuir
a carroça de sena.

A pedra central, chamada comumente de \"bucho\", é
o coração da partida. É a única pedra na qual
pode-se encaixar 4 pedras. O bucho só pode ter
pedras jogadas nas suas pontas caso pelo menos uma
pedra tenha sido jogada em cada uma das suas laterais.

Pontos são marcados nas seguintes condições:

- Valor da soma das extremidades livres na mesa for
múltiplo de cinco: o time recebe uma pontuação
equivalente à soma das pontas livres na mesa de jogo.
Nas regras amazonenses essa pontuação deve ser
anunciada, caso contrário os pontos serão perdidos
assim que a dupla adversária jogar a próxima pedra.
No QDominoes optamos pela contagem automática das
pontas livres, sendo assim não há necessidade da
pontuação ser anunciada.

OBS: As carroças possuem um comportamento
diferenciado. Quando jogadas, elas são dispostas
em posição perpendicular à das pedras adjacentes.
Desta forma quando uma carroça é uma das pedras
em uma das extremidades da mesa, ambas as suas
pontas são utilizadas na contagem. Por exemplo,
caso a primeira pedra seja uma carroça de sena (6,6)
e o segundo jogador jogar um sena e terno (6,3), esse
jogador deve anunciar que fará 15 pontos, caso queira
que seus pontos sejam validados. Nestas situações, o
QDominoes também realiza a contagem de pontos
automaticamente, não havendo a necessidade do
jogador anunciar a marcação de pontos.

- Quando um jogador adversário não possuir uma pedra
compatível com as extremidades livres, ocorre o que é
chamado  de \"passe\". Na regra amazonense toda vez
que você faz com que um dos seus adversários passe a
vez, seu time ganha 20 pontos. Porém quando um
jogador passa a vez e o próximo a jogar também o faz,
este último não gera 20 pontos para a dupla adversária.
Isto é chamado \"passe nas costas\".

- O \"Galo\". Um galo significa que logo depois da sua
jogada, nenhum outro jogador na mesa terá condições
de jogar, além de você. Você faz com que seu parceiro
e a dupla adversária fiquem uma rodada sem jogar.
Esta jogada vale 50 pontos. O jogador deve anunciar
esta jogada antes de fazê-la. Caso a jogada seja
executada corretamente, com todos os outros jogadores
passando, sua dupla ganhará os pontos. Caso você
anuncie a jogada e ela não seja executada corretamente,
ou seja, caso haja a possibilidade de alguém que não
seja quem anunciou o galo jogar, a dupla adversária
ganha os pontos, lance chamado tradicionalmente de
\"galo gay\". Caso um dos jogadores execute esta jogada
mas não a anunciepreviamente, ele ganhará somente 20
pontos referentes ao passe do próximo jogador.

- Fim da rodada. Assim que um dos jogadores terminar
de jogar todas as pedras da mão, evento chamado de
\"bater\", a rodada acaba. Com o fim da rodada os
membros da dupla adversária do jogador que bateu o
jogo devem exibir as pedras restantes que possuem.
Os valores das pontas destas pedras são somados e o
time vencedor ganha um número de pontos equivalente
ao múltiplo de 5 mais próximo da soma das pedras,
sempre arredondando para baixo. Por exemplo, a dupla
perdedora tem como somatório do número de pedras o
número 19. O múltiplo de 5 mais próximo arredondado
pra baixo, no caso, é 15. Logo, essa é a pontuação
que a dupla vencedora deve ganhar. A pontuação
adquirida desta forma é chamada de \"garagem\".

OBS: caso o jogador que bater o fizer jogando como
última pedra uma carroça, sua dupla receberá 20
pontos. Este lance é chamado de \"batida de carroça\".

- \"Jogo trancado\" se caracteriza por nenhum dos
jogadores possuir qualquer pedra que possa ser
jogada na mesa. Diferentemente do fim da rodada,
no jogo trancado nenhum dos jogadores baixou todas
as suas pedras. Sendo assim as duplas devem
contabilizar suas respectivas garagens, e a dupla
que possuir a menor garagem ganha a pontuação
equivalente à garagem da outra dupla. Esta pontuação
é chamada de \"garagem do jogo trancado\". Caso o valor
da soma de ambas as duplas seja o mesmo, nenhuma
delas ganhará pontos.

Apesar do jogo ter uma pontuação determinada para o
fim, muitas vezes essa pontuação é alcançada durante
a execução de uma rodada. Neste caso o jogo deve
continuar até o final da rodada, e quem possuir mais
pontos ao final ganha o jogo."

            }
        }

        Image{
            id: scroll
            anchors{
                right: parent.right
                rightMargin: 20
                verticalCenter: parent.verticalCenter
            }
            source: "qrc:/Rules/scroll.png"
            Image{
                id: scrollBar
                source: "qrc:/Rules/scroll-1.png"
                anchors.horizontalCenter: parent.horizontalCenter
                MouseArea{
                    id: scrollMouse
                    width: scrollBar.width
                    height: scrollBar.height
                    anchors.fill: parent
                    drag.target: scrollBar
                    drag.axis: Drag.YAxis
                    y: 0

                    property real maxY: scroll.height - scrollBar.height
                    property bool clicked: false

                    onPressed: {
                        clicked = true
                    }
                    onReleased: {
                        clicked = false
                    }

                    onMouseYChanged: {
                        if(scrollBar.y <0)
                            scrollBar.y = 0
                        else if(scrollBar.y > maxY)
                            scrollBar.y = maxY
                        if(clicked){
                            rules.contentY = (textRules.height-scroll.height)*scrollBar.y/maxY
                        }
                    }
                }
            }

        }
    }

    Button{
        id: backButton
        anchors{
            top: rulesScreen.top
            topMargin: 15
            rightMargin: 15
            right: rulesScreen.right
        }
        buttonImage: "qrc:/Voltar.png"
        pressedImage: "qrc:/Voltar.png"
        onClicked: {
            rulesScreen.backButtonClicked()
            if(ViewsDirector.currentView() != "menu")
                ViewsDirector.nextView("menu")
        }
    }
}
