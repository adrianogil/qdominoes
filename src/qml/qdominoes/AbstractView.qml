import QtQuick 1.1
import "../js/ViewsDirector.js" as ViewsDirector;

Item {
    id: view

    width: parent.width
    height: parent.height
    visible: false;
    property bool destroyable: true

    signal nextView(string name);
    signal back;
    signal hide;
    signal show;
    signal showDefault;
    signal enteringFinished;
    signal leavingFinished;

    function currentView(){
        return ViewDirector.currentView();
    }

    states: [
        State {
            name: "INVISIBLE"
            PropertyChanges {
                target: view;
                visible: false;
            }
        },
        State {
            name: "VISIBLE"
            PropertyChanges {
                target: view;
                visible: true;
            }
        }
    ]

    transform: Rotation {
        id: rot
        origin.x: view.width
        origin.y: view.height/2
        origin.z: 0
        axis.x: 0; axis.y: 1; axis.z: 0;
        angle: 0;
    }

    onNextView: {
        ViewsDirector.nextView(name);
    }

    onHide: {
        var cover = coverComponent.createObject(view,{});
        leaving.start();
    }

    onShow: {
        var cover = coverComponent.createObject(view,{});
        view.enteringFinished.connect(function(){
                                          cover.destroy()
                                      });
        view.state = "VISIBLE";
        entering.start();
    }

    onShowDefault: {
        view.x = 0;
        view.state = "VISIBLE";
    }

    SequentialAnimation{
        id: leaving
        NumberAnimation {
            target: rot
            duration: 500
            property: "angle"
            to: -90
            easing.type: Easing.OutExpo
        }
        onCompleted: {
            view.state = "INVISIBLE";
        }
    }

    SequentialAnimation{
        id: entering
        NumberAnimation {
            target: view
            duration: 550
            property: "x"
            to: 0
            easing.type: Easing.OutExpo
        }
        onCompleted: {
            view.enteringFinished();
        }
    }

    Component {
        id: coverComponent
        Item {
            id: myCover
            function destroy(){
                myCover.visible = false;
                myCover.destroy(100);
            }

            anchors.fill: parent
            MouseArea {
                anchors.fill: parent
                onClicked: {}
            }
        }
    }

}
