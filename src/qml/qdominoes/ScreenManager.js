var component
var counter = 0
var stack = new Array()
var profileScreenValue = null
var propertyName = null

function addScreen(screen) {
    component = Qt.createComponent(screen);
    profileScreenValue = true
     if (component.status == Component.Ready)
         finishCreation();
     else
         component.statusChanged.connect(finishCreation);
}


function addScreenWithProperty(screen,propertyValue, profileScreenFlag) {
     component = Qt.createComponent(screen);
     if(profileScreenFlag != null) {
         profileScreenValue = profileScreenFlag
         propertyName = propertyValue
     }
     if (component.status == Component.Ready)
         finishCreation();
     else
         component.statusChanged.connect(finishCreation);
}

function finishCreation() {
    if (component.status == Component.Ready) {
        if (profileScreenValue == false) {
            stack[counter] = component.createObject(main, {"width": main.width, "height": main.height, "z":counter, "newProfile": profileScreenValue
                                                        ,"oldProfileName": propertyName});
        } else {
            stack[counter] = component.createObject(main, {"width": main.width, "height": main.height, "z":counter});
        }

        if (stack[counter] == null) {
            // Error Handling
            console.log("Error creating object\n"+item);
        }
        counter++
        console.log("Screen created: "+counter)
    } else if (component.status == Component.Error) {
        // Error Handling
        console.log("Error loading component:", component.errorString());
    }
}

function deleteCurrentScreen(){
    stack[counter-1].destroy()
    counter--
    console.log("Screen deleted")
}

function deletePreviousScreen(){
    if(counter > 1){
        stack[counter-2].destroy()
        stack[counter-2] = stack[counter]
        counter--
        console.log("Screen deleted")
    }
}
