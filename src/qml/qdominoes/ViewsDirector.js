.pragma library

Qt.include("Dictionary.js");
Qt.include("Fade.js");
Qt.include("Menu.js")

// ###############################
// ### NORMAL VIEWS MANAGEMENT ###
// ###############################

var front;
var back;

var instances = new Dictionary();
var views = new Dictionary();
var isStart = true;

var container;

// map of properties that can be accessed by the view or popup
var viewArgs;
var popupArgs;

function setContainer(obj) {
    container = obj;
}

function start(){
    push("default");
}

function nextViewWithArgs(name,args){
    viewArgs = args;
    push(name);
}

function nextView(name){
    viewArgs = null;
    push(name);
}

function getViewArgs(){
    return viewArgs;
}

function setDefaultView(view){
    views.add("default",view);
}

function registerView(name,component){
    views.add(name,component);
}

function createView(name){
    if(instances.contains(name) === false){
        var view = views.get(name);
        var obj = view.createObject(container)
        instances.add(name,obj);
    }
    return instances.get(name);
}

function currentView(){
    return front;
}

function destroyView(name){
    if(instances.contains(name) === true){
        var obj = instances.get(name);
        obj.destroy(1000);
        instances.remove(name);
    }
}

function backToDefaultView(){
    push("default");
}

function push(name){
    if(name != front){
        back = front;
        front = name;
        if (instances.contains(front) !== true){
            createView(front);
            instances.get(front).x = -instances.get(front).width
        }

        if (name == "default" && isStart == true){
            instances.get(name).showDefault();
            isStart = false;
        }else{
            instances.get(front).z = instances.get(back).z + 1;
            instances.get(back).hide();
            instances.get(front).show();
            destroyView(back);
        }
    }
}


// ###############################
// #### FADE VIEWS MANAGEMENT ####
// ###############################

Qt.include("Fade.js");

var fade = new Fade();

function setFadeColorAndOpacity(color){
    fade.setColor(color);
}

function registerPopup(id,component){
    fade.registerPopup(id,component);
}

function createPopup(popupId){
    var parent = instances.get(front);
    fade.createFade(parent);
    var obj = fade.createPopup(popupId, parent);
    fade.show();
    return obj;
}

function showPopupWithArgs(popupId,args){
    popupArgs = args;
    createPopup(popupId);
}

function showPopup(popupId){
    popupArgs = null;
    createPopup(popupId);
}

function getPopupArgs(){
    return popupArgs;
}

function closePopup() {
    fade.hide();
}

function getPopupObject() {
    return fade.getPopupObject();
}

// ###############################
// ####### MENU MANAGEMENT #######
// ###############################

var menu = new Menu();

function setMenuComponent(component){
    menu.setContainer(container);
    menu.setMenu(component);
    menu.create();
}

function openMenu(){
    menu.open(instances.get(front).z);
}

function closeMenu(){
    menu.close();
}
