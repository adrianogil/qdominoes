import QtQuick 1.1
import "../../ViewsDirector/js/Director.js" as ViewsDirector
import "../../ViewsDirector/qml"

AbstractView {
    id: titleScreen

    signal playBClicked
    signal multiPlayBClicked
    signal contactBClicked
    signal aboutBClicked
    signal rulesBClicked
    signal exitBClicked
    signal backBClicked

    Image{
        id: bg
        anchors.fill: titleScreen
        source: "qrc:/Menu/Background.png"
    }

    Image{
        id: menuLogo
        source: "qrc:/Menu/Menu.png"
        anchors.horizontalCenter: titleScreen.horizontalCenter
        anchors.top: titleScreen.top
        anchors.topMargin: 20

    }



    Grid{
        columns: 2
        spacing: 30
        anchors{
            centerIn: parent
        }
        Button{
            id: playButton
            buttonImage: "qrc:/Menu/Single.png"
            pressedImage: "qrc:/Menu/SingleP.png"
            onClicked: {
                if(ViewsDirector.currentView() != "levelChoice"){
                    ViewsDirector.nextView("levelChoice");
                    titleScreen.playBClicked()
                }
            }
        }

        Button{
            id: contactButton
            buttonImage: "qrc:/Menu/Fale.png"
            pressedImage: "qrc:/Menu/FaleP.png"
            onClicked: {
                titleScreen.contactBClicked()
                if(ViewsDirector.currentView() != "contacts"){
                    ViewsDirector.nextView("contacts");
                }
            }
        }
/*
        Button{
            id: multiPlayButton
            buttonImage: "qrc:/Menu/Multi.png"
            pressedImage: "qrc:/Menu/MultiP.png"
            onClicked: {
                titleScreen.multiPlayBClicked()
            }
        }*/
        Button{           
           id: aboutButton
            buttonImage: "qrc:/Menu/Sobre.png"
            pressedImage: "qrc:/Menu/SobreP.png"
            onClicked: {
                titleScreen.aboutBClicked()
                if(ViewsDirector.currentView() != "about"){
                    ViewsDirector.nextView("about");
                }
            }
        }
        Button{
            id: rulesButton
            buttonImage: "qrc:/Menu/Regras.png"
            pressedImage: "qrc:/Menu/RegrasP.png"
            onClicked: {
                titleScreen.rulesBClicked()
                if(ViewsDirector.currentView() != "rules"){
                    ViewsDirector.nextView("rules");
                }
            }
        }/*
        Button{
            id: exitButton
            buttonImage: "qrc:/Menu/Sair.png"
            pressedImage: "qrc:/Menu/SairP.png"
            onClicked: {
                titleScreen.exitBClicked()
                Qt.quit()
            }
        }*/
    }

    Button {
        id: backButton
        buttonImage: "qrc:/Menu/Voltar.png"
        pressedImage: "qrc:/Menu/Voltar.png"
        anchors.right: titleScreen.right
        anchors.rightMargin: 10
        anchors.topMargin: 10
        anchors.top: titleScreen.top
        onClicked: {
            titleScreen.backBClicked()
            if(ViewsDirector.currentView() != "default")
                ViewsDirector.backToDefaultView()
        }
    }
}
