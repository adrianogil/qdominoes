// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../ViewsDirector/qml"
import "../../ViewsDirector/js/Director.js" as ViewsDirector

AbstractView {

    id: contactScreen
    property variant fontFamily

    Image {
        id:contactTag
        source: "qrc:/Contacts/fale-conosco.png"
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: contactScreen.top
            topMargin: 15
        }
    }

    Button {
        id: backButton
        buttonImage: "qrc:/Voltar.png"
        pressedImage: "qrc:/Voltar.png"

        anchors{
            top: contactScreen.top
            topMargin: 15
            rightMargin: 15
            right: contactScreen.right
        }
        onClicked: {
            if(ViewsDirector.currentView() != "menu")
                ViewsDirector.nextView("menu")
        }
    }

//    ScrollContainer {
//        id: scrollContacts
//        containerText: contactController.getContactsText()
//        tagScreenImagePath: "qrc:/Contacts/fale-conosco.png"
//        anchors.centerIn: parent
//        Button {
//            id: facebookButton
//            buttonImage: "qrc:/Contacts/facebook.png"
//            pressedImage: "qrc:/Contacts/facebook_p.png"
//            anchors.top: parent.bottom
//            anchors.topMargin: 10
//            anchors.left: parent.left
//            anchors.leftMargin: 20
//            onClicked: contactController.openFacebook()
//        }
//        Button {
//            id: twitterButton
//            buttonImage: "qrc:/Contacts/twitter.png"
//            pressedImage: "qrc:/Contacts/twitter_p.png"
//            anchors.top: parent.bottom
//            anchors.topMargin: 10
//            anchors.left: facebookButton.right
//            anchors.leftMargin: 10
//            onClicked:contactController.openTwitter()
//        }
//    }

    Item {
        id: contactContainer
        width: containerImage.width
        height: containerImage.height
        clip: true
        anchors{
            horizontalCenter: contactScreen.horizontalCenter
            top: contactTag.bottom
            topMargin: 30
        }

        Image{
            id: containerImage
            source: "qrc:/Contacts/quadro.png"
        }

        Flickable {
            id: contactText
            anchors{
                left: parent.left
                leftMargin: 35
                right: scroll.left
                rightMargin: 3
                top: containerImage.top
                topMargin: 50
                bottom: parent.bottom
                bottomMargin: 90
            }
            clip: true
            contentHeight: textContainer.height
            contentWidth: textContainer.width
            flickableDirection: Flickable.VerticalFlick

            onContentYChanged: {
                scrollBar.y = (scroll.height)*contactText.contentY/textContainer.height

                if(scrollBar.y <0) {
                    scrollBar.y = 0
                  } else if(scrollBar.y > scrollMouse.maxY) {
                    scrollBar.y = scrollMouse.maxY
                }
            }

            Text{
                id: textContainer
                width: contactText.width - scroll.width
                wrapMode: Text.WordWrap
                font.family: fontFamily.name
                font.pixelSize: 17
                text: contactController.getContactsText()
            }
        }

        Image{
            id: scroll
            anchors{
                right: parent.right
                rightMargin: 15
                verticalCenter: parent.verticalCenter
                verticalCenterOffset: -40

            }
            source: "qrc:/Contacts/barra.png"

            Image{
                id: scrollBar
                source: "qrc:/Contacts/barra2.png"
                anchors.horizontalCenter: parent.horizontalCenter
                MouseArea{
                    id: scrollMouse
                    width: scrollBar.width
                    height: scrollBar.height
                    anchors.fill: parent
                    drag.target: scrollBar
                    drag.axis: Drag.YAxis
                    y: 0

                    property real maxY: scroll.height - scrollBar.height
                    property bool clicked: false

                    onPressed: {
                        clicked = true
                    }
                    onReleased: {
                        clicked = false
                    }

                    onMouseYChanged: {
                        if(scrollBar.y <0)
                            scrollBar.y = 0
                        else if(scrollBar.y > maxY)
                            scrollBar.y = maxY
                        if(clicked){
                            contactText.contentY = (scroll.height - scrollBar.height)* scrollBar.y/maxY
                        }
                    }
                }
            }
        }

        Button {
            id: facebookButton
            buttonImage: "qrc:/Contacts/facebook.png"
            pressedImage: "qrc:/Contacts/facebook_p.png"
            anchors.top: contactText.bottom
            anchors.topMargin: 10
            anchors.left: parent.left
            anchors.leftMargin: 20
            onClicked: contactController.openFacebook()
        }
        Button {
            id: twitterButton
            buttonImage: "qrc:/Contacts/twitter.png"
            pressedImage: "qrc:/Contacts/twitter_p.png"
            anchors.top: contactText.bottom
            anchors.topMargin: 10
            anchors.left: facebookButton.right
            anchors.leftMargin: 10
            onClicked:contactController.openTwitter()
        }
    }
}
