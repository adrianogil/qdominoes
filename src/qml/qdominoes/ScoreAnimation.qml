// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item {
    property int score

    function scored(points) {
        console.debug('ScoreAnimation.qml - scored ' + points)
        score = points
        scoreAnimation.start();
    }

    width: 20
    height: scoreText.height

    Text {
        id: scoreText
        property int pixelSize: 18
        opacity: 0
        font.pixelSize: pixelSize

        font.weight: Font.Bold
        text: "+" + score
    }

    SequentialAnimation {
        id: scoreAnimation
        running: false
        ParallelAnimation {
            NumberAnimation {
                target: scoreText;
                property: "opacity";
                from:0; to: 1; duration: 1500;
                easing.type: Easing.InQuad
            }
            NumberAnimation {
                target: scoreText;
                property: "name";
                from: 18; to: 30; duration: 1500;
                easing.type: Easing.InQuad
            }
        }
        NumberAnimation {
            target: scoreText;
            property: "opacity";
            from:1; to: 0;
            duration: 1500;
            easing.type: Easing.InQuad
        }
    }


}
