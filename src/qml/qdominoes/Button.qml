import QtQuick 1.0
import QtQuick 1.1

Item {

    property int buttonWidth: 160
    property int buttonHeight: 40

    id: button

    width: buttonImg.progress? buttonImg.width : buttonWidth
    height: buttonImg.progress? buttonImg.height : buttonHeight

    property bool textVisible: false
    property bool enabled: true

    //fontLoader
    property url fontPath: ""
    property string fontName: ""

    //text properties (TextButton)(ImageTextButton)
    property color textColor: "white"
    property color textColorPressed: "#00adef"
    property int textPointSize: 18
    property string text: "Teste"

    property color disabledColor: "#1C1C1C"
    property color pressedColor: "#3e4345"
    property color defaultColor: "#00adef"
    property int buttonRadius: 0

    //button images url
    property url buttonImage: ""
    property url pressedImage: ""
    property url disabledImage: ""

    signal clicked
    signal released

    //Image shape for button
    Rectangle {
        id: buttonShape
        anchors.fill: button
        color: buttonImg.progress? "transparent" : (button.enabled ? defaultColor : disabledColor)
        radius: buttonRadius
        border.width: 0
        property bool shapePressed: false

        onShapePressedChanged: {
            if(shapePressed && !buttonImg.progress) {
                buttonShape.color = pressedColor
            } else if (!buttonImg.progress){
                buttonShape.color = defaultColor
            }
        }
    }

    //Image Button
    Image {
        id: buttonImg
        //anchors.fill: button
        property bool pressed: false
        source: enabled ? (pressed && button.pressedImage != "" ? button.pressedImage : button.buttonImage) : button.disabledImage
    }

    FontLoader {
        id: fontLoader
        name: button.fontName
        source: button.fontPath
    }

    Text {
        id: buttonText
        visible: button.textVisible
        color: buttonImg.pressed || buttonShape.shapePressed ? button.textColorPressed : button.textColor
        text: button.text; anchors.centerIn: button
        font { family: fontLoader.name;  pointSize: button.textPointSize; weight: Font.DemiBold }
        horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignVCenter;
    }

    states: [
        State {
            name: "up"
            //when: !button.pressed
            PropertyChanges {
                target: buttonShape
                shapePressed: false
            }
            PropertyChanges {
                target: buttonImg
                pressed: false

            }
        },
        State {
            name: "down"
            //when: button.pressed
            PropertyChanges {
                target: buttonShape
                shapePressed: true
            }
            PropertyChanges {
                target: buttonImg
                pressed: true

            }

        }
    ]

    MouseArea {
        id: mouseArea

        enabled: button.enabled

        anchors.fill: parent
        onPressed: {
            if (buttonImg.progress == 0) {
               button.state = "down"
            } else {
                button.state = "down"
            }

        }
        onReleased: {
            if (buttonImg.progress == 0) {
               button.state = "up"

            } else {
                button.state = "up"
            }
            button.released()
        }
        onClicked: button.clicked()

    }


}

