// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../ViewsDirector/qml"
import "../../ViewsDirector/js/Director.js" as ViewsDirector


AbstractView {

    id: aboutScreen
    property variant fontFamily

    Image {
        id:aboutTag
        source: "qrc:/Sobre/sobre.png"
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: aboutScreen.top
            topMargin: 15
        }
    }

    Button {
        id: backButton
        buttonImage: "qrc:/Voltar.png"
        pressedImage: "qrc:/Voltar.png"

        anchors{
            top: aboutScreen.top
            topMargin: 15
            rightMargin: 15
            right: aboutScreen.right
        }

        onClicked: {
            if(ViewsDirector.currentView() != "menu")
                ViewsDirector.nextView("menu")
        }
    }

    Item {
        id: aboutContainer
        width: containerImage.width
        height: containerImage.height
        clip: true
        anchors{
            horizontalCenter: aboutScreen.horizontalCenter
            top: aboutTag.bottom
            topMargin: 30
        }

        Image{
            id: containerImage
            source: "qrc:/Sobre/quadro.png"
        }

        Image {
            id: tileTag
            source: "qrc:/Sobre/marca.png"
            anchors.top: containerImage.top
            anchors.topMargin: 20
            anchors.horizontalCenter: containerImage.horizontalCenter

        }
        Flickable {
            id: aboutText
            anchors{
                left: parent.left
                leftMargin: 35
                right: scroll.left
                rightMargin: 3
                top: tileTag.bottom
                topMargin: 5
                bottom: parent.bottom
                bottomMargin: 90
            }
            clip: true
            contentHeight: textContainer.height
            contentWidth: textContainer.width
            flickableDirection: Flickable.VerticalFlick

            onContentYChanged: {
                scrollBar.y = (scroll.height)*aboutText.contentY/textContainer.height

                if(scrollBar.y <0) {
                    scrollBar.y = 0
                  } else if(scrollBar.y > scrollMouse.maxY) {
                    scrollBar.y = scrollMouse.maxY
                }
            }

            Text {
                id: versionText
                text: "Versão 1.0 - 15/08/2012"
                font.family: fontFamily.name
                font.pixelSize: 12
                anchors {
                    top: parent.top
                    horizontalCenter: parent.horizontalCenter
                }
            }

            Text{
                id: textContainer
                width: aboutText.width - scroll.width
                anchors.top: versionText.bottom
                anchors.topMargin: 10
                wrapMode: Text.WordWrap
                font.family: fontFamily.name
                font.pixelSize: 17
                text: aboutController.getAboutMessage()
            }
        }

        Image{
            id: scroll
            anchors{
                right: parent.right
                rightMargin: 15
                verticalCenter: parent.verticalCenter
                verticalCenterOffset: -40

            }
            source: "qrc:/Sobre/barra.png"

            Image{
                id: scrollBar
                source: "qrc:/Sobre/barra2.png"
                anchors.horizontalCenter: parent.horizontalCenter
                MouseArea{
                    id: scrollMouse
                    width: scrollBar.width
                    height: scrollBar.height
                    anchors.fill: parent
                    drag.target: scrollBar
                    drag.axis: Drag.YAxis
                    y: 0

                    property real maxY: scroll.height - scrollBar.height
                    property bool clicked: false

                    onPressed: {
                        clicked = true
                    }
                    onReleased: {
                        clicked = false
                    }

                    onMouseYChanged: {
                        if(scrollBar.y <0)
                            scrollBar.y = 0
                        else if(scrollBar.y > maxY)
                            scrollBar.y = maxY
                        if(clicked){
                            aboutText.contentY = (scroll.height - scrollBar.height)* scrollBar.y/maxY
                        }
                    }
                }
            }
        }

        Button {
            id: updateButton
            buttonImage: "qrc:/Sobre/atualizar.png"
            pressedImage: "qrc:/Sobre/atualizar_p.png"
            anchors.top: aboutText.bottom
            anchors.topMargin: 10
            anchors.left: parent.left
            anchors.leftMargin: 20
            onClicked: aboutController.updateSoftware()
            visible: aboutController.showUpdateButton()
        }
        Button {
            id: downloadButton
            buttonImage: "qrc:/Sobre/baixar.png"
            pressedImage: "qrc:/Sobre/baixar_p.png"
            anchors.top: aboutText.bottom
            anchors.topMargin: 10
            anchors.left: updateButton.right
            anchors.leftMargin: 10
            onClicked: aboutController.downloadApps()
        }
    }



}
