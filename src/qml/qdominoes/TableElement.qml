import QtQuick 1.0

import "TableElement.js" as TableElementJS

Item {
    id: tableElement

    property QtObject tableModel

    function createTableElementOnCorner(tableModel, corner) {

    }

    onTableModelChanged: TableElementJS.onTableElementModelChanged(tableElement);
}
