// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "Hand.js" as HandJS

Item {
    id: hand

    property QtObject handController
    property bool interactionAllowed

    width: 305; height: 81

    state: 'show'

    signal movingTile
    signal releasedTile
    signal releasedTileOnTable(int tileX, int tileY, int w, int h, int sideA, int sideB)
    signal movedTileOnTable(int tileX, int tileY, int w, int h, int sideA, int sideB)
    signal rotateTile(int angle)
    signal rotateBack

    QtObject {
        id: animationProps
        property int showY: 0
        property int hideY: 0
        property bool initialized: false
    }
    Component.onCompleted: {
        animationProps.showY = handBackground.y;
        animationProps.hideY = handBackground.y + hand.height;
        animationProps.initialized = true;
    }

    onStateChanged: {
        console.log('Hand.qml - onStateChanged1: ' + state +
                    ' - showY: ' + animationProps.showY +
                    ' - hideY: ' + animationProps.hideY +
                    ' - hand.y: ' + hand.y);
        if (animationProps.initialized) {
            console.log('Hand.qml - onStateChanged2: ' + state +
                        ' - showY: ' + animationProps.showY +
                        ' - hideY: ' + animationProps.hideY +
                        ' - hand.y: ' + hand.y);
            if (state == 'hide') {
                handBackground.y = animationProps.hideY;
            } else if (state == 'show') {
                handBackground.y = animationProps.showY;
            }
        }
    }


    function sendTileToTable(sideA,sideB,corner) {
        HandJS.sendTileToTable(sideA,sideB,corner);
    }
    function sendbackTileToHand(sideA,sideB) {
        HandJS.sendbackTileToHand(sideA,sideB);
    }

    Image {
        id: handBackground

        Behavior on y {
            NumberAnimation { duration: 500 }
        }

        width: hand.width; height: hand.height*1.5
        anchors.bottom: parent.bottom
        source: "qrc:/gamescreen/barrademadeira.png"
    }

    ListView{
        id: modelContainer
        anchors.verticalCenter: handBackground.verticalCenter
        anchors.horizontalCenter: handBackground.horizontalCenter
        width: 270
        height: handBackground.height
        orientation: ListView.Horizontal
        spacing: 10; interactive: false
        delegate: Tile {
            id: tile

            tileModel: model.modelData
            enableMouseArea: false
            state: "hidden"

            Component.onCompleted: {
                HandJS.invokeTileElement(hand,
                                         {
                                             "x" : index*(tile.width + modelContainer.spacing),
                                             "y" : 0,
                                             "tileModel" : tile.tileModel,
                                             "allowMove" : hand.interactionAllowed
                                         }, index);
            }
        }

        model: handController.tilesModel
    }

    Connections {
        target: handController
        onRemovedTile: {
            HandJS.onTileRemoved(tileModel);
        }
    }
}
