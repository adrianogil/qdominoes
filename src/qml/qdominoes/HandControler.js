var tiles = new Array()
var corners = new Array()
var highlights = new Array()
var count = 0
var playCount = 0
var hlCount = 0
var component = Qt.createComponent("Tile.qml");
var hl = Qt.createComponent("Highlight.qml");

function createHighlights(f,s){
    for(var i=0;i<playCount;i++){
        if(f == corners[i].secondValue || s == corners[i].secondValue){
            createTile2(corners[i].x+corners[i].width, corners[i].y)
        }
    }

}

function deleteHighlights(){
    for(var i=0;i<hlCount;i++){
        highlights[i].destroy()
    }
    hlCount = 0
}

function updateCorners(tile){
    if(playCount > 0){
        for(var i=0;i<playCount;i++){
            if(tile.firstValue == corners[i].secondValue){
                corners[i] = tile
            }else if(tile.secondValue == corners[i].secondValue){
                var temp
                temp = tile.firstValue
                tile.firstValue = tile.secondValue
                tile.secondValue = temp
                corners[i] = tile
            }
        }
    }else{
        for(i=0;i<4;i++){
            corners[playCount] = tile
            playCount++
        }
    }
}

function createTile(x,y,f,s) {

     if (component.status == Component.Ready)
         finishCreation(x,y,f,s);
     else
         component.statusChanged.connect(finishCreation);
}

function createTile2(x,y) {
     if (hl.status == Component.Ready)
         finishCreation2(x,y);
     else
         hl.statusChanged.connect(finishCreation);
}

function finishCreation2(x,y) {
    if (hl.status == Component.Ready) {
        highlights[hlCount] = hl.createObject(tableArea, {"x": x, "y": y});
        if (highlights[hlCount] == null) {
            // Error Handling
            console.log("Error creating object\n"+item);
        }

        hlCount++
        console.log("tile created")
    } else if (hl.status == Component.Error) {
        // Error Handling
        console.log("Error loading component:", hl.errorString());
    }

}

function finishCreation(x,y,f,s) {
    if (component.status == Component.Ready) {
        tiles[count] = component.createObject(tableArea, {"x": x, "y": y , "firstValue":f, "secondValue": s});
        console.log(tiles[count].x + " " + tiles[count].y)
        if (tiles[count] == null) {
            // Error Handling
            console.log("Error creating object\n"+item);
        }

        count++
        console.log("tile created")
    } else if (component.status == Component.Error) {
        // Error Handling
        console.log("Error loading component:", component.errorString());
    }

}
