var component
var counter = 0
var list = new Array()

function addProfile(name) {
    component = Qt.createComponent("Profile.qml");
     if (component.status == Component.Ready)
         finishCreation(name);
     else
         component.statusChanged.connect(finishCreation);
}

function finishCreation(name) {
    if (component.status == Component.Ready) {
        list[counter] = component.createObject(profileScreen.profileList, {"name": name, "index": counter});
        if (list[counter] == null) {
            // Error Handling
            console.log("Error creating object\n"+item);
        }
        list[counter].updatePosition()
        counter++
        console.log("Profile added")
    } else if (component.status == Component.Error) {
        // Error Handling
        console.log("Error loading component:", component.errorString());
    }

}

function deleteProfile(index){
    list[index].destroy()
    for(var i=index;i<counter-1;i++){
        list[i] =  list[i+1]
        list[i].index = i
        list[i].updatePosition()
        list[i].current = false
    }
    if(counter > 0){
        list[0].current = true
    }

    counter--
    console.log("Profile deleted")
}
