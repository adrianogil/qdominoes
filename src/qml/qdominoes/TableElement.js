/*
    This file is part of QDominoes.

    QDominoes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QDominoes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QDominoes.  If not, see <http://www.gnu.org/licenses/>.


*/

// Including external javascript libraries
Qt.include("utils.js");
Qt.include("Tile.js");

/**************************************************************/
// Define variables related to Tile Management inside a Table Element

var tableElementQMLFile = "TableElement.qml";

var tableElementComponent = Qt.createComponent("TableElement.qml");

var tableElementsOnCorner = {};

var currentTile = null;

function createTableElement(parent, options, callback) {
    console.debug("TableElement.js::createTableElement - parent:" + parent +
                  "options:" + options);
    createDynamicComponent(tableElementComponent,
                           parent,
                           function(obj) {
                               configureTableElement(obj, options, callback)
                           });
}

function configureTableElement(tableElementObj, options, callback) {
    configureObj(tableElementObj, options);
    if (callback != undefined)
        callback(tableElementObj);
}

//@TODO Create specific functions about Tile creation and management
// inside a Table Element

/**************************************************************
     * When the model is updated all the render must be update too
        --> If not exists any tiles create a new tile
        --> If the current tableModel have children nodes
       then create new TableElements and store its references
    ***************************************************************/
function onTableElementModelChanged(parent) {
    var tableModel = parent.tableModel;

    // Update current TileModel
    if (!currentTile || currentTile.tileModel != tableModel.tileModel) {
        if (currentTile) {
            currentTile.visible = false;
            currentTile.destroy(1000);
            currentTile = null;
        }

        createTileObj(parent, {
                          "tileModel" : tableModel.tileModel
                      },
                      // TableElement gets the same size of its Tile children
                      function(tileObj) {
                          parent.width = tileObj.width;
                          parent.height = tileObj.height;
                          currentTile = tileObj;
                      });
    }

    // Update corners
    for (var c = 0; c < 4; c++) {
        var corner = tableModel.cornerModel(c);
        var currentDirection = corner.direction;
        if (tableElementsOnCorner[currentDirection]) {
            var tableElementObj = tableElementsOnCorner[currentDirection];
            tableElementObj.tableModel = corner.tableModel;
        } else {
            createTableElementOnCorner(parent, corner.tableModel, currentDirection);
        }
    }
}


function createTableElementOnCorner(tableElement, tableModel, direction) {
    var rotationXVectors = {
        // Direction : [Width, Height]
        'Up' : [0,  0],
        'Right' : [1,  0],
        'Down' : [0,  0],
        'Left' : [0, -1]
    };

    var rotationYVectors = {
        // Direction : [Width, Height]
        'Up' : [0,  -1],
        'Right' : [1/2,  -1/2],
        'Down' : [0,  1],
        'Left' : [1/2, -1/2]
    };

    createTableElement(tableElement, {
                           "x" : rotationXVectors[direction][0] * tableElement.width +
                                 rotationXVectors[direction][1] * tableElement.height,
                           "y" : rotationYVectors[direction][0] * tableElement.width +
                                 rotationYVectors[direction][1] * tableElement.height,
                           "tableModel" : tableModel
                       }, function(tableElementObj) {tableElementsOnCorner[direction] = tableElementObj});
}
