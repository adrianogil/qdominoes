//@author: ricardoerikson
import QtQuick 1.1

Item {
    id:splash
    anchors.fill: parent


    property int duration: 0
    signal splashCompleted;
    signal loadScreens;

    function fadeOut(){
        fadeAnimation.start();
    }

    Image {
        id: animation
        width: splash.width; height: splash.height
        source: "qrc:/splash2.png"
    }

    SequentialAnimation {
        id: fadeAnimation
        NumberAnimation { target: splash; property: "opacity"; to: 0; duration: 600 }

        onCompleted: {
            splash.destroy(1000);
        }
    }

    SequentialAnimation {
        id: animation2
        running: true
        PauseAnimation{duration:splash.duration/2}
        ScriptAction{
            script: {
                splash.loadScreens();
            }
        }
        PauseAnimation{duration:splash.duration/2}

        onCompleted: {
            splash.splashCompleted();
        }
    }

    onSplashCompleted: {
        splash.fadeOut();
    }

    Component.onCompleted: {
        animation2.start();
    }
}
