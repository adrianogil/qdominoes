/*
    This file is part of QDominoes.

    QDominoes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QDominoes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QDominoes.  If not, see <http://www.gnu.org/licenses/>.

*/

// Including external javascript libraries
Qt.include("utils.js");
Qt.include("Tile.js");
Qt.include("Highlight.js");
Qt.include("gameutils.js");

// Store all created tiles
var tileObjList = []

// Connectivity struct of tiles
var tableStructure = {}

// Store all tile Objs placed on corners
var cornerTileObjs = {}

// Default direction strings
var directions = ['Up', 'Right', 'Down', 'Left'];

// Store all highlights objects created according a direction
var highlightObjs = {};

// @TODO: Must be verified
function doScoreAnimation(sideA, sideB, points, corners) {
    console.debug('Table.js - doScoreAnimation - set current Score: ' + points
                  + ' about tile (' + sideA + ',' + sideB + ') on corner ' + corners);
    var tileObject = getTileObj(sideA,sideB);
    if(tileObject != undefined) {
        tileObject.doScoreAnimation(points, corners)
    } else console.debug('Table.js - doScoreAnimation - can\'t find the tileObject (' +
                         sideA + ',' + sideB + ')');
}

function setTableStructure(newTStructure) {
    tableStructure = newTStructure;
}

function onTableModelChanged(parent, tableModel)
{
    updateTableRendering(tableStructure, tableModel);
}

// Update the rendering of tiles connectivity struct
function updateTableRendering(tStructure, tableModel, direction)
{
    if (tableModel == null || tableModel == undefined)
        return;

    if (direction == undefined)
        direction = 'first'

    if (tStructure == undefined)
        tStructure = {};

    if (tableModel.tileModel != undefined) {
        // Create/update tiles
        if (tStructure['tileObj'] != undefined)
            updateTileRendering(tableModel.tileModel, direction, tStructure['tileObj']);
        else
            updateTileRendering(tableModel.tileModel, direction);
    }
    // Dealing with the case of first tile on table
    if (direction == 'first') {
        // Update corners
        for (var c = 0; c < 4; c++) {
            if (tableModel.cornerModel == undefined)
                return;
            var corner = tableModel.cornerModel(c);
            if (corner == undefined)
                continue;
            var currentDirection = corner.direction;
            if (corner.tableModel != undefined) {
                updateTableRendering(tStructure[currentDirection],corner.tableModel, currentDirection)
            }
        }
    } else {
        var corner = tableModel.cornerModel(direction);
        updateTableRendering(tStructure[direction], corner.tableModel, direction);
    }
}

// Updates/create Tile from the next tile on corner
function updateTileRendering(tileModel, direction, tileObj)
{
    if (tileModel == undefined)
        return;

    if (tileObj == undefined) {
        // Must create a new Tile
        if (direction == "first") {
            // First tile is centered on table
            createTileObj(table, {
                              "x" : table.width/2,
                              "y" : table.height/2,
                              "tileModel" : tileModel
                          },
                          function(tileObj) {
                              configureTileOnTable(tileObj, direction);
                          })
        }
        else {
            // Following tiles must respect the current direction
            var tilePosition = getTilePositionByDirection(direction, tileModel.sideA == tileModel.sideB);

            createTileObj(table, {
                              "x" : tilePosition['x'],
                              "y" : tilePosition['y'],
                              "tileModel" : tileModel,
                              //"enableAnimation" : false,
                              "tileRotation" : tilePosition['rotation']
                          },
                          function(tileObj) {
                              configureTileOnTable(tileObj, direction);
                          })
        }

    }
}

// Callback function to update TableStruture
function configureTileOnTable(tileObj, direction)
{
    tileObjList[tileObjList.length] = tileObj;

    if (direction == 'first') {
        /** Creating a TableStructure **/
        var tStructure = {};
        tStructure['tileObj'] = tileObj;
        setTableStructure(tStructure);
        // Corner must point at first tile when beginning
        cornerTileObjs['Up'] = tileObj;
        cornerTileObjs['Right'] = tileObj;
        cornerTileObjs['Down'] = tileObj;
        cornerTileObjs['Left'] = tileObj;
        cornerTileObjs['first'] = tileObj;
    } else {
        /** Update current TableStructure **/
        // Put tile on current corner direction
        cornerTileObjs[direction] = tileObj;

        // Initialize auxiliar variables to navigate through TableStructure
        var directionStructures = [tableStructure];
        var currentStructure = tableStructure
        var nextStructure = currentStructure[direction];

        // Search for last object on corner direction
        while(nextStructure != undefined) {
            currentStructure = nextStructure;
            nextStructure = nextStructure[direction];
            directionStructures[directionStructures.length] = currentStructure;
        }

        // Set up last object on corner direction
        currentStructure[direction] = {};
        currentStructure[direction]['tileObj'] = tileObj;
        directionStructures[directionStructures.length] = currentStructure[direction];

        if (directionStructures.length > 2) {
            for (var i = directionStructures.length - 2; i >= 0; i--) {
                directionStructures[i][direction] = directionStructures[i+1];
            }

            setTableStructure(directionStructures[0]);
        } else {
            setTableStructure(currentStructure);
        }
    }
}

// Debug purposes
function printTableStructure(tStructure, direction, level) {
    if (tStructure == undefined)
        return;
    if (direction == undefined)
        direction = 'first'
    if (level == undefined)
        level = 0;
    var msg = '';
    for (var i = 0; i <= level; i++)
        msg = msg + '-';
    if (tStructure['tileObj'] != undefined)
        msg = msg +  'direction: ' + direction +
            ' - tileObj: (' + tStructure['tileObj'].tileModel.sideA + ',' + tStructure['tileObj'].tileModel.sideB + ')';
    console.debug(msg);
    for (var c = 0; c < 4; c++)
        printTableStructure(tStructure[directions[c]], directions[c], level+1);
}

function onTileMovedFromHand(x,y,w,h,sideA,sideB) {
    createHighlight(sideA,sideB);
    var callbackOnCollision = function(i) {
        var tileObj = cornerTileObjs[directions[i]];
        console.debug('Table.js - onTileMovedFromHand - callbackOnCollision - '
                      + 'direction: ' + i + " - tileModel: (" +
                      tileObj.tileModel.sideA + ',' + tileObj.tileModel.sideB + ')')
        var angle = 0;
        if(i == 0) {
            if(sideA == sideB){
                angle = 90;
            } else if (tileObj != undefined){
                if (sideA == tileObj.tileModel.sideB)
                    angle = 180;
            }
        } else if (i == 1 && tileObj != undefined && sideA != sideB){
            if (sideA == tileObj.tileModel.sideB)
                angle = -90;
            else if (sideB == tileObj.tileModel.sideB)
                angle = 90;
        } else if (i == 2) {
            if(sideA == sideB){
                angle = 90;
            } else if (tileObj != undefined){
                if (sideB == tileObj.tileModel.sideB)
                    angle = 180;
            }
        } else if (i == 3 && tileObj != undefined && sideA != sideB) {
            if (sideA == tileObj.tileModel.sideB)
                angle = 90;
            else if (sideB == tileObj.tileModel.sideB)
                angle = -90;
        }
        table.rotateTile(angle);
    };
    var callbackOnNonCollision = function() {
        table.rotateTile(0)
    };

    checkCollisionWithHighlights(x,y,w,h,sideA,sideB,callbackOnCollision,callbackOnNonCollision);
}

function checkCollisionWithHighlights(x,y,w,h,sideA,sideB,callbackOnCollision,callbackOnNonCollision) {
    var collided = false;
    for (var i = 0; i < directions.length; i++) {
        //        console.debug('Table.js - checkCollisionWithHighlights - direction: ' + directions[i]);
        var hObj = highlightObjs[directions[i]];
        if (hObj != undefined) {
            var tileObj = {"x" : x, 'y': y, 'width' : w, 'height' : h};
            //            console.debug('Table.js - checkCollisionWithHighlights - 1');
            if(verifyCollision(tileObj, hObj)) {
                //                console.debug('Table.js - checkCollisionWithHighlights - 2')
                collided = true;
                callbackOnCollision(i);
                break;
            }
        }
    }
    if (!collided)
        callbackOnNonCollision();
}

function onTileReleased(x,y,w,h,sideA,sideB) {
    if (tileObjList.length == 0) {
        // Verify first move
        var isValidMove = tableController.isValidMove('first', sideA, sideB);
        if (isValidMove) {
            table.acceptedTileOnTable(sideA,sideB,-1);
            //            console.debug('Table.js - onTileReleased - Accepted tile');
        }
    } else {
        checkCollisionWithHighlights(x,y,w,h,sideA,sideB,
                                     function(corner) {
                                         table.acceptedTileOnTable(sideA,sideB,corner);
                                     },
                                     function() {
                                         table.nonAcceptedTileOnTable(sideA,sideB)
                                     });
    }
    destroyAllHighlights();
}


function destroyAllHighlights() {
    for (var i = 0; i < directions.length; i++) {
        var hObj = highlightObjs[directions[i]];
        if (hObj != undefined) {
            hObj.visible = false;
            hObj.destroy(1000);
            highlightObjs[directions[i]] = undefined;
        }
    }
}

function createHighlight(sideA, sideB) {
    var upDownOpenCorner = cornerTileObjs[directions[1]] != cornerTileObjs['first']
        && cornerTileObjs[directions[3]] != cornerTileObjs['first'];
    for (var i = 0; i < 4; i++) {
        if ((i == 0 || i == 2) && !upDownOpenCorner)
            continue;
        var isValidMove = tableController.isValidMove(directions[i], sideA, sideB);
        if (isValidMove && cornerTileObjs[directions[i]] != undefined) {
            if (highlightObjs[directions[i]] == undefined)
                createHighlightAtCorner(directions[i]);
        }
    }
}

function createHighlightAtCorner(direction) {
    var tileCornerObj = cornerTileObjs[direction];

    var tilePosition = getTilePositionByDirection(direction, false);

    createHighlightObj(table, tilePosition, function(hObj) {configureHighlightObj(hObj, direction);});
}

function configureHighlightObj(hObj, direction) {
    highlightObjs[direction] = hObj;
}

function getTileObj(sideA,sideB) {
    var tileObj = null;

    for (var t = 0; t < tileObjList.length; t++) {
        var tModel = tileObjList[t].tileModel;
        if ((tModel.sideA == sideA && tModel.sideB == sideB) ||
                (tModel.sideA == sideB && tModel.sideB == sideA)) {
            tileObj = tileObjList[t];
            break;
        }
    }

    return tileObj;
}

function getTilePositionByDirection(direction, isADouble) {

    var positionXVectors = {
        // Direction : [Width, Height]
        'Up' : [0,  0],
        'Right' : [1,  0],
        'Down' : [0,  0],
        'Left' : [0, -1]
    };

    var positionYVectors = {
        // Direction : [Width, Height]
        'Up' : [0,  -1],
        'Right' : [1/2,  -1/4],
        'Down' : [0,  1],
        'Left' : [1/2, -1/4]
    };

    var rotations = {
        'Up' : isADouble? 90: 0,
        'Right' : isADouble? 0: 90,
        'Down' : isADouble? 90: 180,
        'Left' : isADouble? 0: -90
    }

    var lastTileObj = cornerTileObjs[direction];

    var tileWidth = lastTileObj.width;
    var tileHeight = lastTileObj.height;

    var adjustX = 0;
    var adjustY = 0;

    if (lastTileObj == cornerTileObjs['first']) {
        if (direction == 'Left' || direction == 'Right')
            adjustX = tileWidth * 1/2;
    } else {
        if (direction == 'Right')
            adjustX = tileWidth;
    }

    if (isADouble) {
        var newAdjust = {
            'Up' :    [0, 1/4],
            'Right' : [-1/2, 0],
            'Down' :  [0, -1/4],
            'Left' :  [1/2, 0]
        };
        adjustX = adjustX + newAdjust[direction][0] * tileWidth;
        adjustY = adjustY + newAdjust[direction][1] * tileHeight;
    } else if (lastTileObj != cornerTileObjs['first'] &&
               lastTileObj.tileModel.sideA == lastTileObj.tileModel.sideB) {
        var newAdjust = {
            'Up' :    [0, 1/2],
            'Right' : [-1/2, 0],
            'Down' :  [0, 0],
            'Left' :  [1/2, 0]
        };
        adjustX = adjustX + newAdjust[direction][0] * tileWidth;
        adjustY = adjustY + newAdjust[direction][1] * tileHeight;
    }

    var tilePosition = {
        "x" : lastTileObj.x + adjustX +
              positionXVectors[direction][0] * tileWidth +
              positionXVectors[direction][1] * tileHeight,
        "y" : lastTileObj.y + adjustY +
              positionYVectors[direction][0] * tileWidth +
              positionYVectors[direction][1] * tileHeight,
        'rotation' : rotations[direction]
    };

    return tilePosition;
}
