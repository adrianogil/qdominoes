// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../ViewsDirector/qml"
import "../../ViewsDirector/js/Director.js" as ViewsDirector

AbstractView {
    id: levelChoiceScreen

    signal easyClicked
    signal mediumClicked
    signal hardClicked
    signal backButtonClicked
    signal menuButtonClicked

    Image{
        id: label
        source: "qrc:/Level/Jogo-individual.png"
        anchors{
            top: levelChoiceScreen.top
            horizontalCenter: levelChoiceScreen.horizontalCenter
            topMargin: 15
        }
    }

    Column{
        spacing: 15
        anchors{
            horizontalCenter: levelChoiceScreen.horizontalCenter
            top: label.bottom
            topMargin: 30
        }
        Button{
            buttonImage: "qrc:/Level/botao-facil.png"
            pressedImage: "qrc:/Level/botao-facil-press.png"
            onClicked: {
                levelChoiceScreen.easyClicked()
                mainController.startGame();
            }
        }
        Button{
            buttonImage: "qrc:/Level/botal-medio.png"
            pressedImage: "qrc:/Level/botal-medio-press.png"
            onClicked: {
                levelChoiceScreen.mediumClicked()
                mainController.startGame();

            }
        }
        Button{
            buttonImage: "qrc:/Level/botao-dificil.png"
            pressedImage: "qrc:/Level/botao-dificil-press.png"
            onClicked: {
                levelChoiceScreen.hardClicked()
                mainController.startGame();
            }
        }
    }

//    Button{
//        id: menuButton
//        anchors{
//            top: levelChoiceScreen.top
//            topMargin: 15
//            leftMargin: 15
//            left: levelChoiceScreen.left
//        }

//        buttonImage: "qrc:/Menu.png"
//        pressedImage: "qrc:/Menu.png"
//        onClicked: {
//            levelChoiceScreen.menuButtonClicked()

//        }
//    }

    Button{
        id: backButton
        anchors{
            top: levelChoiceScreen.top
            topMargin: 15
            rightMargin: 15
            right: levelChoiceScreen.right
        }
        buttonImage: "qrc:/Voltar.png"
        pressedImage: "qrc:/Voltar.png"
        onClicked: {
            levelChoiceScreen.backButtonClicked()
            if(ViewsDirector.currentView() != "menu"){
                ViewsDirector.nextView("menu");
            }
        }
    }
}
