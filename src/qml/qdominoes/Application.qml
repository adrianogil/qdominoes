// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../ViewsDirector/js/Director.js" as ViewsDirector

Rectangle {
    id:mainView

    Image{
        anchors.fill: parent
        width: mainView.width; height: mainView.height
        source: "qrc:/Menu/Background.png"
    }

    FontLoader { id: font; source: "qrc:/Chalkduster.ttf" }

    Rectangle {
        id: container

        width: mainView.width; height: mainView.height

        color: "transparent"

        Component {
            id: newProfileViewComponent
            NewProfileScreen {
                width: mainView.width; height: mainView.height
            }
        }
        Component {
            id: profileViewComponent
            ProfileScreen {
                width: mainView.width; height: mainView.height
            }
        }
        Component {
            id: menuViewComponent
            TitleScreen {
                width: mainView.width; height: mainView.height
            }
        }
        Component {
            id: contactViewComponent
            ContactScreen {
                width: mainView.width; height: mainView.height
                fontFamily: font
            }
        }
        Component {
            id: gameScreenComponent
            GameScreen {
                width: mainView.width; height: mainView.height
            }
        }
        Component {
            id: aboutViewComponent
            AboutScreen {
                width: mainView.width; height: mainView.height
                fontFamily: font
            }
        }
        Component {
            id: rulesViewComponent
            RulesScreen {
                width: mainView.width; height: mainView.height
                fontFamily: font
            }
        }
        Component {
            id: levelChoiceComponent
            LevelChoiceScreen {
                width: mainView.width; height: mainView.height
            }
        }
        Component {
            id: deleteProfilePopUpComponent
            DeleteProfilePopUp {
                width: mainView.width; height: mainView.height
            }
        }
        Component {
            id: closeAppPopUpComponent
            CloseAppPopUp {
                width: mainView.width; height: mainView.height
            }
        }
        Component {
            id: passedTurnPopUpComponent
            PassedTurnPopUp {
                width: mainView.width; height: mainView.height
                fontFamily: font
            }
        }
        Component {
            id: moveBackMenuPopUpComponent
            MoveBackMenuPopUp {
                width: mainView.width; height: mainView.height
                fontFamily: font
            }
        }
        Component {
            id: resultsPopUpComponent
            Results {
                width: mainView.width; height: mainView.height
            }
        }
    }

    Component.onCompleted: {
        ViewsDirector.setContainer(container);
        ViewsDirector.setDefaultView(profileViewComponent)
        ViewsDirector.registerPopup("passedTurnPopUp",passedTurnPopUpComponent)
        ViewsDirector.registerPopup("deleteProfilePopUp",deleteProfilePopUpComponent)
        ViewsDirector.registerPopup("closeAppPopUp",closeAppPopUpComponent)
        ViewsDirector.registerPopup("moveBackMenuPopUp", moveBackMenuPopUpComponent)
        ViewsDirector.registerView("menu", menuViewComponent)
        ViewsDirector.registerView("gameScreen", gameScreenComponent)
        ViewsDirector.registerView("levelChoice", levelChoiceComponent)
        ViewsDirector.registerView("newProfile", newProfileViewComponent)
        ViewsDirector.registerView("about", aboutViewComponent)
        ViewsDirector.registerView("contacts", contactViewComponent)
        ViewsDirector.registerView("rules", rulesViewComponent)
        ViewsDirector.registerView("results", resultsPopUpComponent)
        ViewsDirector.setFadeColorAndOpacity("#000000")
        ViewsDirector.start();

    }
}
