// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item {
    id: results
    signal facebookClicked
    signal twitterClicked
    signal backButtonClicked

    property string player1Name: ""
    property string player2Name: ""
    property string resultText: "\tA dupla vencedora foi "+player1Name+" e "+player2Name
    property string boxText: resultText + ".\n\n\tCompartilhe o resultado no Twitter ou" +
                             " \n\tfacebook e peça uma revanche."


    Rectangle{
        anchors.fill: parent
        color: "black"
        opacity: 0.5
    }

    Image{
        source: "qrc:/popups/Cauxa-resultados-pop-up.png"
        anchors.centerIn: parent

        Text{
            width: parent.width
            height: parent.height*0.8
            anchors{
                top: parent.top
                //horizontalCenter: parent.horizontalCenter
                topMargin: 40
            }

            text: results.boxText
        }

        Button{
            id: facebookButton
            buttonImage: "qrc:/popups/botao-facebook.png"
            pressedImage: "qrc:/popups/botao-facebook-press.png"
            anchors{
                bottom: parent.bottom
                left: parent.left
                bottomMargin: 12
                leftMargin: 30
            }
            onClicked: {
                results.facebookClicked()
            }

        }

        Button{
            id: twitterButton
            buttonImage: "qrc:/popups/botao-twitter.png"
            pressedImage: "qrc:/popups/botao-twitter-press.png"
            anchors{
                bottom: parent.bottom
                left: facebookButton.right
                bottomMargin: 12
                leftMargin: 10
            }
            onClicked: {
                results.twitterClicked()
            }
        }

        Button {
            id: backButton
            buttonImage: "qrc:/popups/botao-retornar-menu.png"
            pressedImage: "qrc:/popups/botao-retornar-menu-press.png"
            anchors{
                bottom: parent.bottom
                right: parent.right
                bottomMargin: 12
                rightMargin: 30
            }
            onClicked: {
                results.backButtonClicked()
            }
        }
    }
}
