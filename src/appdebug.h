#ifndef APPDEBUG_H
#define APPDEBUG_H

#include <QDebug>

#define debugMsg qDebug() << __PRETTY_FUNCTION__

#endif // APPDEBUG_H
