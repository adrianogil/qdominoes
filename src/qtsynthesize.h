#ifndef QTSYNTHESIZE_H
#define QTSYNTHESIZE_H

#include <QtCore/QObject>

#define Q_SSYNTHESIZE(varType, propName) \
private: varType m_##propName;\
public: varType propName(void) const { return m_##propName; }

//! Generate set and get methods and private declaration for a given property
#define Q_SYNTHESIZE(varType, propName, funcName, signalName) \
private: varType m_##propName;\
public: varType propName(void) { return m_##propName; }\
public: void set##funcName(varType var){ \
    if (var != m_##propName) {\
        m_##propName = var; \
        emit signalName(); \
        } \
}

//! Generate set and get methods and private declaration for a given property
#define Q_NSYNTHESIZE(varType, propName, funcName) \
private: varType m_##propName;\
public: varType propName(void) { return m_##propName; }\
public: void set##funcName(varType var){ \
    if (var != m_##propName) {\
        m_##propName = var; \
        } \
}

//! Generate set and get methods and private declaration for a given property
#define Q_CSYNTHESIZE(varType, propName, funcName, signalName) \
private: varType m_##propName;\
public: varType propName(void) const { return m_##propName; }\
public: void set##funcName(const varType &var){ \
    if (var != m_##propName) {\
        m_##propName = var; \
        emit signalName(); \
        } \
}

#endif // QTSYNTHESIZE_H
